<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//
Route::get('/', function () {
  return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
  Route::get('/changePassword','HomeController@showChangePasswordForm');
  Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::get('/admin', function()
    {
        return view('admin.home');
    });
    Route::resource('/admin/classroom', 'Admin\classroomController');//ชนิดห้องเรียน
    Route::resource('/admin/position', 'admin\positionController');//ตำแหน่ง
    Route::resource('/admin/subject', 'admin\subjectController');//กลุ่มสาระ
    Route::resource('/admin/status', 'admin\statusController');//สถานะ
    Route::resource('/admin/courses', 'admin\coursecontroller');//รายวิชา
    Route::get('/admin/showcourses', 'admin\coursecontroller@show');  //บุคลากร
    Route::resource('/admin/personel', 'admin\personelController');
    Route::get('/admin/showpersonel', 'admin\personelController@show');
    Route::get('/admin/personelheader_sub','admin\personelController@header_sub');  //scheduel
    Route::resource('/admin/scheduel', 'admin\scheduelController');
    Route::post('/admin/scheduel/store', 'admin\scheduelController@store');
    Route::get('/admin/adminshowstdscheduel/', 'admin\scheduelController@show');
    Route::delete('/admin/scheduel/{id}', 'admin\scheduelController@destroy');
    Route::post('/admin/scheduel/create', 'admin\scheduelController@create');
    Route::get('/admin/remove/item', 'admin\scheduelController@destroy');
    Route::get('/admin/scheduel/reset/reset', 'admin\scheduelController@resetscheduel');
    Route::get('/admin/scheduel/reset/resettch', 'admin\scheduelController@resettchscheduel');  //ตารางสอน
    Route::get('/admin/showteachscheduel/', 'admin\teachcontroller@show');
    Route::resource('/admin/teachscheduel', 'admin\teachcontroller');  //รายวิชาที่เปิดสอน
    Route::resource('/admin/instructor', 'admin\instructorcontroller');
    Route::get('/admin/showinstructor/', 'admin\instructorcontroller@show');  //กลุ่มเรียน
    Route::resource('/admin/student_group', 'admin\student_groupcontroller');  //ตารางเรียนของนักเรียน
    Route::resource('/admin/stdscheduel', 'admin\stdcontroller');
    Route::get('/admin/showstdscheduel/', 'admin\stdcontroller@show');
    Route::get("/admin/loadpdf_stdscheduel/","admin\HomeController@downloadPDFstd");//pdf
    Route::get("/admin/loadpdf_teacherscheduel/","admin\HomeController@downloadPDFteacher");  //จัจัดตารางเรียนสำหรับครูผู้สอน
    Route::resource('/admin/createscheduelteacher', 'admin\scheduelteacher');
    Route::get('/admin/instructor/{id}/edit', 'admin\instructorcontroller@edit');
});
Route::group(['middleware' => 'App\Http\Middleware\UserMiddleware'], function()
{
  // Submatters //
  Route::get('/sub_mat/home','Matter\PageController@getHome');
  Route::get('/sub_mat/view_instructor_year_studies', 'Matter\PageController@getViewInstructorYearStudies');
  Route::get('/sub_mat/add_instructor','Matter\PageController@getAddInstructor');
  Route::get('/sub_mat/add_study_course','Matter\PageController@getAddStudyCourse');
  Route::get('/sub_mat/loadPdf_instructor/{id}/{ids}','Matter\PageController@loadPdfInstructor');
  Route::get('/sub_mat/loadPdf_instructor_personels/{id}/{ids}','Matter\PageController@loadPdfInstructorPersonels');
  Route::get('/sub_mat/loadPdf_scheduel/{id}/{ids}','Matter\PageController@loadPdfScheduel');
  Route::get('/sub_mat/exportExcel_scheduel{id}','Matter\PageController@exportExcelScheduel');
  Route::get('/sub_mat/view_Scheduel','Matter\PageController@getViewScheduel');
  Route::get('/sub_mat/edit_instructor','Matter\UpdateInstructorController@getEditInstructor');
  Route::get('/sub_mat/update_instructor_personels/{id}/{ids}','Matter\ViewInstructorController@UpdateInstructorPersonels');
  Route::get('/sub_mat/view_personels','Matter\PageController@getViewPersonel');
  Route::get('/sub_mat/loadPdf_personels','Matter\PageController@loadPdfPersonels');
  Route::get('/sub_mat/view_table/{id}/{ids}','Matter\PageController@getViewTable');
  Route::get('/sub_mat/return{id}','Matter\ViewScheduelController@search');
  Route::get('/sub_mat/view_instructor{id}','Matter\PageController@getViewInstructor');
  Route::get('/sub_mat/show_courses_name','Matter\PageController@getCoursesName');
  Route::get('/sub_mat/view_instructor','Matter\PageController@getViewInstructor');
  Route::get('/sub_mat/search/{id}','Matter\ViewInstructorController@search');

  Route::post('/sub_mat/view_instructor','Matter\PageController@getViewInstructor');
  Route::post('/sub_mat/search/{id}','Matter\ViewInstructorController@search');
  Route::post('/sub_mat/search_table','Matter\ViewScheduelController@search');
  Route::post('/sub_mat/submitCourses','Matter\AddCoursesController@submit');
  Route::post('/sub_mat/submitInstructor','Matter\AddInstructorController@submit');
  Route::post('/sub_mat/submitStudyCourse','Matter\AddStudyCourseController@submit');
  Route::post('/sub_mat/submitYearStudies','Matter\AddYearStudiesController@submit');
  Route::post('/sub_mat/update_instructor{id}','Matter\UpdateInstructorController@getUpdateInstructor');
  Route::post('/sub_mat/update{id}','Matter\UpdateInstructorController@getUpdateInstructors');

});


Route::group(['middleware' => 'App\Http\Middleware\DirecterMiddleware'], function()
{
  // Director //
  Route::get('/director/home','Director\PageController@getHome');
  Route::get('/director/view_personels','Director\PageController@getViewPersonel');
  Route::get('/director/view_scheduel','Director\PageController@getViewScheduel');
  Route::get('/director/view_student_groups','Director\PageController@getViewStudentGroups');
  Route::get('/director/view_table_student_groups/{id}/{ids}{idss}','Director\PageController@getViewTableStudentGroups');
  Route::get('/director/view_table/{id}/{ids}','Director\PageController@getViewTable');
  Route::get('/director/return/{id}/{ids}','Director\PageController@getReturn');
  Route::get('/director/confrim_tables/{id}','Director\ViewScheduelController@confrimTables');
  Route::get('/director/return_student_groups/{id}/{ids}','Director\PageController@getReturnStudentGroups');

  Route::post('/director/search_table','Director\ViewScheduelController@searchTable');
  Route::post('/director/search_tables{ids}','Director\ViewScheduelController@searchTables');
  Route::post('/director/searchYearStudies','Director\ViewScheduelController@searchYearStudies');
  Route::post('/director/searchSubjectmatter{id}','Director\ViewScheduelController@searchSubjectmatter');
  Route::post('/director/confrimScheduel/{id}','Director\ViewScheduelController@confrimScheduelStudentGroups');
  Route::post('/director/confrimPersonelsScheduels/{id}/{ids}','Director\ViewScheduelController@confrimScheduelPersonels');
});
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
