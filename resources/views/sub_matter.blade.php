<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>ระบบจัดตารางเรียนตารางสอน</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	  <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
      <!-- Our Custom CSS -->
      <link rel="stylesheet" href="/css/style.css">
      <script src="../admin/scrip.js?v=<?php echo rand(); ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-default">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
             <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a href="#" class="navbar-brand">โรงเรียนไทรแก้ววิทยา</a>
         </div>
         <!-- Collection of nav links and other content for toggling -->
         <div id="navbarCollapse" class="collapse navbar-collapse">
             <ul class="nav navbar-nav navbar-right">
                <li><a href="#">ข้อมูลส่วนตัว</a></li>
                 <li><a href="#">ออกจากระบบ</a></li>
             </ul>
         </div>
     </nav>
     <div class="wrapper">
             <!-- Sidebar Holder -->
             <nav id="sidebar" class="active">
                   <h4 class="glyphicon glyphicon-user"> หัวหน้ากลุ่มสาระ</h4>

                 <ul class="list-unstyled">
                     <li >
                         <a href="/sub_mat/add_instructor" >
                           <i class="glyphicon glyphicon-pencil"></i>
                           จัดการรายวิชาที่เปิดสอน
                         </a>
                     </li>
                     <li >
                       <a href="#homeSubmenu_submat1" data-toggle="collapse" aria-expanded="false">
                           <i class="glyphicon glyphicon-search"></i>
                           ค้นหา
                       </a>
                       <ul class="collapse list-unstyled" id="homeSubmenu_submat1">
                           <li ><a href="/sub_mat/view_instructor" class="glyphicon glyphicon-search"> ค้นหารายวิชาที่เปิดสอน</a></li>
                           <li ><a href="#" class="glyphicon glyphicon-search"> ค้นหาตารางสอน</a></li>
                       </ul>
                     </li>
                 </ul>

             </nav>
             <!-- Page Content Holder -->
             <div id="content">

             </div>
             @yield('content')
         </div>
         <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
            @if(Session::has('message'))
                var type="{{Session::get('alert-type','success')}}"

                switch(type){
                    case 'success':
                        toastr.success("{{ Session::get('message') }}");
                        break;
                    case 'error':
                        toastr.error("{{ Session::get('message') }}");
                        break;
                }
            @endif
        </script>

  </body>
</html>
