@if(session('success'))
    <div class="alert alert-success col-xs-12 " style="text-align: center;">
        {{session('success')}}
    </div>
@endif

<div class="form-group">
  @if(count($errors) > 0)
    @foreach($errors->all() as $error)
      <div class="alert alert-danger col-xs-12 " style="text-align: center;">
          {{$error}}
      </div>
    @endforeach
  @endif
</div>
