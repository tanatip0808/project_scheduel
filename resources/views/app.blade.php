<!DOCTYPE html>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>ระบบจัดตารางเรียนตารางสอน</title>
    @yield('title')
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
     <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
     {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css"> --}}
      <!-- Our Custom CSS -->
      <link rel="stylesheet" href="/css/style.css">
      <script  src="/js/scrip.js?v=<?php echo rand(); ?>"></script>
  </head>
  <body>
    <nav class="navbar navbar-default">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
             <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a href="#" class="navbar-brand">โรงเรียนไทรแก้ววิทยา</a>
         </div>
         <!-- Collection of nav links and other content for toggling -->
         <div id="navbarCollapse" class="collapse navbar-collapse">

             <ul class="nav navbar-nav navbar-right">
                <li><a>ยินดีต้อนรับคุณ {{ Auth::user()->name }} </a></li>
                <li>
                              <a href="{{url('/changePassword')}}">
                                 เปลี่ยนรหัสผ่าน
                              </a>
                </li>
                <li> <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            ออกจากระบบ
                                        </a></li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
             </ul>
         </div>
     </nav>
     <div class="wrapper">
             <!-- Sidebar Holder -->
             <nav id="sidebar" class="active">
              <h4>ระบบจัดตารางเรียน</h4>
                 <ul class="list-unstyled components">
                     <li class="{{ Request::is('admin/classroom') ? 'active' : '' }}">
                         <a href="{{url('/admin/classroom')}}">
                             <i class="glyphicon glyphicon-home"></i>
                             ห้องเรียน
                         </a>

                     </li>
                     <li  class="{{ Request::is('admin/student_group') ? 'active' : '' }}">
                         <a href="/admin/student_group/">
                             <i class="glyphicon glyphicon-briefcase"></i>
                             กลุ่มเรียน
                         </a>
                      </li>
                      <li class="{{ Request::is('admin/courses') ? 'active' : '' }}">
                         <a href="/admin/courses/">
                             <i class="glyphicon glyphicon-briefcase"></i>
                             รายวิชา
                         </a>
                      </li>
                      <li class="{{ Request::is('admin/scheduel') ? 'active' : '' }}">
                         <a href="/admin/scheduel/">
                             <i class="glyphicon glyphicon-th"></i>
                            <b>จัดตารางเรียนนักเรียน</b>
                         </a>
                      </li>
                      <li class="{{ Request::is('admin/createscheduelteacher') ? 'active' : '' }}">
                         <a href="/admin/createscheduelteacher">
                             <i class="glyphicon glyphicon-th"></i>
                            <b>จัดตารางเรียนสำหรับครูผู้สอน</b>
                         </a>
                      </li>
                      <li class="{{ Request::is('admin/stdscheduel') ||  Request::is('admin/teachscheduel') ? 'active' : '' }}">
                         <a href="#search" data-toggle="collapse" aria-expanded="false">
                             <i class="glyphicon glyphicon-eye-open"></i>
                             ค้นหา/พิมพ์
                         </a>
                         <ul class="collapse list-unstyled" id="search">
                             <li class ="{{ Request::is('admin/stdscheduel') ? 'active' : ''}}"> <a href="{{url('admin/stdscheduel')}}">ตารางเรียน</a></li>
                             <li  class ="{{ Request::is('admin/teachscheduel') ? 'active' : ''}}" ><a href="{{url('admin/teachscheduel')}}">ตารางสอน</a></li>
                         </ul>
                      </li>
                      <li class="{{ Request::is('admin/personel') ||  Request::is('admin/personelheader_sub') ? 'active' : '' }}">
                           <a href="#pagepersonal" data-toggle="collapse" aria-expanded="false">
                               <i class="glyphicon glyphicon-user"></i>
                              บุคลากร
                           </a>
                           <ul class="collapse list-unstyled" id="pagepersonal">
                               <li class ="{{ Request::is('admin/personel') ? 'active' : ''}}"><a href="{{url('admin/personel')}}">ครูผู้สอน</a></li>
                               <li class ="{{ Request::is('admin/personelheader_sub') ? 'active' : ''}}"><a href="{{url('admin/personelheader_sub')}}">หัวหน้ากลุ่มสาระ</a></li>
                               <li class ="{{ Request::is('admin/director') ? 'active' : ''}}"><a href="{{url('admin/director')}}">ผู้อำนวยการ</a></li>
                           </ul>
                        </li>
                        <li class ="{{ Request::is('admin/subject') ? 'active' : ''}}">
                         <a href="{{url('admin/subject')}}">
                             <i class="glyphicon glyphicon-list-alt"></i>
                          กลุ่มสาระการเรียนรู้
                         </a>
                        </li>
                        <li  class ="{{ Request::is('admin/status') ? 'active' : ''}}">
                         <a href="{{url('admin/status')}}">
                             <i class="glyphicon glyphicon-thumbs-up"></i>
                            สถานะ
                         </a>
                        </li>
                        <li class ="{{ Request::is('admin/position') ? 'active' : ''}}">
                         <a href="{{url('admin/position')}}">
                             <i class="glyphicon glyphicon-thumbs-up"></i>
                            คำแหน่ง
                         </a>
                         </li>
                         <li class ="{{ Request::is('admin/instructor') ? 'active' : ''}}">
                         <a href="{{url('admin/instructor')}}">
                             <i class="glyphicon glyphicon-list-alt"></i>
                          รายวิชาที่เปิดสอน
                         </a>
                         </li>
             </nav>
             <!-- Page Content Holder -->
             <div class="col-md-12">
         @yield('content')
     </div>
        </div>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </body>
</html>
