@extends('layout.app')
@section('content')
    <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
  <div class="container-fluid">
    <form action="{{$url}}" method="POST" id="add_subject">
        {{method_field($method)}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="form-group">
        <label for="position_name">ชื่อกลุ่มสาระ</label>
        <input type="text" class="form-control"  name="subject" value="{{$obj->subject_matter_name or ''}}">
      </div>
      <button type="submit" class="btn btn-success">ตกลง</button>  <button type="reset" class="btn btn-danger">ล้าง</button>
    </form>
  </div>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#add_subject').bootstrapValidator({
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          subject: {
            validators: {
              notEmpty: {
                message: 'กรุณากรอกชื่อกลุ่มสาระการเรียนรู้'
              }
            }
          },
        },
      });
    });
  </script>
@endsection
