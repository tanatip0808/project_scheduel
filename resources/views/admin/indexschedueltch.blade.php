@extends('layout.app')

  @section('title')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @endsection
  @section('content')
  <br><br><br><br>
  <form action="{{url('/admin/createscheduelteacher/create')}}" method="get">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="std_group">ชื่อครูผู้สอน</label>
      <select id="std_group" class="form-control" name="teacher_id"  class="selectpicker form-control" required>
          <option value="" disabled selected>กรุณาเลือกครูผู้สอน</option>
        @foreach ($teacher as $tc)
        <option value="{{$tc->id}}" >{{$tc->first_name .'&nbsp;&nbsp;&nbsp;'.$tc->last_name}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="year">ปีการศึกษา</label>
      <select id="year" class="form-control" name= 'year'   class="selectpicker form-control" required>
          <option value="" disabled selected>กรุณาเลือกปีการศึกษา</option>
        @foreach ($year as $y)
        <option value="{{$y->id}}" >{{'   ภาคการศึกษาที่  ' .$y->term.'/'.$y->year}}</option>
        @endforeach
      </select>
    </div>
  </div><br>
    <button type="submit" class="btn btn-primary">ค้นหา</button>
</form>
<br>
<div class="well">
<h1>กรุณาเลือกครูผู้สอนที่จะจัดตารางเรียน!</h1>
</div>
<style>
   .well {
       height: 500px;
       width: 100%;
       background-color: #cccccc;
   }
   .well h1{
     padding-top: 15%;
     padding-left: 25%;
   }
</style>
@endsection
