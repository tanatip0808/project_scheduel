<!DOCTYPE html>
<html>
<title>ตารางรางเรียนชั้นมัธยมศึกษาปีที่ {{$stdlabel->class}}</title>
  <head>
      {{-- <link href="css/bootstrap.min.css" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/style.css">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
          font-family: 'THSarabunNew';
          font-style: normal;
          font-weight: bold;
          src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: normal;
          src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: bold;
          src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
      }
      table {
          width: 100%;
          border-collapse: collapse;
          /* border: 1px solid black; */
          /* border-style: groove; */
      }

      td,
      th {
          /* border: 1px solid black; */
           border: 1px groove black;
          /* border-style: ; */
          text-align: center;
      }
      th {
    background-color: #4CAF50;
    color: white;
}
        body {
            font-family: "THSarabunNew";
        }
        .container {
   height: auto;
   overflow: hidden;
}

.rightlicent {
    float: right;
    font-size: 20px;
}

.leftlicent {
    float: none; /* not needed, just for clarification */
    /* the next props are meant to keep this block independent from the other floated one */
    width: auto;
  font-size: 20px;
}
    </style>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  <body>
    <div>
      <img src="{{ public_path("/img/logo.jpg") }}" alt="" style="width: 100px; height: 150px; position:fixed; top:0; left:0;"><
      <br><br><br><br>
           <div align='center'>
              <b><font size="5">ตารางเรียนชั้นมัธยมศึกษาปีที่ {{$stdlabel->class or ''}}</font></b><br>
              <p><font size="5">ประจำภาคเรียนที่ {{$termyear->term}}   ปีการศึกษา {{$termyear->year}}</font><p>
              <p><font size="4">โรงเรียนไทรแก้ววิทยา  ตำบลกันตวจระมวล  อำเภอปราสาท  จังหวัดสุรินทร์</font></p>
        </div>
    </div>
    <div class="col-md-12 bg-default">
          <table class="table  text-center right left" >
              <tr>
                <th>วัน/เวลา</th>
                <th>08.30-09.20</th>
                <th>09.25-10.15</th>
                <th>10.20-11.10</th>
                <th>11.15-12.05</th>
                <th>12.05-12.55</th>
                <th>13.00-13.50</th>
                <th>13.55-14.45</th>
                <th>14.50-15.40</th>
              </tr>
              <tr>
                <td>วันจันทร์</td>
                @for ($i=11; $i <=18; $i++)
                  @if ($i-10 == 5)
                    <td class="ok">พักเที่ยง</td>
                  @elseif ($i-10 == 8 )
                      <td class="ok"><p>วิชาบูรณาการ</p></td>
                      @else
                      <td class="drop" data-day=1 data-time={{$i-10}}>@if(empty($data[$i]))
                          {{''}}
                      @else
                            {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
                      @endif
                          @endif
                    </td>
                @endfor
              </tr>
              <tr>
              <td>วันอังคาร</td>
              @for ($i=21; $i <=28; $i++)
                @if ($i-20 == 5)
                  <td class="ok">พักเที่ยง</td>
                @elseif ($i-20 == 7)
                    <td class="ok">ชุมนุม</td>
                @elseif ($i-20 == 8 )
                    <td class="ok"><p>วิชาบูรณาการ</p></td>
                    @else
                    <td class="drop" data-day=2 data-time={{$i-20}}>@if(empty($data[$i]))
                        {{''}}
                    @else
                          {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
                    @endif
                  @endif
                  </td>
              @endfor
            </tr>
            <tr>
            <td>วันพุธ</td>
            @for ($i=31; $i <=38; $i++)
              @if ($i-30 == 5)
                <td class="ok">พักเที่ยง</td>
              @elseif ($i-30 == 7 and  $std_display->class == '1/1'  ||  $std_display->class == '1/2'  ||
               $std_display->class == '2/1' ||  $std_display->class == '2/2' ||   $std_display->class =='3/1' ||   $std_display->class =='3/2')
                <td class="ok"><p>ลูกเสือ</p></td>
              @elseif ($i-30 == 8 )
                  <td class="ok"><p>วิชาบูรณาการ</p></td>
                  @else
                  <td class="drop" data-day=3 data-time={{$i-30}}>@if(empty($data[$i]))
                      {{''}}
                  @else
                        {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}

                  @endif
                @endif
                </td>
            @endfor
          </tr>
          <tr>
          <td>วันพฤหัส</td>
          @for ($i=41; $i <=48; $i++)
            @if ($i-40 == 5)
              <td class="ok">พักเที่ยง</td>
            @elseif ($i-40 == 8 )
                <td class="ok"><p>วิชาบูรณาการ</p></td>
                @else
                <td class="drop" data-day=4 data-time={{$i-40}}>@if(empty($data[$i]))
                    {{''}}
                @else
                        {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
                @endif
              @endif
              </td>
          @endfor
        </tr>
        <tr>
        <td>วันศุกร์</td>
        @for ($i=51; $i <=58; $i++)
          @if ($i-50 == 5)
            <td class="ok">พักเที่ยง</td>
          @elseif ($i-50 == 8 )
              <td class="ok"><p>วิชาบูรณาการ</p></td>
              @else
              <td class="drop" data-day=5 data-time={{$i-50}}>@if(empty($data[$i]))
                  {{''}}
              @else
                      {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
              @endif
            @endif
            </td>
        @endfor
      </tr>
      </table>
  </div>
  <div style="margin-left:33%; font-size:32px;">
      รวมเวลาเรียนทั้งสิ้น {{$amount_course}} คาบ
  </div>
  <div class="container">
    <div class="rightlicent">
    <p>  ลงชื่อ.........................................................
      <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(นายธรรมนูญ  มีเสนา)<br>
      &nbsp;&nbsp;&nbsp;&nbsp;ผู้อำนวยการโรงเรียนไทรแก้ววิทยา</p></div>
    <div class="leftlicent">
      ลงชื่อ.........................................................
  <p style="padding-left:1.5em;">(นางสาวเบญญาภา  คนกลาย)<br>หัวหน้ากลุ่มงานบริหารวิชาการ</p></div>
    </div>
  </body>
</html>
