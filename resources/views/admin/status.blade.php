@extends('layout.app')
@section('content')
  <div class="container">
    <a href="{{$url}}" class="btn btn-primary">เพิ่มสถานะ</a><br><br>
      <table class="table table-striped text-center">
      <tr>
        <td>สถานะ</td>
          <td>action</td>
      </tr>
      @foreach ($objs as $row)
        <tr>
          <td>{{$row->status_detail}}</td>
            <td>
                <a href="{{url('admin/status/'.$row->id.'/edit')}}" class="fl btn btn-warning">แก้ไข</a>
              <form  action="{{url('admin/status/'.$row->id)}}" method="post" onsubmit="return(confirm('คุณต้องการลบใช่หรือไม่'))">
                {{method_field('DELETE')}}
                {{csrf_field()}}
                <button type="submit" class="fl btn btn-danger " id="delete">ลบ</button>
              </form>
            </td>
        </tr>
      @endforeach
    </table>
  </div>
@endsection
