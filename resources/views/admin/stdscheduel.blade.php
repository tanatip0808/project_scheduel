@extends('layout.app')
  @section('title')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @endsection
  @section('content')
  <br><br><br><br>
  <form action="{{url('admin/showstdscheduel/')}}" method="get">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="std_group">ชั้น</label>
      <select  name="std_group" id="std_group"   class="selectpicker form-control" required>
          <option value="" disabled selected>กรุณาเลือกชั้น</option>
        @foreach ($stdgroup as $sj)
        <option value="{{$sj->id}}" @if ($std_display->stdid == $sj->id) selected="selected" @endif   >{{'ชั้นมัธมศึกษาปีที่ '.$sj->class}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="year">ปีการศึกษา</label>
      <select id="year" class="selectpicker form-control" name= 'year' required>
        <option value="" disabled selected>กรุณาเลือกปีการศึกษา</option>
        @foreach ($year as $y)
        <option value="{{$y->id}}"  @if ($year_display->yearid == $y->id) selected="selected" @endif >{{'   ภาคการศึกษาที่  ' .$y->term.'/'.$y->year}}</option>
        @endforeach
      </select>
    </div>
  </div><br>
    <button type="submit" class="btn btn-primary">ค้นหา</button>
</form>
    <br>
  <br><br><br>
  <div align="center">
      <h4> ตารางเรียนชั้นมัธยมศึกษาปีที่ {{       $std_display->class  or ''}}</h4><br>
  <h4>  ประจำภาคเรียนที่  {{$year_display->term or ''}}  ปีการศึกษา {{$year_display->year or ''}} </h4>
</div>
  <div class="col-md-12 bg-default">
        <table class="table table-bordered text-center right left" >
            <tr>
              <td>วัน/เวลา</td>
              <td>08.30-09.20</td>
              <td>09.25-10.15</td>
              <td>10.20-11.10</td>
              <td>11.15-12.05</td>
              <td>12.05-12.55</td>
              <td>13.00-13.50</td>
              <td>13.55-14.45</td>
              <td>14.50-15.40</td>
            </tr>
            <tr>
              <td><p>วันจันทร์</p></td>
              @for ($i=11; $i <=18; $i++)
                @if ($i-10 == 5)
                  <td class="ok"><p>พักเที่ยง</p</td>
                @elseif ($i-10 == 8 )
                    <td class="ok"><p>วิชาบูรณาการ</p></td>
                    @else
                    <td class="drop" data-day=1 data-time={{$i-10}}>@if(empty($data[$i]))
                        {{''}}
                    @else
                              <div style="color:{{$color[$i]}}">{{$data[$i]}}<br>{{$datacourse[$i]}}<br>{{$classroom[$i]}}</div>
                    @endif
                        @endif
                  </td>
              @endfor
            </tr>
            <tr>
            <td><p>วันอังคาร</p></td>
            @for ($i=21; $i <=28; $i++)
              @if ($i-20 == 5)
                <td class="ok"><p>พักเที่ยง</p></td>
              @elseif ($i-20 == 7)
                  <td class="ok"><p>ชุมนุม</p></td>
              @elseif ($i-20 == 8 )
                  <td class="ok"><p>วิชาบูรณาการ</p></td>
                  @else
                  <td class="drop" data-day=2 data-time={{$i-20}}>@if(empty($data[$i]))
                      {{''}}
                  @else
                            <div style="color:{{$color[$i]}}">{{$data[$i]}}<br>{{$datacourse[$i]}}<br>{{$classroom[$i]}}</div>
                  @endif
                @endif
                </td>
            @endfor
          </tr>
          <tr>
          <td><p>วันพุธ</p></td>
          @for ($i=31; $i <=38; $i++)
            @if ($i-30 == 5)
              <td class="ok"><p>พักเที่ยง</p</td>
            @elseif ($i-30 == 7 and  $std_display->class == '1/1'  ||  $std_display->class == '1/2'  ||
             $std_display->class == '2/1' ||  $std_display->class == '2/2' ||   $std_display->class =='3/1' ||   $std_display->class =='3/2')
              <td class="ok"><p>ลูกเสือ</p></td>
            @elseif ($i-30 == 8 )
                <td class="ok"><p>วิชาบูรณาการ</p></td>
                @else
                <td class="drop" data-day=3 data-time={{$i-30}}>@if(empty($data[$i]))
                    {{''}}
                @else
                        <div style="color:{{$color[$i]}}">{{$data[$i]}}<br>{{$datacourse[$i]}}<br>{{$classroom[$i]}}</div>

                @endif
              @endif
              </td>
          @endfor
        </tr>
        <tr>
        <td><p>วันพฤหัส</p></td>
        @for ($i=41; $i <=48; $i++)
          @if ($i-40 == 5)
            <td class="ok"><p>พักเที่ยง</p</td>
          @elseif ($i-40 == 8 )
              <td class="ok"><p>วิชาบูรณาการ</p></td>
              @else
              <td class="drop" data-day=4 data-time={{$i-40}}>@if(empty($data[$i]))
                  {{''}}
              @else
                        <div style="color:{{$color[$i]}}">{{$data[$i]}}<br>{{$datacourse[$i]}}<br>{{$classroom[$i]}}</div>
              @endif
            @endif
            </td>
        @endfor
      </tr>
      <tr>
      <td><p>วันศุกร์</p></td>
      @for ($i=51; $i <=58; $i++)
        @if ($i-50 == 5)
          <td class="ok"><p>พักเที่ยง</p</td>
        @elseif ($i-50 == 8 )
            <td class="ok"><p>วิชาบูรณาการ</p></td>
            @else
            <td class="drop" data-day=5 data-time={{$i-50}}>@if(empty($data[$i]))
                {{''}}
            @else
                    <div style="color:{{$color[$i]}}">{{$data[$i]}}<br>{{$datacourse[$i]}}<br>{{$classroom[$i]}}</div>
            @endif
          @endif
          </td>
      @endfor
    </tr>
		</table>
    </div>
    <div style="margin-left:47%;">
      จำนวน {{$amount_course}}  คาบ
    </div>
    <br>
    @if ($amount_course == 0)
      {{ '' }}
      @else
        <form action="{{url('/admin/loadpdf_stdscheduel/')}}" method="get">
          <input type="hidden" name="stdforpdf" value={{$stdforpdf}}>
            <input type="hidden" name="year_use_id" value={{$year_use_id}}>
          <div class="col-md-12 text-center">
            <button type="submit" class="btn btn-primary" formtarget="_blank">พิมพ์ตารางเรียน</button>
          </div>
      </form>
    @endif

@endsection
