@extends('layout.app')
@section('content')

  <div class="container">
    <a href="{{$url}}" class="btn btn-primary">เพิ่มตำแหน่ง</a><br><br>
      <table class="table table-striped text-center">
      <tr>
        <td>ตำแหน่ง</td>
          <td>action</td>
      </tr>
      @foreach ($objs as $row)
        <tr>
          <td>{{$row->position_name}}</td>
            <td>
                <a href="{{url('admin/position/'.$row->id.'/edit')}}" class="fl btn btn-warning">แก้ไข</a>
              <form  action="{{url('admin/position/'.$row->id)}}" method="post" onsubmit="return(confirm('คุณต้องการลบใช่หรือไม่'))">
                {{method_field('DELETE')}}
                {{csrf_field()}}
                <button type="submit" class="fl btn btn-danger " id="delete">ลบ</button>
              </form>
            </td>
        </tr>
      @endforeach
    </table>
  </div>

  {{-- <div class="col-md-2 bg-default">
      <table class="table table-bordered text-center">
          <tr>
            <td><div  class="item">ชัยณรงค์ <br> แสนมี</div></td>
          </tr>
      </table>
  </div>
<div class="col-md-10 bg-primary">.col-md-fgklhfdgdhhollj</div> --}}
@endsection
