@extends('layout.app')
  @section('title')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @endsection
  @section('content')
    <br><br><br><br>
    <form action="{{url('admin/scheduel/create')}}" method="get">
    <div class="form">
      <div class="form-group col-md-3">
        <label for="std_group">ชั้น</label>
        <select  name="std_group" id="std_group"   class="selectpicker form-control" required>
            <option value="" disabled selected>กรุณาเลือกชั้น</option>
          @foreach ($stdgroup as $sj)
          <option value="{{$sj->id}}" >{{' มัธยมศึกษาปีที่ '.$sj->class}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="year">ปีการศึกษา</label>
        <select id="year" class="selectpicker form-control" name= 'year' required>
          <option value="" disabled selected>กรุณาเลือกปีการศึกษา</option>
          @foreach ($year as $y)
          <option value="{{$y->id}}" >{{'   ภาคการศึกษาที่  '   .$y->term.'/'.$y->year}}</option>
          @endforeach
        </select>
      </div>
    </div><br>
      <button type="submit" class="btn btn-primary">ค้นหา</button>
    </form>
      <br>
      <div class="well">
    <h1>กรุณาเลือกชั้นเรียนที่จะจัดตารางเรียน!</h1>
  </div>
  <style>
         .well {
             height: 500px;
             width: 100%;
             background-color: #cccccc;
         }
         .well h1{
           padding-top: 15%;
           padding-left: 25%;
         }
    </style>
@endsection
