@extends('layout.app')
@section('content')
  <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
  <div class="container">
    <form action="{{$url}}" method="POST" id="add_classroom">
        {{method_field($method)}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">ชนิดห้องเรียน</label>
        <div class="col-sm-6">
          <input type="text" class="form-control" name="classroom_type" value="{{$obj->classroom_type or ''}}">
        </div>
      </div><br><br>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-primary">บันทึก</button>    <button type="submit" class="btn btn-danger">ล้าง</button>
        </div>
      </div>
    </form>
  </div>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#add_classroom').bootstrapValidator({
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          classroom_type: {
            validators: {
              notEmpty: {
                message: 'กรุณากรอกชื่อห้อง'
              }
            }
          },
        },
      });
    });
  </script>
@endsection
