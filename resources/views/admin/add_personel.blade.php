@extends('layout.app')
@section('content')
    <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
  <div class="container">
    <h2>เพิ่มบุคลากร</h2>
    <form action="{{$url}}" method="POST" class="form-horizontal" id="add_personel">
        {{method_field($method)}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">ชื่อ</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="fname" value="{{$objs->first_name or ''}}">
          </div>
        </div>
        <div class="form-group">
          <label for="lname" class="col-sm-2 control-label">นามสกุล</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="lname" value="{{$objs->last_name or ''}}">
          </div>
        </div>
        <div class="form-group">
          <label for="tel" class="col-sm-2 control-label">เบอร์โทรศัพท์</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="phone"  placeholder="xx-xxxx-xxxx" value="{{$objs->tel or ''}}">
          </div>
        </div>
        <div class="form-group">
          <label for="position" class="col-sm-2 control-label">ตำแหน่ง</label>
          <div class="col-sm-6">

            <select class="form-control" name="position">
                @foreach ($positions as $sp)
                  <option value="{{$sp->id}}">{{$sp->position_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="status" class="col-sm-2 control-label">สถานะ</label>
          <div class="col-sm-6">
            <select class="form-control" name="status">
                @foreach ($status as $st)
                  <option value="{{$st->id}}">{{$st->status_detail}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="position" class="col-sm-2 control-label">กลุ่มสาระ</label>
          <div class="col-sm-6">
            <select class="form-control" name="subject">
              @foreach ($subject as $sj)
                  <option value="{{$sj->id}}">{{$sj->subject_matter_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">บันทึก</button>    <button type="submit" class="btn btn-danger">ล้าง</button>
          </div>
        </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#add_personel').bootstrapValidator({
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          fname: {
            validators: {
              notEmpty: {
                message: 'กรุณากรอกชื่อ'
              }
            }
          },
          lname: {
            validators: {
              notEmpty: {
                message: 'กรุณากรอกนามสกุล'
              }
            }
          },
          phone: {
            validators: {
             notEmpty: {
                 message: 'กรุณากรอกหมายเลขโทรศัพท์'
             },
             phone: {
                         country: 'TH',
                         message: 'กรุกรุณากรอกหมายเลขโทรศัพท์ให้ครบ'
                     }
                 }
             }
           }
      });
    });
  </script>
@endsection
