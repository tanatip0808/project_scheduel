@extends('layout.app')
@section('content')
  @section('title')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @endsection
  <div class="col-md-10 bg-default">
                <table class="table table-bordered text-center left">
                  @foreach ($instructor as $item)
                    <tr>
                      <td><div class="item" data-id={{$item->id}}>{{$item->courses_name}}<br>{{$item->first_name.'   '.$item->last_name}}</div></td>
                    </tr>
                  @endforeach
              </table>
        <table class="table table-bordered text-center right" >
				<tr>
					<td class="title bg-info">วัน/เวลา</td>
					<td class="title bg-info">วันจันทร์</td>
					<td class="title bg-info">วันอังคาร</td>
					<td class="title bg-info">วันพุธ</td>
					<td class="title bg-info">วันพฤหัสบดี</td>
					<td class="title bg-info">วันศุกร์</td>
				</tr>
				<tr>
					<td class="bg-warning">08:30</td>
          <td class="drop" data-id="1"></td>
					<td class="drop" data-id="2"></td>
					<td class="drop" data-id="3"></td>
					<td class="drop" data-id="4"></td>
					<td class="drop" data-id="5"></td>
				</tr>
				<tr>
					<td class="bg-warning">09:30</td>
          <td class="drop" data-id="6"></td>
					<td class="drop" data-id="7"></td>
					<td class="drop" data-id="8"></td>
					<td class="drop" data-id="9"></td>
					<td class="drop" data-id="10"></td>
				</tr>
				<tr>
					<td class="bg-warning">10:30</td>
          <td class="drop" data-id="11"></td>
					<td class="drop" data-id="12"></td>
					<td class="drop" data-id="13"></td>
					<td class="drop" data-id="14"></td>
					<td class="drop" data-id="15"></td>
				</tr>
				<tr>
					<td class="bg-warning">11:30</td>
          <td class="drop" data-id="16"></td>
					<td class="drop" data-id="17"></td>
					<td class="drop" data-id="18"></td>
					<td class="drop" data-id="19"></td>
					<td class="drop" data-id="20"></td>
				</tr>
        <tr>
					<td class="bg-danger">12:30</td>
					<td class="bg-danger" colspan="5">พักกลางวัน</td>
				</tr>
				<tr>
					<td class="bg-warning">13:30</td>
          <td class="drop" data-id="21"></td>
					<td class="drop" data-id="22"></td>
					<td class="drop" data-id="23"></td>
					<td class="drop" data-id="24"></td>
					<td class="drop" data-id="25"></td>
				</tr>
				<tr>
					<td class="bg-warning">14:30</td>
          <td class="drop" data-id="26"></td>
					<td class="drop" data-id="27"></td>
					<td class="drop" data-id="28"></td>
					<td class="drop" data-id="29"></td>
					<td class="drop" data-id="30"></td>
				</tr>
				<tr>
					<td class="bg-warning">15:30</td>
          <td class="drop" data-id="31"></td>
					<td class="drop" data-id="32"></td>
					<td class="drop" data-id="33"></td>
					<td class="drop" data-id="34"></td>
					<td class="drop" data-id="35"></td>
				</tr>
				<tr>
					<td class="bg-warning">16:30</td>
          <td class="drop" data-id="36"></td>
					<td class="drop" data-id="37"></td>
					<td class="drop" data-id="38"></td>
					<td class="drop" data-id="39"></td>
					<td class="drop" data-id="40"></td>
				</tr>
        <tr>
					<td class="bg-warning">17:30</td>
          <td class="drop" data-id="41"></td>
					<td class="drop" data-id="42"></td>
					<td class="drop" data-id="43"></td>
					<td class="drop" data-id="44"></td>
					<td class="drop" data-id="45"></td>
				</tr>
			</table>
    </div>
@endsection
