@extends('layout.app')
@section('content')
  <div class="container-fluid">
    <form action="{{$url}}" method="POST">
        {{method_field($method)}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="form-group">
        <label for="position_name">ตำแหน่ง</label>
        <input type="text" class="form-control"  name="position_name" value="{{$obj->position_name or ''}}">
      </div>
      <button type="submit" class="btn btn-success">ตกลง</button>  <button type="reset" class="btn btn-danger">ล้าง</button>
    </form>
  </div>
@endsection
