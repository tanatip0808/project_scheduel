@extends('layout.app')
@section('content')
  @if (session('success'))
          <div class="alert alert-success">
              {{ session('success') }}
          </div>
      @endif
      @if (session('error'))
              <div class="alert alert-danger">
                  {{ session('error') }}
              </div>
          @endif
  <script type="text/javascript">
  window.setTimeout(function() {
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
}, 4000);
  </script>
  <div class="container">
    <a href="{{$url}}" class="btn btn-primary">เพิ่มรายวิชา</a><br><br>
    <div class="col-lg-6">
  <form  action="{{url('/admin/showcourses')}}" method="get">
          <div class="input-group">
            <select class="form-control" name="subject">
                @foreach ($subject as $sj)
                <option value="{{$sj->smid}}"  @if($subject_display->smid == $sj->smid) selected="selected" @endif>{{$sj->sname}}</option>
                @endforeach
            </select>
          <span class="input-group-btn">
            <button type="submit" class="btn btn-success" >
              ค้นหา
            </button>
          </span>
          <span class="input-group-btn">
              <a href="{{url('/admin/courses/')}}" class="fl btn btn-primary">ดูทั้งหมด</a>
          </span>
</form>
      </div><br>
    </div>
      <table class="table table-striped text-center">
      <tr>
        <td>รหัสวิชา</td>
          <td>ชื่อวิชา</td>
          <td>action</td>
      </tr>
      @foreach ($objs as $row)
        <tr>
          <td>{{$row->courses_id}}</td>
            <td>{{$row->courses_name}}</td>
            <td>
                <a href="{{url('/admin/courses/'.$row->cid.'/edit')}}" class="fl btn btn-warning">แก้ไข</a>
              <form  action="{{url('/admin/courses/'.$row->cid)}}" method="post" onsubmit="return(confirm('คุณต้องการลบใช่หรือไม่'))">
                {{method_field('DELETE')}}
                {{csrf_field()}}
                <button type="submit" class="fl btn btn-danger " id="delete">ลบ</button>
              </form>
            </td>
        </tr>
      @endforeach
    </table>
  </div>
  {!! $objs->appends(Request::all())->render() !!}
  {{-- <div class="col-md-2 bg-default">
      <table class="table table-bordered text-center">
          <tr>
            <td><div  class="item">ชัยณรงค์ <br> แสนมี</div></td>
          </tr>
      </table>
  </div>
<div class="col-md-10 bg-primary">.col-md-fgklhfdgdhhollj</div> --}}
@endsection
