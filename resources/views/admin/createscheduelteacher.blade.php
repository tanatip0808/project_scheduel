@extends('layout.app')

  @section('title')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @endsection
  @section('content')
  <br><br><br><br>
  <form action="{{url('/admin/createscheduelteacher/create')}}" method="get">
  <div class="form-row">
    <div class="form-group col-md-3">
          <label for="std_group">ครูผู้สอน</label>
      <select   name="teacher_id"  class="form-control" required>
        <option value="" disabled selected>กรุณาเลือกครูผู้สอน</option>
        @foreach ($teacher as $tc)
        <option value="{{$tc->id}}"  @if ($teacher_display->teacherid == $tc->id) selected="selected" @endif>{{$tc->first_name.'&nbsp;&nbsp;&nbsp;'.$tc->last_name}} </option>
        @endforeach
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="year">ปีการศึกษา</label>
      <select id="year" name= 'year' class="selectpicker form-control" required>
          <option value="" disabled >กรุณาเลือกปีการศึกษา</option>
        @foreach ($year as $y)
        <option value="{{$y->id}}" @if ($year_display->yearid == $y->id) selected="selected" @endif>{{'   ภาคการศึกษาที่  ' .$y->term.'/'.$y->year}}</option>
        @endforeach
      </select>
    </div>
  </div><br>
    <button type="submit" class="btn btn-primary">ค้นหา</button>
</form>
    <br>
    <div align="center">
        <h4> ตารางสอนของคุณครู    {{$teacher_display->first_name.' '.$teacher_display->last_name }}</h4><br>
    <h4>  ประจำภาคเรียนที่  {{$year_display->term or ''}}  ปีการศึกษา {{$year_display->year or ''}} </h4>
</div>
  <div class="col-md-12 bg-default">
@include('admin/flash-message')
                <table class="table table-bordered text-center left" id="item"  >
                    <tr>
                        @foreach ($instructor as $item)
                            @for ($i=0; $i < ($item->times_study - $item->check_times_study); $i++)
                    @if ($p<10)
                      <td>
                        <div style="background:{{$item->color}};" class="item" id={{$item->id}}  data-id={{$item->id}}
                          data-classroom={{$item->classid}}  data-year={{$item->yearid}} data-tchid={{$item->tchid}} data-times_study={{$item->check_times_study}}
                           data-stdid={{$item->stdid}}>
                        {{$item->courses_name}}<br>{{$item->class}}<br>
                        {{ $item->classroom !='ปกติ' ? $item->classroom : ' ' }}
                        {{ $item->coach != null ?  $item->coach : ' ' }}
                      </div>
                       </td>
                       @php
                             $p=$p+1
                           @endphp
                       @else

                    @if ($p==10 || $p== 20)
                    </tr>
                    <tr>
                    <td>
                      <div style="background:{{$item->color}};" class="item" id={{$item->id}}  data-id={{$item->id}}
                        data-classroom={{$item->classid}}  data-year={{$item->yearid}} data-tchid={{$item->tchid}} data-times_study={{$item->check_times_study}}
                         data-stdid={{$item->stdid}}>
                      {{$item->courses_name}}<br>{{$item->class}}<br>
                      {{ $item->classroom !='ปกติ' ? $item->classroom : ' ' }}
                      {{ $item->coach != null ?  $item->coach : ' ' }}
                    </div>
                     </td>
                     @php
                           $p=$p+1
                         @endphp
                     @else
                       <td>
                         <div style="background:{{$item->color}};" class="item" id={{$item->id}}  data-id={{$item->id}}
                           data-classroom={{$item->classid}}  data-year={{$item->yearid}} data-tchid={{$item->tchid}} data-times_study={{$item->check_times_study}}
                            data-stdid={{$item->stdid}}>
                         {{$item->courses_name}}<br>{{$item->class}}<br>
                         {{ $item->classroom !='ปกติ' ? $item->classroom : ' ' }}
                         {{ $item->coach != null ?  $item->coach : ' ' }}
                       </div>
                        </td>
                        @php
                              $p=$p+1
                            @endphp

                        @if ($p==20)

                        @endif
                    @endif
                    @endif

                         @endfor
                         @endforeach
                    </tr>
                  </table>
        <table class="table table-bordered text-center right left" id="table" >
            <tr>
              <td>วัน/เวลา</td>
              <td>08.30-09.20</td>
              <td>09.25-10.15</td>
              <td>10.20-11.10</td>
              <td>11.15-12.05</td>
              <td>12.30-12.55</td>
              <td>13.00-13.50</td>
              <td>13.55-14.45</td>
              <td>14.50-15.40</td>
            </tr>
            <tr>
              <td>วันจันทร์</td>
              @for ($i=11; $i <=18; $i++)
                @if ($i-10 == 5)
                  <td class="ok">พักเที่ยง</td>
                @elseif ($i-10 == 8 )
                    <td class="ok"><p>วิชาบูรณาการ</p></td>
                  @else
                    <td class="drop" data-day=1 data-time={{$i-10}}>@if(empty($data[$i]))
                        {{''}}
                              <div class="alert-danger" id={{'m'.$i}} hidden></div>
                              <div class="alert-danger" id={{'n'.$i}} hidden></div>
                    @else
                      <div class="itemadd" style="background:{{$color[$i]}}; "  data-idsch={{$idsch[$i]}} data-dayitem={{$day[$i]}} data-insid ={{$insid[$i]}}
                      data-classroom={{$class_type_id[$i]}}   data-year={{$yearid[$i]}} data-tchid={{$tchid[$i]}}
                     data-timeitem={{$time[$i]}}>
                     <span class="glyphicon glyphicon-remove"style="float:right; cursor: pointer;" aria-hidden="true"    data-idsch={{$idsch[$i]}}  data-insid ={{$insid[$i]}}></span>
                     {{$datacourse[$i]}}<br>{{$stdgroup[$i]}}<br>
                     {{$classroom[$i] !='ปกติ' ?  $classroom[$i] : '' }}
                     {{$coach[$i] != null ?  $coach[$i] : ' ' }}
                    </div>
                        <div class="alert-danger" id={{'m'.$i}} hidden></div>
                        <div class="alert-danger" id={{'n'.$i}} hidden></div>
                    @endif
                  @endif
                  </td>
              @endfor
            </tr>
            <tr>
            <td>วันอังคาร</td>
            @for ($i=21; $i <=28; $i++)
              @if ($i-20 == 5)
                <td class="ok">พักเที่ยง</td>
              @elseif ($i-20 == 7)
                  <td class="ok">ชุมนุม</td>
                @elseif ($i-20 == 8 )
                    <td class="ok"><p>วิชาบูรณาการ</p></td>
                @else
                  <td class="drop" data-day=2 data-time={{$i-20}}>@if(empty($data[$i]))
                      {{''}}
                          <div class="alert-danger" id={{'m'.$i}} hidden></div>
                          <div class="alert-danger" id={{'n'.$i}} hidden></div>
                  @else
                          <div class="itemadd" style="background:{{$color[$i]}};"  data-idsch={{$idsch[$i]}} data-dayitem={{$day[$i]}} data-insid ={{$insid[$i]}}
                          data-classroom={{$class_type_id[$i]}}   data-year={{$yearid[$i]}} data-tchid={{$tchid[$i]}}
                         data-timeitem={{$time[$i]}}>
                       <span class="glyphicon glyphicon-remove"style="float:right; cursor: pointer;" aria-hidden="true"    data-idsch={{$idsch[$i]}}  data-insid ={{$insid[$i]}}></span>
                         {{$datacourse[$i]}}<br>{{$stdgroup[$i]}}<br>
                         {{$classroom[$i] !='ปกติ' ?  $classroom[$i] : '' }}
                         {{$coach[$i] != null ?  $coach[$i] : ' ' }}
                       </div>
                            <div class="alert-danger" id={{'m'.$i}} hidden></div>
                            <div class="alert-danger" id={{'n'.$i}} hidden></div>
                  @endif
                @endif
                </td>
            @endfor
          </tr>
          <tr>
          <td>วันพุธ</td>
          @for ($i=31; $i <=38; $i++)
            @if ($i-30 == 5)
              <td class="ok">พักเที่ยง</td>
            @elseif ($i-30 == 7)
              <td class="ok">ลูกเสือ</td>
            @elseif ($i-30 == 8 )
                <td class="ok"><p>วิชาบูรณาการ</p></td>
              @else
                <td class="drop" data-day=3 data-time={{$i-30}}>@if(empty($data[$i]))
                    {{''}}
                        <div class="alert-danger" id={{'m'.$i}} hidden></div>
                        <div class="alert-danger" id={{'n'.$i}} hidden></div>
                @else
                        <div class="itemadd" style="background:{{$color[$i]}};" data-idsch={{$idsch[$i]}} data-dayitem={{$day[$i]}} data-insid ={{$insid[$i]}}
                        data-classroom={{$class_type_id[$i]}} data-year={{$yearid[$i]}} data-tchid={{$tchid[$i]}}
                     data-timeitem={{$time[$i]}}>
                   <span class="glyphicon glyphicon-remove"style="float:right; cursor: pointer;" aria-hidden="true"    data-idsch={{$idsch[$i]}}  data-insid ={{$insid[$i]}}></span>
                     {{$datacourse[$i]}}<br>{{$stdgroup[$i]}}<br>
                     {{$classroom[$i] !='ปกติ' ?  $classroom[$i] : '' }}
                     {{$coach[$i] != null ?  $coach[$i] : ' ' }}
                   </div>
                            <div class="alert-danger" id={{'m'.$i}} hidden></div>
                            <div class="alert-danger" id={{'n'.$i}} hidden></div>
                      @endif
                @endif
              </td>
          @endfor
        </tr>
        <tr>
        <td>วันพฤหัส</td>
        @for ($i=41; $i <=48; $i++)
          @if ($i-40 == 5)
            <td class="ok">พักเที่ยง</td>
          @elseif ($i-40 == 8 )
              <td class="ok"><p>วิชาบูรณาการ</p></td>
            @else
              <td class="drop" data-day=4 data-time={{$i-40}}>@if(empty($data[$i]))
                    <div class="alert-danger" id={{'m'.$i}} hidden></div>
                    <div class="alert-danger" id={{'n'.$i}} hidden></div>
                  {{''}}
              @else
                      <div class="itemadd" style="background:{{$color[$i]}};" data-idsch={{$idsch[$i]}} data-dayitem={{$day[$i]}} data-insid ={{$insid[$i]}}
                      data-classroom={{$class_type_id[$i]}}  data-year={{$yearid[$i]}} data-tchid={{$tchid[$i]}}
                     data-timeitem={{$time[$i]}}>
                     <span class="glyphicon glyphicon-remove"style="float:right; cursor: pointer;" aria-hidden="true"    data-idsch={{$idsch[$i]}}  data-insid ={{$insid[$i]}}></span>
                     {{$datacourse[$i]}}<br>{{$stdgroup[$i]}}<br>
                     {{$classroom[$i] !='ปกติ' ?  $classroom[$i] : '' }}
                     {{$coach[$i] != null ?  $coach[$i] : ' ' }}
                   </div>
                      <div class="alert-danger" id={{'m'.$i}} hidden></div>
                      <div class="alert-danger" id={{'n'.$i}} hidden></div>
              @endif
            @endif
            </td>
        @endfor
      </tr>
      <tr>
      <td>วันศุกร์</td>
      @for ($i=51; $i <=58; $i++)
        @if ($i-50 == 5)
          <td class="ok">พักเที่ยง</td>
        @elseif ($i-50 == 8 )
            <td class="ok"><p>วิชาบูรณาการ</p></td>
          @else
            <td class="drop" data-day=5 data-time={{$i-50}}>@if(empty($data[$i]))
                {{''}}
                  <div class="alert-danger" id={{'m'.$i}} hidden></div>
                  <div class="alert-danger" id={{'n'.$i}} hidden></div>
            @else
                    <div class="itemadd" style="background:{{$color[$i]}};" data-idsch={{$idsch[$i]}} data-dayitem={{$day[$i]}} data-insid ={{$insid[$i]}}
                    data-classroom={{$class_type_id[$i]}}  data-year={{$yearid[$i]}} data-tchid={{$tchid[$i]}}
                     data-timeitem={{$time[$i]}}>
                   <span class="glyphicon glyphicon-remove"style="float:right; cursor: pointer;" aria-hidden="true"    data-idsch={{$idsch[$i]}}  data-insid ={{$insid[$i]}}></span>
                     {{$datacourse[$i]}}<br>{{$stdgroup[$i]}}<br>
                     {{$classroom[$i] !='ปกติ' ?  $classroom[$i] : '' }}
                     {{$coach[$i] != null ?  $coach[$i] : ' ' }}
                   </div>
                      <div class="alert-danger" id={{'m'.$i}} hidden></div>
                      <div class="alert-danger" id={{'n'.$i}} hidden></div>
            @endif
          @endif
          </td>
      @endfor
    </tr>
		</table>
  </div>
  <form action="{{url('/admin/scheduel/reset/resettch')}}" method="GET">
    <input type="hidden" name="teacher_id" value='{{$teacher_id}}'/>
    <input type="hidden" name="yearid" value='{{$year_use_id}}'/>
    <button type="submit" class="btn btn-danger" style="margin-left:50%;">ล้างข้อมูล</button>
    <br><br>
  </form>
@endsection
