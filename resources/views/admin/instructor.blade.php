@extends('layout.app')
@section('content')

<div class="container">
<div class="col-lg-6">
  <form  action="{{url('admin/showinstructor/')}}" method="get">
      <div class="input-group">
        <select class="form-control" name="course">
            @foreach ($subject as $sj)
            <option value="{{$sj->id}}">{{$sj->subject_matter_name}}</option>
            @endforeach
        </select>
      <span class="input-group-btn">
        <button type="submit" class="btn btn-default" >
          ค้นหา
        </button>
      </span>
      </form>
  </div>
</div>
<br><br>
      <table class="table table-striped text-center">
      <tr>
        <td>รหัสวิชา</td>
        <td>ชื่อวิชา</td>
        <td>หน่วยกิต</td>
        <td>เวลาเรียน</td>
        <td>เวลาเรียนรวม</td>
        <td>อาจารย์ผู้สอน</td>
        <td>ห้องเรียน</td>
        <td>กลุ่มเรียน</td>
        <td>หลักสูตร</td>
        <td>ปีการศึกษา</td>
      </tr>
      @foreach ($instructor as $row)
        <tr>
          <td>{{$row->courses_id or ''}}</td>
          <td>{{$row->courses_name or ''}}</td>
          <td>{{$row->credit or ''}}</td>
          <td>{{$row->times_study or ''}}</td>
          <td>{{$row->scheduel_times or ''}}</td>
          <td>{{$row->first_name or ''}}</td>
          <td>{{$row->classroom_type or ''}}</td>
          <td>{{$row->class or ''}}</td>
          <td>{{$row->study_course_name or ''}}</td>
          <td>{{$row->year or ''}}</td>
        </tr>
      @endforeach
    </table>
  </div>
@endsection
