<style media="screen">
#success {
		 background-color: rgb(204, 255, 204);
    position: fixed;
		color: #003300;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
		text-align: center;
}
#error {
		background-color: rgb(255, 179, 179);
    position: fixed;
		color: #cc0000;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
		text-align: center;
}
#update {
		 background-color: rgb(204, 255, 204);
    position: fixed;
		color: #003300;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
		text-align: center;
}
</style>

<div class="alert alert-success alert-block col-md-4 " id="success" hidden>
	<button type="button" class="close" data-dismiss="alert">×</button>
        <p class="text-center text-success"></p>
</div>
<div class="alert alert-danger alert-block col-md-4 " id="error" hidden>
	<button type="button" class="close" data-dismiss="alert">×</button>
        <p class="text-center text-danger"></p>
</div>
<div class="alert alert-success alert-block col-md-4 " id="update" hidden>
	<button type="button" class="close" data-dismiss="alert">×</button>
        <p class="text-center text-success"></p>
</div>
