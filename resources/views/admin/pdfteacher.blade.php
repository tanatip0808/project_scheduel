<!DOCTYPE html>
<html>
<title>ตารางสอนของ คุณครู  {{$personellabel->first_name or ''}}</title>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/scrip.js?v=<?php echo rand(); ?>"></script>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }
        @font-face {
          font-family: 'THSarabunNew';
          font-style: normal;
          font-weight: bold;
          src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: normal;
          src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
      }
      @font-face {
          font-family: 'THSarabunNew';
          font-style: italic;
          font-weight: bold;
          src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
      }
      table {
          width: 100%;
          border: 2px solid black;
      }

      td,
      th {
          border: 1px solid black;
      }
        body {
            font-family: "THSarabunNew";
        }
        .container {
   height: auto;
   overflow: hidden;
}

.rightlicent {
    float: right;
}

.leftlicent {
    float: none; /* not needed, just for clarification */
    /* the next props are meant to keep this block independent from the other floated one */
    width: auto;

}
    </style>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  <body>
    <div align="center">
        <b><font size="6">ตารางสอนของครู {{$personellabel->first_name }} {{$personellabel->last_name }}</font></b><br>
        <p><font size="5">ประจำภาคเรียนที่ {{$termyear->term}}   ปีการศึกษา {{$termyear->year}}</font><p>
        <p><font size="4">โรงเรียนไทรแก้ววิทยา  ตำบลกันตวจระมวล  อำเภอปราสาท  จังหวัดสุรินทร์</font></p>
    </div>
    <div class="col-md-12 bg-default">
          <table class="table table-bordered text-center right left" >
              <tr>
                <td>วัน/เวลา</td>
                <td>08.30-09.20</td>
                <td>09.25-10.15</td>
                <td>10.20-11.10</td>
                <td>11.15-12.05</td>
                <td>12.05-12.55</td>
                <td>13.00-13.50</td>
                <td>13.55-14.45</td>
                <td>14.50-15.40</td>
              </tr>
              <tr>
                <td>วันจันทร์</td>
                @for ($i=11; $i <=18; $i++)
                  @if ($i-10 == 5)
                    <td class="ok">พักเที่ยง</td>
                  @elseif ($i-10 == 8 )
                      <td class="ok"><p>วิชาบูรณาการ</p></td>
                      @else
                      <td class="drop" data-day=1 data-time={{$i-10}}>@if(empty($data[$i]))
                          {{''}}
                      @else
                            {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
                      @endif
                          @endif
                    </td>
                @endfor
              </tr>
              <tr>
              <td>วันอังคาร</td>
              @for ($i=21; $i <=28; $i++)
                @if ($i-20 == 5)
                  <td class="ok">พักเที่ยง</td>
                @elseif ($i-20 == 7)
                    <td class="ok">ชุมนุม</td>
                @elseif ($i-20 == 8 )
                    <td class="ok"><p>วิชาบูรณาการ</p></td>
                    @else
                    <td class="drop" data-day=2 data-time={{$i-20}}>@if(empty($data[$i]))
                        {{''}}
                    @else
                          {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
                    @endif
                  @endif
                  </td>
              @endfor
            </tr>
            <tr>
            <td>วันพุธ</td>
            @for ($i=31; $i <=38; $i++)
              @if ($i-30 == 5)
                <td class="ok">พักเที่ยง</td>
              @elseif ($i-30 == 8 )
                  <td class="ok"><p>วิชาบูรณาการ</p></td>
                  @else
                  <td class="drop" data-day=3 data-time={{$i-30}}>@if(empty($data[$i]))
                      {{''}}
                  @else
                        {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}

                  @endif
                @endif
                </td>
            @endfor
          </tr>
          <tr>
          <td>วันพฤหัส</td>
          @for ($i=41; $i <=48; $i++)
            @if ($i-40 == 5)
              <td class="ok">พักเที่ยง</td>
            @elseif ($i-40 == 8 )
                <td class="ok"><p>วิชาบูรณาการ</p></td>
                @else
                <td class="drop" data-day=4 data-time={{$i-40}}>@if(empty($data[$i]))
                    {{''}}
                @else
                        {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
                @endif
              @endif
              </td>
          @endfor
        </tr>
        <tr>
        <td>วันศุกร์</td>
        @for ($i=51; $i <=58; $i++)
          @if ($i-50 == 5)
            <td class="ok">พักเที่ยง</td>
          @elseif ($i-50 == 8 )
              <td class="ok"><p>วิชาบูรณาการ</p></td>
              @else
              <td class="drop" data-day=5 data-time={{$i-50}}>@if(empty($data[$i]))
                  {{''}}
              @else
                      {{$course_id[$i]}}<br>{{$data[$i]}}<br>{{$classroom[$i]}}
              @endif
            @endif
            </td>
        @endfor
      </tr>
      </table>
  </div>
      <div class="col-md-12 text-center">
          รวมเวลาเรียนทั้งสิ้น {{$amount_course}} คาบ
      </div>
      <div class="container">
        <div class="rightlicent">
          ลงชื่อ.........................................................
          <br>(นายธรรมนูญ  มีเสนา)<br>ผู้อำนวยการโรงเรียนไทรแก้ววิทยา</div>
        <div class="leftlicent">
          ลงชื่อ.........................................................
      <br>(นางสาวเบญญาภา  คนกลาย)<br>หัวหน้ากลุ่มงานบริหารวิชาการ</div>
        </div>
  </body>
</html>
