@extends('layout.app')
@section('content')
    <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
  <div class="container">
    <h2>เพิ่มรายวิชา</h2>
    <form action="{{$url}}" method="POST" class="form-horizontal" id="add_personel">
        {{method_field($method)}}
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">รหัสวิชา</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="courses_id" value="{{$objs->courses_id or ''}}">
          </div>
        </div>
        <div class="form-group">
          <label for="lname" class="col-sm-2 control-label">ชื่อวิชา</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="courses_name" value="{{$objs->courses_name or ''}}">
          </div>
        </div>
        <div class="form-group">
          <label for="position" class="col-sm-2 control-label">กลุ่มสาระ</label>
          <div class="col-sm-6">
            <select  class="selectpicker form-control" required name="subject" >
                <option value="">กรุณาเลือกกลุ่มสาระ</option>
              @foreach ($subject as $sj)
                  <option value="{{$sj->id}}">{{$sj->sname}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">บันทึก</button>    <button type="submit" class="btn btn-danger">ล้าง</button>
          </div>
        </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function () {
      $('#add_personel').bootstrapValidator({
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          courses_id: {
            validators: {
              notEmpty: {
                message: 'กรุณากรอกรหัสวิชา'
              }
            }
          },
          courses_name: {
            validators: {
              notEmpty: {
                message: 'กรุณากรอกชื่อวิชา'
                    }
                }
              }
           }
      });
    });
  </script>
@endsection
