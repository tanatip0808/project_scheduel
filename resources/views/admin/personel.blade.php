@extends('layout.app')
@section('content')

  <div class="container">
  <a href="{{$url}}" class="btn btn-primary">เพิ่มบุคลากร</a>
<div class="col-lg-6">
  <form  action="{{url('admin/showpersonel')}}" method="get">
      <div class="input-group">
        <select class="form-control" name="subject">
            @foreach ($subject as $sj)
            <option value="{{$sj->subid}}">{{$sj->sname}}</option>
            @endforeach
        </select>
      <span class="input-group-btn">
        <button type="submit" class="btn btn-default" >
          ค้นหา
        </button>
      </span>
      </form>
  </div>
</div>
<br><br>
      <table class="table table-striped text-center">
      <tr>
        <td>ชื่อ</td>
        <td>นามสกุล</td>
        <td>เบอร์โทรศัพท์</td>
        <td>ตำแหน่ง</td>
        <td>สถานะ</td>
        <td>กลุ่มสาระ</td>
        <td>action</td>
      </tr>
      @foreach ($users as $row)
        <tr>
          <td>{{$row->first_name}}</td>
          <td>{{$row->last_name}}</td>
          <td>{{$row->tel}}</td>
          <td>{{$row->position_name}}</td>
          <td>{{$row->status_detail}}</td>
          <td>{{$row->subject_matter_name}}</td>
            <td>
                <a href="{{url('admin/personel/'.$row->id.'/edit')}}" class="fl btn btn-warning">แก้ไข</a>
              <form  action="{{url('admin/personel/'.$row->id)}}" method="post" onsubmit="return(confirm('คุณต้องการลบใช่หรือไม่'))">
                {{method_field('DELETE')}}
                {{csrf_field()}}
                <button type="submit" class="fl btn btn-danger " id="delete">ลบ</button>
              </form>
            </td>
        </tr>
      @endforeach
    </table>
  </div>
@endsection
