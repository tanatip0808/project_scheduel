@extends('layout.sub_matter') @section('content')

<div class="container col-md-11">
    <h3 class="glyphicon glyphicon-user divs-left"> รายชื่ออาจารย์ภายในกลุ่มสาระ{{$subject_matters_name->subject_matter_name}}</h3>
    <div class="form-group">
    </div>
    @if($count_personels=='0') @else
    <a class="btn btn-danger" href="{!! URL::to('/sub_mat/loadPdf_personels') !!}" style="float:right;" target="_blank">
        <span class="glyphicon glyphicon-duplicate" id="btnsss"></span> ไฟล์ Pdf
    </a>
    @endif @if($count_personels=='0')
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1 class="tablesss" style="color:#c1bbbb;" id="btns">ไม่มีรายชื่อบุคคลากร
    </h1>
    @else
    <table class="table table-bordered ">
        <thead style="background-color:#EEEEEE;">
            <tr>
                <th style="width:170px;">
                    <span class="glyphicon glyphicon-user"></span> ชื่อ-นามสกุล</th>
                <th style="width:90px;">
                    <span class="glyphicon glyphicon-phone"></span> เบอร์โทรศัพท์</th>
                <th style="width:90px;">
                    <span class="glyphicon glyphicon-education"></span> ตำเเหน่ง</th>
                <th style="width:125px;">
                    <span class="glyphicon glyphicon-folder-close"></span> กลุ่มสาระ</th>
                <th style="width:75px;">
                    <span class="glyphicon glyphicon-user"></span> สถานะ</th>
            </tr>
        </thead>
        <tbody>
            @foreach($view_personels as $personels1)
            <tr>
                <td>
                    {{ $personels1->first_name.' '.$personels1->last_name }}
                </td>
                <td>
                    {{ $personels1->tel }}
                </td>
                <td>
                    {{ $personels1->position_name }}
                </td>
                <td>
                    {{ $personels1->subject_matter_name }}
                </td>
                <td>
                    {{ $personels1->status_detail }}
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
    @endif
    <div class="tablesss">
        {!! $view_personels->appends(Request::all())->render() !!}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var message = $('#btnsss');
        message.show('slow');
    });
    $(document).ready(function () {
        var message = $('#btns');
        message.show('slow');
    });
</script>

@stop
