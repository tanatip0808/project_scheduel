@extends('layout.sub_matter') @section('content')
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

<div class="container col-md-11">
  <h3 class="glyphicon glyphicon-list-alt divs-left"> รายวิชา</h3>
  {!! Form::open(['url' => 'sub_mat/search/'.$year_studies]) !!}
  <div class="form-group">
    <select class="form-control divss" name="id" style="width:220px;" required>
      <option value="" disabled selected>เลือกอาจารย์...</option>
      @foreach($personelss as $personel11)
      <option value="{{ $personel11->id }}">{{$personel11->first_name.' '.$personel11->last_name}}</option>
      @endforeach
    </select>
  </div>
  <button class="btn btn-info" type="submit">
    <span class="glyphicon glyphicon-search"></span> ค้นหา
  </button>
  @if($count_instructors_personels=='0') @else
  <a class="btn btn-danger" href="{!! URL::to('/sub_mat/loadPdf_instructor_personels/'.$personel_pdf,$year_studies) !!}" style="float:right;" target="_blank"> 
    <span class="glyphicon glyphicon-duplicate" id="btnsss"></span> ไฟล์ Pdf
  </a>
  @endif 
  @if($personel_pdf!='0')
  <a class="btn btn-primary" href="{!! URL::to('/sub_mat/view_instructor'.$year_studies) !!}" style="float:right;margin-right:5px;" id="msg1">
    <span class="glyphicon glyphicon-eye-open" id="btns"></span> ดูรายวิชาทั้งหมด
    <span id="nbrmsgg" class="badge">{{ $count_instructors.' วิชา' }}</span>
  </a>
  <a class="btn btn-outline-light" style="float:right;margin-right:5px;" id="msg" disabled>
    <span class="glyphicon glyphicon-list-alt" id="btnss"></span> วิชาที่สอน
    <span id="nbrmsg" class="badge">{{ $count_instructors_personels.' วิชา' }}</span>
  </a>  
  <a class="btn btn-outline-light" style="float:right;" disabled>
    <span class="glyphicon glyphicon-calendar"></span> ปีการศึกษา {{ $year_name->year.' / '.$year_name->term }}
  </a>
  @endif {!! Form::close() !!} @if($count_instructors_personels=='0')
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <h1 class="tablesss" style="color:#c1bbbb;" id="btnssss">ไม่มีรายวิชาของ อาจารย์ {{ $show_personels->first_name.' '.$show_personels->last_name }} 
  </h1>
  @else
  <table class="table table-bordered ">
    <thead style="background-color:#EEEEEE;">
      <tr>
      <th style="width:62px;">
          <span class="glyphicon glyphicon-list-alt"></span> รหัสวิชา</th>
        <th style="width:150px;">
          <span class="glyphicon glyphicon-list-alt"></span> ชื่อวิชา</th>
        <th style="width:110px;">
          <span class="glyphicon glyphicon-user"></span> อาจารย์</th>
        <th style="width:100px;">
          <span class="glyphicon glyphicon-user"></span> นักศึกษาฝึกสอน</th>
        <th style="width:70px;">
          <span class="glyphicon glyphicon-time"></span> เวลาเรียน</th>
        <th style="width:69px;">
          <span class="glyphicon glyphicon-user"></span> กลุ่มเรียน</th>
        <th style="width:55px;">
          <span class="glyphicon glyphicon-home"></span> ห้อง</th>
        <th style="width:90px;">
          <span class="glyphicon glyphicon-question-sign"></span> จัดการ</th>
      </tr>
    </thead>
    <tbody>
      @foreach($view_instructors as $instructor111)
      <tr>
      <td>
          {{ $instructor111->courses_id }}
        </td>
        <td>
          {{ $instructor111->courses_name }}
        </td>
        <td>
          {{ $instructor111->first_name.' '.$instructor111->last_name }}
        </td>
        <td>
          {{ $instructor111->coach != null ? $instructor111->coach : 'ไม่มี' }}
        </td>
        <td>
          {{ $instructor111->times_study }}
        </td>
        <td>
          {{ $instructor111->class }}
        </td>
        <td>
          {{ $instructor111->classroom_type }}
        </td>
        <td style="text-align: center;">
          <button type="button" class="btn btn-warning btn-edit" data-toggle="modal" data-target=".bd-example-modal-sm" data-id="{{ $instructor111->id }}">
            <span class="glyphicon glyphicon-edit"></span> เเก้ไข
          </button>
        </td>
      </tr>
    </tbody>
    @endforeach
  </table>
  @endif
  <div class="tablesss">
    {!! $view_instructors->appends(Request::all())->render() !!}
  </div>
</div>

<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" style="width:390px;">
      <div class="modal-header">
        <h3 class="modal-title glyphicon glyphicon-edit"> เเก้ไขรายวิชา</h3>
      </div>
      <form action="{{ URL::to('/sub_mat/update'.$year_studies) }}" method="post" id="viewEditInstructor" name="viewEditInstructor">
        {!! csrf_field() !!}
        <div class="modal-body">
         <input id="id" name="id" type="text" hidden>
         <input id="ids" name="ids" type="text" hidden>         
          <input id="personels_id"  type="text" hidden>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>รหัสวิชา : </span>
              </span>
              <input id="courses_id" name="courses_id" type="text" class="form-control" placeholder="รหัสวิชา" style="width:280px;" readonly>
            </div>
          </div>         
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>ชื่อวิชา : </span>
              </span>
              <input id="courses_name" name="courses_name" type="text" class="form-control" placeholder="ชื่อวิชา" style="width:287px;" readonly>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>เวลาเรียน : </span>
              </span>
              <input id="times_study" name="times_study" type="text" class="form-control" placeholder="เวลาเรียน" style="width:272px;" readonly>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>กลุ่มเรียน : </span>
              </span>
              <input id="student_groups_id" name="student_groups_id" type="text" class="form-control" placeholder="ชั้น" style="width:274px;" readonly>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>อาจารย์ : </span>
              </span>
              <select class="form-control" name="personels_id" style="width:284px;">
              <option value="" disabled selected>---กรุณาเลือกอาจารย์---</option>
              @foreach($personels as $personel0)
                <option value="{{ $personel0->id }}">{{$personel0->first_name.' '.$personel0->last_name}}</option>
              @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>นักศึกษาฝึกงาน : </span>
              </span>
              <input id="coach" name="coach" type="text" class="form-control" placeholder="ไม่มี" style="width:237px;">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>ห้อง : </span>
              </span>
              <select class="form-control" name="classrooms_id" style="width:303px;">
              <option value="" disabled selected>---กรุณาเลือกห้อง---</option>
              @foreach($classrooms as $classrooms0)
                <option value="{{ $classrooms0->id }}">{{$classrooms0->classroom_type}}</option>
              @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-danger btn-reset" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
          <button type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-ok"></span> ตกลง</button>
      </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    var message = $('#nbrmsg');
    $('#msg').append(message);
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#nbrmsgg');
    $('#msg1').append(message);
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#btns');
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#btnss');
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#btnsss');
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#btnssss');
    message.show('slow');
  });
  $('tbody').delegate('.btn-edit', 'click', function () {
    $('#viewEditInstructor').bootstrapValidator('resetForm', true);  
    var value = $(this).data('id');
    var url = '{{ URL::to('/sub_mat/edit_instructor') }}';          
    $.ajax({
      type : 'get',
      url : url,
      data : {'id':value},
      success: function (data) {
        console.log(data[0]);
        $('#id').val(data[0].id);
        $('#ids').val(data[0].ids);
        $('#courses_id').val(data[0].courses_id);
        $('#courses_name').val(data[0].courses_name);
        $('#times_study').val(data[0].times_study+' ชั่วโมง');
        $('#coach').val(data[0].coach);
        $('#student_groups_id').val(data[0].class);
        // $("#personels_idd").html('<option value="' + data[0].personels_id + '">' + data[0].first_name + ' ' + data[0].last_name + '</option>');
        // $('#student_groups_id').html('<option value="' + data[0].student_groups_id + '">' + data[0].class + '</option>');
        // $('#classrooms_id').html('<option value="' + data[0].classrooms_id + '">' + data[0].classroom_type + '</option>');
      }
    });
  });
  $('.viewEditInstructor').on('submit', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    $.ajax({
      type : 'post',
      url : url,
      data : {data}
    });
  });
  $(document).ready(function () {
    $('#viewEditInstructor').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        // courses_id: {
        //   validators: {
        //     stringLength: {
        //       min: 6,
        //       max: 6,
        //       message: 'กรอกรหัสวิชาให้ครบ 6 ตัวอักษร'
        //     },
        //     notEmpty: {
        //       message: 'กรุณากรอกรหัสวิชา'
        //     }
        //   }
        // },
        // courses_name: {
        //   validators: {
        //     regexp: {
        //       regexp: /^[u0E01-\u0E7F\s]+$/i,
        //       message: 'กรอกชื่อวิชาให้ถูกต้อง'
        //     },
        //     notEmpty: {
        //       message: 'กรุณากรอกชื่อวิชา'
        //     }
        //   }
        // },
        coach: {
          validators: {
            regexp: {
              regexp: /^[u0E01-\u0E7F\s]+$/i,
              message: 'กรอกชื่อนักศึกษาฝึกสอนให้ถูกต้อง'
            }
          }
        },
        // times_study: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกเวลาเรียน'
        //     }
        //   }
        // },
        // scheduel_times: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกเวลารวม'
        //     }
        //   }
        // },
        personels_id: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกอาจารย์'
            }
          }
        },
        // student_groups_id: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกชั้น'
        //     }
        //   }
        // },
        classrooms_id: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกห้อง'
            }
          }
        }
      },
    });
  });
</script>

@stop



