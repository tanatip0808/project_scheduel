@extends('layout.sub_matter') @section('content') @include('sub_matter.add_study_course')

<div class="aaa container col-lg-6">
  <h3 class="glyphicon glyphicon-plus"> เพิ่มรายวิชาที่เปิดสอน</h3>
  <br>
  <br> {!! Form::open(['url' => 'sub_mat/submitInstructor/','id' => 'addInstructor']) !!}

  <div class="form-group">
    <div class="input-group " style="width:490px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-calendar"></i>
      </span>
      <select class="form-control " style="width:490px;" name="year_studies_id">
        <option value="" disabled selected>ปีการศึกษา</option>
        @foreach($year_studies as $year_studie0)
        <option value="{{ $year_studie0->id }}">{{'ปีการศึกษา '.$year_studie0->year.' เทอม '.$year_studie0->term}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group divssss">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <select class="form-control student_groups_id" name="student_groups_id" style="width:490px;">
        <option value="" disabled selected>กลุ่มเรียน</option>
        @foreach($student_groups as $student_group0)
        <option value="{{ $student_group0->id }}">{{'ชั้นมัธยมศึกษาปีที่ '.$student_group0->class}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group divssss">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-home"></i>
      </span>
      <select class="form-control divssss" name="classrooms_id" style="width:490px;">
        <option value="" disabled selected>ห้อง</option>
        @foreach($classrooms as $classroom0)
        <option value="{{ $classroom0->id }}">{{$classroom0->classroom_type}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group divs-lefts">
    <div class="input-group " style="width:158px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-list-alt"></i>
      </span>
      <select class="form-control divssss courses_id" id="coursess_id" name="courses" style="width:158px;">
        <option value="" disabled selected>รหัสวิชา</option>
      </select>
    </div>
  </div>
  <div class="form-group divs-lefts">
    <div class="input-group" style="width:99px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-list-alt"></i>
      </span>
      <input class="form-control divssss credit" type="text" style="width:99px;" placeholder="หน่วยกิต" readonly></input>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group" style="width:135px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-time"></i>
      </span>
      <input class="form-control divssss times_study" type="text" style="width:135px;" placeholder="เวลาเรียน" readonly></input>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group" style="width:490px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-list-alt"></i>
      </span>
      <input class="form-control divssss courses_name" type="text" style="width:490px;" placeholder="ชื่อวิชา" readonly></input>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group " style="width:290px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <select class="form-control " name="personels_id" placeholder="กรุณาเลือกครูผู้สอน" style="width:490px;">
        <option value="" disabled selected>อาจารย์</option>
        @foreach($personels as $personel0)
        <option value="{{ $personel0->id }}">{{$personel0->first_name.' '.$personel0->last_name}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group " style="width:290px;">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-user"></i>
      </span>
        <input type="text" class="form-control" name="coach" placeholder="นักศึกษาฝึกสอน *ถ้ามี*" style="width:490px;"/>
    </div>
  </div>
  <span style="color:red;">*หมายเหตุ* จะเลือกรายวิชาได้ต้องเลือกกลุ่มเรียนก่อน</span>
  <div style="float:right;">
    <button type="reset" class="btn btn-danger" style="margin-right:5px;">
      <span class="glyphicon glyphicon-remove"></span> ยกเลิก
    </button>
    <button type="submit" class="btn btn-success" style="margin-right:115px;">
      <span class="glyphicon glyphicon-ok"></span> ตกลง
    </button>
  </div>
  <br>
  <br>
  <br>
</div>
{!! Form::close() !!}

<script type="text/javascript">
  $(document).on('change','.courses_id',function(){
    var courses_id = $(this).val();
    $.ajax({
      type : 'get',
      url : '{{ URL::to('/sub_mat/show_courses_name') }}',
      data : {'id':courses_id},
      dataType : 'json',
      success: function(data){
        $('.courses_name').val(data.courses_name);
        $('.credit').val('หน่วยกิต '+data.credit);
        $('.times_study').val('เวลาเรียน '+data.times_study+' ชั่วโมง');
      }
    });
  });
  $(document).on('change','.student_groups_id',function(){
    var student_groups_id = $(this).val();
    var op = " ";
    $.ajax({
      type : 'get',
      url : '{{ URL::to('/sub_mat/show_student_groups') }}',
      data : {'id':student_groups_id},
      dataType : 'json',
      success: function(data){
        console.log(data);
        console.log(data.length);
        op+='<option value="0" selected disabled>เลือกรหัสวิชา</option>';
        for(var i=0;i<data.length;i++){
          op+='<option value="'+data[i].id+'">'+data[i].courses_id+'</option>';
        }
        $('.courses_id').html(op);
      }
    });
  });
  $('#addInstructor').on('reset',function(){
    $('#addInstructor').bootstrapValidator('resetForm', true);
  });
  $(document).ready(function () {
    $('#addInstructor').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        // study_courses_id: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกหลักสูตร'
        //     }
        //   }
        // },
        year_studies_id: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกปีการศึกษา'
            }
          }
        },
        student_groups_id: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกกลุ่มเรียน'
            }
          }
        },
        classrooms_id: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกห้อง'
            }
          }
        },
        courses: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกรหัสวิชา'
            }
          }
        },
        personels_id: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกอาจารย์'
            }
          }
        },
        // times_study: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกเวลาเรียน'
        //     }
        //   }
        // },
        // scheduel_times: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกเวลารวม'
        //     }
        //   }
        // },
        // credit: {
        //   validators: {
        //     notEmpty: {
        //       message: 'กรุณาเลือกหน่วยกิต'
        //     }
        //   }
        // },
        coach: {
          validators: {
            regexp: {
              regexp: /^[u0E01-\u0E7F\s]+$/i,
              message: 'กรอกชื่อนักศึกษาฝึกงานให้ถูกต้อง'
            }
          }
        },
      },
    });
  });
</script>

@stop
