<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
</head>

<body>

    <table style="text-align:center;">
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9" style="text-align:center;">
                <h2>ตารางสอนของ อาจารย์ {{ $personels->first_name.' '.$personels->last_name }}</h2>
            </td>
        </tr>
        <tr>
            <td colspan="9" style="text-align:center;">
                <h2>ประจำภาคเรียนที่ @foreach($scheduel as $term_year) @if ($loop->first) {{ $term_year->term }} @endif @endforeach
                    ปีการศึกษา @foreach($scheduel as $term_year) @if ($loop->first) {{ $term_year->year }} @endif @endforeach</h2>
            </td>
        </tr>
        <tr>
            <td colspan="9" style="text-align:center;">
                <h2>โรงเรียนไทรเเก้ววิทยา ตำบลกันตวจระมวล อำเภอปราสาท จังหวัดสุรินทร์</h2>
            </td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr style="text-align:center;">
            <td style="background-color:#EEEEEE;">
                <h4>วัน/เวลา</h4>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>08.30-09.20</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>09.25-10.15</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>10.20-11.10</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>11.15-12.05</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>12.30-12.55</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>13.00-13.50</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>13.55-14.45</h3>
            </td>
            <td style="background-color:#EEEEEE;">
                <h3>14.50-15.40</h3>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;background-color:#EEEEEE;">
                <h4 style="vertical-align:middle;">วันจันทร์</h4>
            </td>
            @for ($i=11; $i
            <=18; $i++) @if($i-10==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h4 style="color:#928e8e;vertical-align:middle;">{{ 'พักกลางวัน' }}</h4>
                @else
                <td style="text-align:center;" data-day=1 data-time={{$i-10}}> @if(empty($data[$i])) {{''}} @else
                    <div class="itemadd" data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    <lable> {{$data[$i]}}
                        <br> {{$datacourse[$i]}}
                        <br> {{$classroom[$i]}}
                    </lable>
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="text-align:center;background-color:#EEEEEE;">
                <h4 style="vertical-align:middle;">วันอังคาร</h4>
            </td>
            @for ($i=21; $i
            <=28; $i++) @if($i-20==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h4 style="color:#928e8e;vertical-align:middle;">{{ 'พักกลางวัน' }}</h4>
                @else
                <td style="text-align:center;" data-day=2 data-time={{$i-20}}> @if(empty($data[$i])) {{''}} @else
                    <div class="itemadd" data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <lable> {{$data[$i]}}
                            <br> {{$datacourse[$i]}}
                            <br> {{$classroom[$i]}}
                        </lable>
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="text-align:center;background-color:#EEEEEE;">
                <h4 style="vertical-align:middle;">วันพุธ</h4>
            </td>
            @for ($i=31; $i
            <=38; $i++) @if($i-30==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h4 style="color:#928e8e;vertical-align:middle;">{{ 'พักกลางวัน' }}</h4>
                @else
                <td style="text-align:center;" data-day=3 data-time={{$i-30}}> @if(empty($data[$i])) {{''}} @else
                    <div class="itemadd" data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <lable> {{$data[$i]}}
                            <br> {{$datacourse[$i]}}
                            <br> {{$classroom[$i]}}
                        </lable>
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="text-align:center;background-color:#EEEEEE;">
                <h4 style="vertical-align:middle;">วันพฤหัสบดี</h4>
            </td>
            @for ($i=41; $i
            <=48; $i++) @if($i-40==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h4 style="color:#928e8e;vertical-align:middle;">{{ 'พักกลางวัน' }}</h4>
                @else
                <td style="text-align:center;" data-day=4 data-time={{$i-40}}> @if(empty($data[$i])) {{''}} @else
                    <div class="itemadd" data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <lable> {{$data[$i]}}
                            <br> {{$datacourse[$i]}}
                            <br> {{$classroom[$i]}}
                        </lable>
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="text-align:center;background-color:#EEEEEE;">
                <h4 style="vertical-align:middle;">วันศุกร์</h4>
            </td>
            @for ($i=51; $i
            <=58; $i++) @if($i-50==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h4 style="color:#928e8e;vertical-align:middle;">{{ 'พักกลางวัน' }}</h4>
                @else
                <td style="text-align:center;" data-day=5 data-time={{$i-50}}> @if(empty($data[$i])) {{''}} @else
                    <div class="itemadd" data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <lable> {{$data[$i]}}
                            <br> {{$datacourse[$i]}}
                            <br> {{$classroom[$i]}}
                        </lable>
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr style="text-align:center;">
            <td colspan="9">
                <h2 style="text-align:center">รวมเวลาสอนทั้งสิ้น {{ $count_scheduels }} คาบ </h2>
            </td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="9"></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center"><h3>ลงชื่อ........................................</h3></td>
            <td></td>
            <td colspan="4" style="text-align:center"><h3>ลงชื่อ...................................................</h3></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center"><h3>(นางสาวเบญญาภา ตนกลาย)</h3></td>
            <td></td>
            <td colspan="4" style="text-align:center"><h3>(นายธรรมนูญ มีเสนา)</h3></td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center"><h3>หัวหน้ากลุ่มงานบริหารวิชาการ</h3></td>
            <td></td>
            <td colspan="4" style="text-align:center"><h3>ผู้อำนวยการโรงเรียนไทรแก้ววิทยา</h3></td>
        </tr>
    </table>

</body>

</html>