@extends('layout.sub_matter') @section('content')

<div class="container col-md-11">
<h5 class="divs-left" style="font-size:18px;margin-top:30px;"> ตารางสอนของกลุ่มสาระ{{$subject_matters_name->subject_matter_name}}</h5> 
    {!! Form::open(['url' => 'sub_mat/search_table/']) !!}
    <div class="form-group">
        <select class="form-control divss" name="id" style="width:210px;" required>
            <option value="" disabled selected>เลือกปีการศึกษา...</option>
            @foreach($year_studies as $year_studies1)
            <option value="{{ $year_studies1->id }}">{{'ปีการศึกษา '.$year_studies1->year.' เทอม '.$year_studies1->term}}</option>
            @endforeach
        </select>
    </div>
    <button class="btn btn-info" type="submit">
        <span class="glyphicon glyphicon-search"></span> ค้นหา
    </button>
    <a class="btn btn-outline-light" style="float:right;" id="msg" disabled>
        <span class="glyphicon glyphicon-th" id="btns"> ปีการศึกษา {{ $show_year_studies->year.' เทอม '.$show_year_studies->term  }} </span> 
    </a>
    {!! Form::close() !!}
    @if($count_scheduels_year_studies=='0')
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1 class="tablesss" style="color:#c1bbbb;" id="btnss">ไม่มีตารางสอนของปี {{ $show_year_studies->year.' เทอม '.$show_year_studies->term}}
    </h1>
    @else
    <br>
    <div style="float:left;">
        @foreach($count_scheduels as $count_scheduels1)
        <a class="btn btn-primary btn-check" href="{!! URL::to('/sub_mat/view_table/'.$count_scheduels1->id,$count_scheduels1->year_studies_id) !!}" style="margin-right:20px;margin-bottom:20px;width:201px;height:100px;">
            <h5 class="glyphicon glyphicon-th" style="margin-top:16px;"> ตารางสอน</h5>
            <h5>{{ $count_scheduels1->first_name.' '.$count_scheduels1->last_name}}</h5>
        </a>
        @endforeach
    </div>
    @endif

    <script type="text/javascript">
        $(document).ready(function () {
            var message = $('#btns');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btnss');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#msg');
            message.show('slow');
        });
    </script>

@stop
