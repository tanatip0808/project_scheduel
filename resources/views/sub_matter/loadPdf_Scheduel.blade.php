<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ตารางสอนของอาจารย์ {{ $personels->first_name.' '.$personels->last_name }} กลุ่มสาระ{{$subject_matters_name->subject_matter_name}} ปีการศึกษา {{ $year_name->year.' / '.$year_name->term }}</title>
    <style type="text/css">
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        table {
            width: 100%;
            border: 1px solid black;
        }

        td,
        th {
            border: 1px solid black;
        }

        body {
            font-family: "THSarabunNew";
        }
    </style>
</head>

<body>
    <img src="img/logo.jpg" style="float:left;width:85px;margin-top:25px;">
    <h3 style="text-align:center;margin-right:80px;">ตารางสอนของอาจารย์ {{ $personels->first_name.' '.$personels->last_name }} กลุ่มสาระ{{$subject_matters_name->subject_matter_name}}
        <br> ประจำภาคเรียนที่ {{ $year_name->term }} ปีการศึกษา {{ $year_name->year }}
        <br> โรงเรียนไทรเเก้ววิทยา ตำบลกันตวจระมวล อำเภอปราสาท จังหวัดสุรินทร์ </h3>
    <table class="table table-bordered " cellpadding="0" cellspacing="0">
        <thead style="background-color:#EEEEEE;">
            <tr>
                <td style="width:5%;">
                    <h5 style="text-align:center;font-size:12px;">วัน/เวลา</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">08.30-09.20</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">09.25-10.15</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">10.20-11.10</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">11.15-12.25</h5>
                </td>
                <td style="width:6%;">
                    <h5 style="text-align:center;">12.30-12.55</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">13.00-13.50</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">13.55-14.45</h5>
                </td>
                <td style="width:10%;">
                    <h5 style="text-align:center;">14.50-15.40</h5>
                </td>
            </tr>
        </thead>
        <tr>
            <td style="height:132px;background-color:#EEEEEE;vertical-align:middle;">
                <h5 style="margin-left:7px;font-size:12px;">วันจันทร์</h5>
            </td>
            @for ($i=11; $i
            <=18; $i++) @if($i-10==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h6 style="color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="text-align:center;" data-day=1 data-time={{$i-10}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <h6 style="font-size:11px;">{{$data[$i]}}
                            <br>{{$datacourse[$i]}}
                            @if($classroom[$i] == 'ปกติ')
                            <br>{{$coach[$i]}}
                            <br>{{$classroom[$i] == ''}}
                            @else
                            <br>{{$classroom[$i]}}
                            <br>{{$coach[$i]}}</h6>
                            @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:129px;background-color:#EEEEEE;vertical-align:middle;">
                <h5 style="margin-left:6px;font-size:12px;">วันอังคาร</h5>
            </td>
            @for ($i=21; $i
            <=28; $i++) @if($i-20==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h6 style="color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                    @else
                    <td style="text-align:center;" data-day=2 data-time={{$i-20}}> @if(empty($data[$i])) {{''}} @else
                        <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                            <h6 style="font-size:11px;">{{$data[$i]}}
                            <br>{{$datacourse[$i]}}
                            @if($classroom[$i] == 'ปกติ')
                            <br>{{$coach[$i]}}
                            <br>{{$classroom[$i] == ''}}
                            @else
                            <br>{{$classroom[$i]}}
                            <br>{{$coach[$i]}}</h6>
                            @endif
                        </div>
                        @endif @endif
                    </td>
                    @endfor
        </tr>
        <tr>
            <td style="height:129px;background-color:#EEEEEE;vertical-align:middle;">
                <h5 style="margin-left:11px;font-size:12px;">วันพุธ</h5>
            </td>
            @for ($i=31; $i
            <=38; $i++) @if($i-30==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h6 style="color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="text-align:center;" data-day=3 data-time={{$i-30}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <h6 style="font-size:11px;">{{$data[$i]}}
                        <br>{{$datacourse[$i]}}
                            @if($classroom[$i] == 'ปกติ')
                            <br>{{$coach[$i]}}
                            <br>{{$classroom[$i] == ''}}
                            @else
                            <br>{{$classroom[$i]}}
                            <br>{{$coach[$i]}}</h6>
                            @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:129px;background-color:#EEEEEE;vertical-align:middle;">
                <h5 style="margin-left:1px;font-size:12px;">วันพฤหัสบดี</h5>
            </td>
            @for ($i=41; $i
            <=48; $i++) @if($i-40==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h6 style="color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="text-align:center;" data-day=4 data-time={{$i-40}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <h6 style="font-size:11px;">{{$data[$i]}}
                        <br>{{$datacourse[$i]}}
                            @if($classroom[$i] == 'ปกติ')
                            <br>{{$coach[$i]}}
                            <br>{{$classroom[$i] == ''}}
                            @else
                            <br>{{$classroom[$i]}}
                            <br>{{$coach[$i]}}</h6>
                            @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:129px;background-color:#EEEEEE;vertical-align:middle;">
                <h5 style="margin-left:10px;font-size:12px;">วันศุกร์</h5>
            </td>
            @for ($i=51; $i
            <=58; $i++) @if($i-50==5) <td style="background-color:#EEEEEE;text-align:center;">
                <h6 style="color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="text-align:center;" data-day=5 data-time={{$i-50}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                        <h6 style="font-size:11px;">{{$data[$i]}}
                        <br>{{$datacourse[$i]}}
                            @if($classroom[$i] == 'ปกติ')
                            <br>{{$coach[$i]}}
                            <br>{{$classroom[$i] == ''}}
                            @else
                            <br>{{$classroom[$i]}}
                            <br>{{$coach[$i]}}</h6>
                            @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
    </table>
    <br>
    <span style="margin-left:280px;font-size:20px;">รวมเวลาสอนทั้งสิ้น {{ $count_scheduels }} คาบ </span>
    <br>
    <br>
    <span style="float:left;margin-left:30px;font-size:19px;">ลงชื่อ........................................
        <br>(นางสาวเบญญาภา ตนกลาย)
        <br>หัวหน้ากลุ่มงานบริหารวิชาการ</span>
    <span style="float:right;margin-right:30px;font-size:19px;">ลงชื่อ..............................................
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(นายธรรมนูญ มีเสนา)
        <br>ผู้อำนวยการโรงเรียนไทรแก้ววิทยา</span>
</body>

</html>
