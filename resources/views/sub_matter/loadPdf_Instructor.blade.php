<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>รายวิชาที่เปิดสอนของกลุ่มสาระ{{$subject_matters_name->subject_matter_name}} ปีการศึกษา {{ $year_name->year.' / '.$year_name->term}}</title>
    <style type="text/css">
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        table {
            width: 100%;
            border: 1px solid black;
        }

        td,
        th {
            border: 1px solid black;
        }

        body {
            font-family: "THSarabunNew";
        }
    </style>
</head>

<body>
    <img src="img/logo.jpg" style="float:left;width:85px;margin-left:10px;">
    <h3 style="text-align:center;margin-right:90px;">รายวิชาที่เปิดสอนของกลุ่มสาระ{{$subject_matters_name->subject_matter_name}} ปีการศึกษา {{ $year_name->year.' / '.$year_name->term}}
        <br> โรงเรียนไทรเเก้ววิทยา ตำบลกันตวจระมวล อำเภอปราสาท จังหวัดสุรินทร์ </h3>
    <table class="table table-bordered" cellpadding="0" cellspacing="0">
        <thead style="background-color:#EEEEEE;">
            <tr style="text-align: center;">
                <th style="width:7%;">รหัสวิชา</th>
                <th style="width:25%;">ชื่อวิชา</th>
                <th style="width:19%;">อาจารย์</th>
                <th style="width:19%;">นักศึกษาฝึกสอน</th>
                <th style="width:7%;">เวลาเรียน</th>
                <th style="width:7%;">กลุ่มเรียน</th>
                <th style="width:9%;">ห้อง</th>
            </tr>
        </thead>
        <tbody>
            @foreach($view_instructors as $instructor0)
            <tr style="text-align: center;">
                <td>
                    {{ $instructor0->courses_id }}
                </td>
                <td>
                    {{ $instructor0->courses_name }}
                </td>
                <td>
                    {{ $instructor0->first_name.' '.$instructor0->last_name }}
                </td>
                <td>
                    {{ $instructor0->coach != null ? $instructor0->coach : 'ไม่มี' }}
                </td>
                <td>
                    {{ $instructor0->times_study }}
                </td>
                <td>
                    {{ $instructor0->class }}
                </td>
                <td>
                    {{ $instructor0->classroom_type }}
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
    <h3 style="text-align:center">รายวิชาทั้งหมด {{ $count_instructors }} วิชา </h3>
</body>

</html>
