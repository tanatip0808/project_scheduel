<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>

<div class="col-lg-1">
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm" style="float:left;" id="nbrmsg">
    <span class="glyphicon glyphicon-plus" ></span> เพิ่มหลักสูตร
  </button>
  <br>
  <br>
  <br> -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm1" style="float:left;" id="nbrmsggg">
    <span class="glyphicon glyphicon-plus" ></span> เพิ่มปีการศึกษา
  </button>
  <!-- <br>
  <br>
  <br>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm2" style="float:left;" id="nbrmsgggg">
    <span class="glyphicon glyphicon-plus" ></span> เพิ่มรายวิชา
  </button> -->
</div>

<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title glyphicon glyphicon-folder-open"> เพิ่มหลักสูตร</h3>
      </div>
      <form action="{!! URL::to('/sub_mat/submitStudyCourse') !!}" method="post" id="addStudyCourse">
        {!! csrf_field() !!}
        <div class="modal-body">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>หลักสูตร : </span>
              </span>
              <input type="text" class="form-control" name="study_course_name" placeholder="หลักสูตร">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
          <button type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-ok"></span> ตกลง</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title glyphicon glyphicon-calendar"> เพิ่มปีการศึกษา</h3>
      </div>
      <form action="{!! URL::to('/sub_mat/submitYearStudies') !!}" method="post" id="addYearStudies">
        {!! csrf_field() !!}
        <div class="modal-body">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>ปีการศึกษา : </span>
              </span>
              <input type="text" class="form-control" name="year" placeholder="ปีการศึกษา">
            </div>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="form-control-label">เทอม :</label>
            <input type="radio" name="term" value="1" required>
            <strong>1</strong>
            <input type="radio" name="term" value="2" required>
            <strong>2</strong>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
          <button type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-ok"></span> ตกลง</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-sm2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title glyphicon glyphicon-list-alt"> เพิ่มรายวิชา</h3>
      </div>
      <form action="{!! URL::to('/sub_mat/submitCourses') !!}" method="post" id="addCourses">
        {!! csrf_field() !!}
        <div class="modal-body">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>รหัสวิชา : </span>
              </span>
              <input type="text" class="form-control" name="courses_id" placeholder="รหัสวิชา">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">
                <span>ชื่อวิชา : </span>
              </span>
              <input type="text" class="form-control" name="courses_name" placeholder="ชื่อวิชา">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
          <button type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-ok"></span> ตกลง</button>
      </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    var message = $('#nbrmsg');
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#nbrmsggg');
    message.show('slow');
  });
  $(document).ready(function () {
    var message = $('#nbrmsgggg');
    message.show('slow');
  });
  $('#nbrmsg').on('click',function(){
    $('#addStudyCourse').bootstrapValidator('resetForm', true);
  });
  $('#nbrmsggg').on('click',function(){
    $('#addYearStudies').bootstrapValidator('resetForm', true);
  });
  $('#nbrmsgggg').on('click',function(){
    $('#addCourses').bootstrapValidator('resetForm', true);
  });
  $(document).ready(function () {
    $('#addStudyCourse').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        study_course_name: {
          validators: {
            between: {
              min: 1000,
              max: 9999,
              message: 'กรุณากรอกหลักสูตรให้ครบ 4 ตัวเเละเป็นตัวเลขเท่านั้น'
            },
            notEmpty: {
              message: 'กรุณากรอกหลักสูตร'
            }
          }
        }
      },
    });
    $('#addYearStudies').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        year: {
          validators: {
            between: {
              min: 1000,
              max: 9999,
              message: 'กรุณากรอกปีการศึกษาให้ครบ 4 ตัวเเละเป็นตัวเลขเท่านั้น'
            },
            notEmpty: {
              message: 'กรุณากรอกปีการศึกษา'
            }
          }
        },
        term: {
          validators: {
            notEmpty: {
              message: 'กรุณาเลือกเทอม'
            }
          }
        }
      }
    });
    $('#addCourses').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        courses_id: {
          validators: {
            stringLength: {
              min: 6,
              max: 6,
              message: 'กรอกรหัสวิชาให้ครบ 6 ตัวอักษร'
            },
            notEmpty: {
              message: 'กรุณากรอกรหัสวิชา'
            }
          }
        },
        courses_name: {
          validators: {
            regexp: {
              regexp: /^[u0E01-\u0E7F\s]+$/i,
              message: 'กรอกชื่อวิชาให้ถูกต้อง'
            },
            notEmpty: {
              message: 'กรุณากรอกชื่อวิชา'
            }
          }
        }
      }
    });
  });
</script>
 