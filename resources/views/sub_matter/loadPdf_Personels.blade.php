<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>รายชื่ออาจารย์ภายในกลุ่มสาระ{{$subject_matters_name->subject_matter_name}}</title>
    <style type="text/css">
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew Bold.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ public_path('fonts/THSarabunNew Italic.ttf') }}") format('truetype');
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ public_path('fonts/THSarabunNew BoldItalic.ttf') }}") format('truetype');
        }

        table {
            width: 100%;
            border: 1px solid black;
        }

        td,
        th {
            border: 1px solid black;
        }

        body {
            font-family: "THSarabunNew";
        }
    </style>
</head>

<body>
    <img src="img/logo.jpg" style="float:left;width:85px;margin-left:30px;">
    <h3 style="text-align:center;margin-right:110px;">รายชื่ออาจารย์ภายในกลุ่มสาระ{{$subject_matters_name->subject_matter_name}}
        <br> โรงเรียนไทรเเก้ววิทยา ตำบลกันตวจระมวล อำเภอปราสาท จังหวัดสุรินทร์ </h3>
    <table class="table table-bordered" cellpadding="0" cellspacing="0">
        <thead style="background-color:#EEEEEE;">
            <tr style="text-align: center;">
                <th style="width:140px;">
                    <span class="glyphicon glyphicon-user"></span> ชื่อ-นามสกุล</th>
                <th style="width:90px;">
                    <span class="glyphicon glyphicon-phone"></span> เบอร์โทรศัพท์</th>
                <th style="width:90px;">
                    <span class="glyphicon glyphicon-education"></span> ตำเเหน่ง</th>
                <th style="width:155px;">
                    <span class="glyphicon glyphicon-folder-close"></span> กลุ่มสาระ</th>
                <th style="width:75px;">
                    <span class="glyphicon glyphicon-user"></span> สถานะ</th>
            </tr>
        </thead>
        <tbody>
            @foreach($view_personels as $personels0)
            <tr style="text-align: center;">
                <td>
                    {{ $personels0->first_name.' '.$personels0->last_name }}
                </td>
                <td>
                    {{ $personels0->tel }}
                </td>
                <td>
                    {{ $personels0->position_name }}
                </td>
                <td>
                    {{ $personels0->subject_matter_name }}
                </td>
                <td>
                    {{ $personels0->status_detail }}
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>
    <h3 style="text-align:center">อาจารย์ทั้งหมด {{ $count_personels }} คน </h3>
</body>

</html>
