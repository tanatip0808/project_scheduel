<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title></title>
  </head>
  <body>
    <div class="container">
      <div class="row" style="margin-top:50px;">
        <a href="{{$url}}" class="btn btn-primary">เพิ่ม User</a>
      <br><br>
      @if (session('error'))
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
        {{ session('error') }}
    </div>
@endif
            <table class="table table-striped text-center">
            <tr>
              <td>user_name</td>
              <td>กลุ่มสาระ</td>
              <td>action</td>
            </tr>
            @foreach ($admin as $row)
              <tr>
                <td>{{$row->user_name}}</td>
                <td>{{$row->subname}}</td>
                  <td>
                    {{-- <a href="{{url('headadmin/head_admin/'.$row->id.'/edit')}}" class="fl btn btn-warning" style="float:left; margin-left:400px;">แก้ไข</a> --}}
                    <form  action="{{url('headadmin/head_admin'.$row->id)}}" method="post" onsubmit="return(confirm('คุณต้องการลบใช่หรือไม่'))">
                      {{method_field('DELETE')}}
                      {{csrf_field()}}
                      <button type="submit" class="fl btn btn-danger" id="delete" >ลบ</button>
                    </form>
                  </td>
              </tr>
            @endforeach
          </table>
      </div>
    </div>
  </body>
</html>
