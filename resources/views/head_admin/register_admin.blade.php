<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title> subject_matters</title>
  </head>
  <body>
    <div class="container">
      <h2>สมัคร subject_matters</h2>
      <form action="{{$url}}" method="POST" class="form-horizontal" id="add_user">
          {{method_field($method)}}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
          <div class="form-group">
            <label for="name" class="col-sm-2 control-label">User_name</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="user_name" pattern="[A-Za-z]" value="{{$objs->first_name or ''}}">
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="password" value="{{$objs->last_name or ''}}">
            </div>
          </div>
          <div class="form-group">
            <label for="Re-password" class="col-sm-2 control-label">Re-Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control"  name="confirmpassword" value="{{$objs->last_name or ''}}">
            </div>
          </div>
          <div class="form-group">
            <label for="position" class="col-sm-2 control-label">กลุ่มสาระ</label>
            <div class="col-sm-6">
              <select class="form-control" name="subject">
                @foreach ($subject as $sj)
                    <option value="{{$sj->subid}}">{{$sj->sname}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary">บันทึก</button>    <button type="submit" class="btn btn-danger">ล้าง</button>
            </div>
          </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function () {
        $('#add_user').bootstrapValidator({
          feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            user_name: {
              validators: {
                notEmpty: {
                  message: 'กรุณากรอก User_name'
                }
              }
            },
            password: {
              validators: {
                notEmpty: {
                  message: 'กรุณากรอกรหัสผ่าน'
                }
              }
            },
          confirmpassword: {
              validators: {
               notEmpty: {
                   message: 'กรุณากรอกรหัสผ่าน'
                 },
                 identical: {
                       field: 'password',
                       message: 'กรุณากรอกรหัสผ่านให้ตรงกัน'
                   }
                   }
               }
             }
        });
      });
    </script>
      <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script>
  </body>
</html>
