@extends('layout.director') @section('content')

<div class="container col-md-11">
    <div class="form-group">
    </div>
    <a class="btn btn-success" href="{!! URL::to('/director/return_student_groups/'.$student_groups,$year_studies) !!}" style="float:right;">
        <span class="glyphicon glyphicon-repeat" id="btnsss"> </span> ย้อนกลับ
    </a>
    <a class="btn btn-outline-light" style="float:right;margin-right:5px;" disabled>
        <span class="glyphicon glyphicon-calendar" id="btns"> ปีการศึกษา {{ $show_year_studies->year.' เทอม '.$show_year_studies->term }} </span>
    </a>

    <h3 style="text-align:left;"> ตารางเรียนของชั้นมัธยมศึกษาปีที่ {{ $show_student_groups->class }}</h3>
    @if($check_scheduel_unconfrim==0) 
    <button type="button" class="btn btn-success" style="float:right;margin-bottom:5px;" disabled>
        <span class="glyphicon glyphicon-ok-circle" id="btnssss"></span> อนุมัติตารางเรียนเเล้ว
        <span id="nbrmsgg" class="badge">{{ 'รวม '.$check_scheduel_confrim.' คาบ' }}</span>
    </button>
    @else
    <button type="button" class="btn btn-danger" style="float:right;margin-bottom:5px;" id="msgg" disabled>
        <span class="glyphicon glyphicon-remove-circle" id="btnssss"></span> ยังไม่อนุมัติตารางเรียน
        <span id="nbrmsgg" class="badge">{{ 'เหลือ '.$check_scheduel_unconfrim.' คาบที่ยังไม่อนุมัติ' }}</span>
    </button>
    @endif 
    @if($check_student_groups==1 || $check_student_groups==2 || $check_student_groups==3)
    <table class="table table-bordered text-center">
        <thead style="background-color:#cacaca;">
            <tr style="height:75px;">
                <td>
                    <br>
                    <label>วัน/เวลา</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>08.30-09.20</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>09.25-10.15</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>10.20-11.10</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>11.15-12.05</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>12.30-12.55</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>13.00-13.50</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>13.55-14.45</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>14.50-15.40</label>
                </td>
            </tr>
        </thead>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันจันทร์</label>
            </td>
            @for ($i=11; $i
            <=18; $i++) @if($i-10==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=1 data-time={{$i-10}}>
                    @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันอังคาร</label>
            </td>
            @for ($i=21; $i
            <=28; $i++) @if($i-20==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=2 data-time={{$i-20}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันพุธ</label>
            </td>
            @for ($i=31; $i
            <=38; $i++) @if($i-30==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=3 data-time={{$i-30}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันพฤหัสบดี</label>
            </td>
            @for ($i=41; $i
            <=48; $i++) @if($i-40==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=4 data-time={{$i-40}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันศุกร์</label>
            </td>
            @for ($i=51; $i
            <=58; $i++) @if($i-50==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=5 data-time={{$i-50}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
    </table>
    @else
    <table class="table table-bordered text-center">
        <thead style="background-color:#cacaca;">
            <tr style="height:75px;">
                <td>
                    <br>
                    <label>วัน/เวลา</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>08.30-09.20</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>09.25-10.15</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>10.20-11.10</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>11.15-12.05</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>12.30-12.55</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>13.00-13.50</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>13.55-14.45</label>
                </td>
                <td style="width:125px;">
                    <br>
                    <label>14.50-15.40</label>
                </td>
            </tr>
        </thead>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันจันทร์</label>
            </td>
            @for ($i=11; $i
            <=18; $i++) @if($i-10==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=1 data-time={{$i-10}}>
                    @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันอังคาร</label>
            </td>
            @for ($i=21; $i
            <=28; $i++) @if($i-20==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=2 data-time={{$i-20}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันพุธ</label>
            </td>
            @for ($i=31; $i
            <=38; $i++) @if($i-30==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=3 data-time={{$i-30}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันพฤหัสบดี</label>
            </td>
            @for ($i=41; $i
            <=48; $i++) @if($i-40==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=4 data-time={{$i-40}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
        <tr>
            <td style="height:110px;background-color:#cacaca;vertical-align:middle;">
                <label>วันศุกร์</label>
            </td>
            @for ($i=51; $i
            <=58; $i++) @if($i-50==5) <td style="background-color:#cacaca;text-align:center;">
                <h6 style="margin-top:40px;color:#928e8e;">{{ 'พักกลางวัน' }}</h6>
                @else
                <td style="vertical-align:middle;" data-day=5 data-time={{$i-50}}> @if(empty($data[$i])) {{''}} @else
                    <div data-dayitem={{$day[$i]}} data-timeitem={{$time[$i]}}>
                    @if($check[$i]=='0')
                        <h6 style="color:red;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:red;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:red;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:red;">{{$coach[$i]}}</h6>
                    @else
                        <h6 style="color:green;">{{$data[$i].' '.$datass[$i]}}</h6>
                        <h6 style="color:green;">{{$datacourse[$i]}}</h6>
                        <h6 style="color:green;">{{$classroom[$i] != 'ปกติ' ? $classroom[$i] : ''}}</h6>
                        <h6 style="color:green;">{{$coach[$i]}}</h6>
                    @endif
                    </div>
                    @endif @endif
                </td>
                @endfor
        </tr>
    </table>
    @endif

    <script type="text/javascript">
        $(document).ready(function () {
            var message = $('#nbrmsgg');
            $('#msg').append(message);
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btns');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btnss');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btnsss');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btnssss');
            message.show('slow');
        });
    </script>

    @stop