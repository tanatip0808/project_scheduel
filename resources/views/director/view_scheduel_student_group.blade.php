@extends('layout.director') @section('content')

<div class="container col-md-11">
    <h3 class="glyphicon glyphicon-th divs-left"> ตารางเรียน</h3>
    {!! Form::open(['url' => 'director/search_table']) !!}
    <div class="form-group">
        <select class="form-control divss" name="ids" style="width:210px;"  required>
            <option value="" disabled selected>เลือกปีการศึกษา...</option>
            @foreach($year_studies as $year_studies1)
            <option value="{{ $year_studies1->id }}">{{'ปีการศึกษา '.$year_studies1->year.' เทอม '.$year_studies1->term}}</option>
            @endforeach
        </select>
    </div>
    <button class="btn btn-info" type="submit">
        <span class="glyphicon glyphicon-search"></span> ค้นหา
    </button>
    {!! Form::close() !!}
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1 class="tablesss" style="color:#c1bbbb;" id="btns">กรุณาเลือกปีการศึกษา
    </h1>

    <script type="text/javascript">
        $(document).ready(function () {
            var message = $('#btns');
            message.show('slow');
        });
    </script>

@stop