@extends('layout.director') @section('content')

<div class="container col-md-11">
    <h3 class="glyphicon glyphicon-th divs-left"> ตารางสอน</h3>
    {!! Form::open(['url' => 'director/searchYearStudies']) !!}
    <div class="form-group">
        <select class="form-control divss" name="id" style="width:210px;"  required>
            <option value="" disabled selected>เลือกปีการศึกษา...</option>
            @foreach($year_studies as $year_studies1)
            <option value="{{ $year_studies1->id }}">{{'ปีการศึกษา '.$year_studies1->year.' เทอม '.$year_studies1->term}}</option>
            @endforeach
        </select>
        <select class="form-control divss" name="ids" style="width:260px;" required>
            <option value="" disabled selected>เลือกกลุ่มสาระ...</option>
            @foreach($subject_matters as $subject_matters1)
            <option value="{{ $subject_matters1->id }}">{{$subject_matters1->subject_matter_name}}</option>
            @endforeach
        </select>
    </div>
    <button class="btn btn-info" type="submit">
        <span class="glyphicon glyphicon-search"></span> ค้นหา
    </button>
    {!! Form::close() !!}
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1 class="tablesss" style="color:#c1bbbb;" id="btns">กรุณาเลือกปีการศึกษาเเละกลุ่มสาระ 
    </h1>

    <script type="text/javascript">
        $(document).ready(function () {
            var message = $('#btns');
            message.show('slow');
        });
    </script>

@stop