@extends('layout.director') @section('content')

<div class="container col-md-11">
    <h5 class="divs-left" style="font-size:18px;margin-top:30px;"> ตารางสอนกลุ่มสาระ{{ $show_subject_matters->subject_matter_name }}</h5> 
    {!! Form::open(['url' => 'director/searchSubjectmatter'.$year_studies]) !!}
    <div class="form-group">
        <select class="form-control divss" name="ids" style="width:260px;" required>
            <option value="" disabled selected>เลือกกลุ่มสาระ...</option>
            @foreach($subject_matters as $subject_matters1)
            <option value="{{ $subject_matters1->id }}">{{$subject_matters1->subject_matter_name}}</option>
            @endforeach
        </select>
    </div>
    <button class="btn btn-info" type="submit">
        <span class="glyphicon glyphicon-search"></span> ค้นหา
    </button>
    <a class="btn btn-success" href="/director/view_scheduel" style="float:right;">
        <span class="glyphicon glyphicon-repeat" id="btnsss"> </span> ย้อนกลับ 
    </a>
    <a class="btn btn-outline-light" style="float:right;margin-right:5px;" disabled>
        <span class="glyphicon glyphicon-calendar" id="btns"> ปีการศึกษา {{ $show_year_studies->year.' เทอม '.$show_year_studies->term  }} </span> 
    </a>
    {!! Form::close() !!}

    <form action="{!! URL::to('/director/confrimPersonelsScheduels/'.$show_subject_matters->id,$year_studies) !!}" method="post">
    {!! csrf_field() !!}
    @if($count_confrim_scheduels>'0')
    <label style="float:right;margin-top:10px;color:red;">ยกเลิกตารางสอนทั้งหมด</label>
    <input type="checkbox" id="selectall1" onClick="selectAll1(this)" style="width:20px;height:20px;float:right;;margin-top:10px;margin-right:5px;margin-left:10px;"></input>
    @endif
    @if($count_Unconfrim_scheduels<='0') @else
    <label style="float:right;margin-top:10px;">เลือกตารางสอนทั้งหมด</label>
    <input type="checkbox" id="selectall" onClick="selectAll(this)" style="width:20px;height:20px;float:right;;margin-top:10px;margin-right:5px;"></input>
    @endif
    <button class="btn btn-warning" type="button" style="float:right;margin-top:5px;margin-right:5px;" id="confrimScheduel" data-toggle="modal" data-target=".bd-example-modal-sm">
        <span class="glyphicon glyphicon-check"></span> อนุมัติตารางสอน
    </button>
    <button class="btn btn-danger" type="button" style="float:right;margin-top:5px;margin-right:5px;" id="unconfrimScheduel" data-toggle="modal" data-target=".bd-example-modal-sm1">
        <span class="glyphicon glyphicon-check"></span> ยกเลิกตารางสอน
    </button>
    @if($count_subject_matters=='0')
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1 class="tablesss" style="color:#c1bbbb;" id="btnss">ไม่มีตารางสอนของกลุ่มสาระ{{ $show_subject_matters->subject_matter_name }}
    </h1>
    @else
    <br>
    <br>
    <div style="float:left;">
    @for($i=0; $i < count($check_scheduels_count); $i++)
        @if($check_status_scheduels[$i] == $check_scheduels_count[$i])
            @foreach($check_scheduels_personels[$i] as $check_confrim_scheduel[$i])
                <input type="checkbox" value="{{$check_confrim_scheduel[$i]->personels_id}}" style="width:30px;height:95px;"  disabled checked>
                <input type="checkbox" name="uncheckScheduel[]" onclick="select11('unconfrimScheduel')" value="{{$check_confrim_scheduel[$i]->personels_id}}" style="width:30px;height:95px;" >
                <a class="btn btn-primary btn-check" href="{!! URL::to('/director/view_table/'.$check_confrim_scheduel[$i]->personels_id,$show_year_studies->id) !!}" style="margin-right:20px;margin-bottom:20px;width:201px;height:100px;">
                    <h5 class="glyphicon glyphicon-th" style="margin-top:16px;"> ตารางสอน</h5>
                    <h5>{{ $check_confrim_scheduel[$i]->first_name.' '.$check_confrim_scheduel[$i]->last_name}}</h5>
                </a>
                </input>
            @endforeach
        @else
            @foreach($check_scheduels_personels[$i] as $check_unconfrim_scheduel[$i])
                <input type="checkbox" name="checkScheduel[]" onclick="select1('confrimScheduel')" value="{{$check_unconfrim_scheduel[$i]->personels_id}}" style="width:30px;height:95px;" >          
                <a class="btn btn-primary btn-check" href="{!! URL::to('/director/view_table/'.$check_unconfrim_scheduel[$i]->personels_id,$show_year_studies->id) !!}" style="margin-right:20px;margin-bottom:20px;width:201px;height:100px;">
                    <h5 class="glyphicon glyphicon-th" style="margin-top:16px;"> ตารางสอน</h5>
                    <h5>{{ $check_unconfrim_scheduel[$i]->first_name.' '.$check_unconfrim_scheduel[$i]->last_name}}</h5>
                </a>
                </input>
            @endforeach
        @endif
    @endfor
    </div>
    @endif

    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" style="width:460px;margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title glyphicon glyphicon-check"> อนุมัติตารางสอน</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <h4 style="text-align:center;" >ต้องการอนุมัติตารางสอนของอาจารย์ที่เลือก ใช่หรือไม่ ?
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-right:15px;width:100px;height:70px;">
                            <h4 class="glyphicon glyphicon-remove"> ไม่</h4>
                        </button>
                        <button type="submit" class="btn btn-success" style="width:100px;height:70px;">
                            <h4 class="glyphicon glyphicon-ok"> ใช่</h4>
                        </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm1" style="width:470px;margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title glyphicon glyphicon-check"> ยกเลิกอนุมัติตารางสอน</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <h4 style="text-align:center;" >ต้องการยกเลิกตารางสอนของอาจารย์ที่เลือก ใช่หรือไม่ ?
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-right:15px;width:100px;height:70px;">
                            <h4 class="glyphicon glyphicon-remove"> ไม่</h4>
                        </button>
                        <button type="submit" class="btn btn-success" style="width:100px;height:70px;">
                            <h4 class="glyphicon glyphicon-ok"> ใช่</h4>
                        </button>
                </div>
            </div>
        </div>
    </div>
    
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            var message = $('#btns');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btnss');
            message.show('slow');
        });
        $(document).ready(function () {
            var message = $('#btnsss');
            message.show('slow');
        });
        function selectAll(box) {
            var chboxs = document.getElementsByName("checkScheduel[]");
		    for(var i in chboxs){
                chboxs[i].checked = box.checked  
                $('#confrimScheduel').hide();
            }
            if($('#selectall').checked = box.checked){
                $('#confrimScheduel').show();
            } 
        }
        function select1(box) {
            var chboxs = document.getElementsByName("checkScheduel[]");
            var vis = "none";
            for(var i=0;i<chboxs.length;i++) { 
                if(chboxs[i].checked){
                    vis = "block";
                    break;
                }
            }
                document.getElementById(box).style.display = vis;
        }
        function selectAll1(box) {
            var chboxs = document.getElementsByName("uncheckScheduel[]");
		    for(var i in chboxs){
                chboxs[i].checked = box.checked  
                $('#unconfrimScheduel').hide();
            }
            if($('#selectall1').checked = box.checked){
                $('#unconfrimScheduel').show();
            } 
        }
        function select11(box) {
            var chboxs = document.getElementsByName("uncheckScheduel[]");
            var vis = "none";
            for(var i=0;i<chboxs.length;i++) { 
                if(chboxs[i].checked){
                    vis = "block";
                    break;
                }
            }
                document.getElementById(box).style.display = vis;
        }
    </script>

@stop



