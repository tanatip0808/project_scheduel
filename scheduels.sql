-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2017 at 09:34 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scheduels`
--

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE `classroom` (
  `id` int(3) NOT NULL,
  `classroom_name` varchar(255) NOT NULL,
  `status_room_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classroom`
--

INSERT INTO `classroom` (`id`, `classroom_name`, `status_room_id`) VALUES
(1, 'ปกติ', 1),
(2, 'คอม', 1);

-- --------------------------------------------------------

--
-- Table structure for table `course_data`
--

CREATE TABLE `course_data` (
  `id` int(11) NOT NULL,
  `study_course_id` int(3) NOT NULL,
  `credit` decimal(2,1) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `time_study` int(2) NOT NULL,
  `sceduel_time` int(3) NOT NULL,
  `subject_matter_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_data`
--

INSERT INTO `course_data` (`id`, `study_course_id`, `credit`, `subject_name`, `time_study`, `sceduel_time`, `subject_matter_id`) VALUES
(1, 1, '1.5', 'วิทยาศาสตร์', 60, 15, 1),
(3, 1, '3.0', 'ภาษาอังกฤษ', 40, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `date_time`
--

CREATE TABLE `date_time` (
  `id` int(2) NOT NULL,
  `date_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `date_time`
--

INSERT INTO `date_time` (`id`, `date_time`) VALUES
(1, 'วันจันทร์ 08.30-09.30'),
(2, 'วันจันทร์ 09.30-10.30'),
(3, 'วันจันทร์ 10.30-11.30'),
(4, 'วันจันทร์ 11.30-12.30'),
(5, 'วันจันทร์ 12.30-13.30'),
(6, 'วันจันทร์ 13.30-14.30'),
(7, 'วันจันทร์ 14.30-15.30'),
(8, 'วันจันทร์ 15.30-16.30'),
(9, 'วันจันทร์ 16.30-17.30'),
(10, 'วันอังคาร 08.30-09.30'),
(11, 'วันอังคาร 09.30-10.30'),
(12, 'วันอังคาร 10.30-11.30'),
(13, 'วันอังคาร 11.30-12.30'),
(14, 'วันอังคาร 12.30-13.30'),
(15, 'วันอังคาร 13.30-14.30'),
(16, 'วันอังคาร 14.30-15.30'),
(17, 'วันอังคาร 15.30-16.30'),
(18, 'วันอังคาร 16.30-17.30'),
(19, 'วันพุธ 08.30-09.30'),
(20, 'วันพุธ 09.30-10.30'),
(21, 'วันพุธ 10.30-11.30'),
(22, 'วันพุธ 11.30-12.30'),
(23, 'วันพุธ 12.30-13.30'),
(24, 'วันพุธ 13.30-14.30'),
(25, 'วันพุธ 14.30-15.30'),
(26, 'วันพุธ 15.30-16.30'),
(27, 'วันพุธ 16.30-17.30'),
(28, 'วันพฤหัสบดี 08.30-09.30'),
(29, 'วันพฤหัสบดี 09.30-10.30'),
(30, 'วันพฤหัสบดี 10.30-11.30'),
(31, 'วันพฤหัสบดี 11.30-12.30'),
(32, 'วันพฤหัสบดี 12.30-13.30'),
(33, 'วันพฤหัสบดี 13.30-14.30'),
(34, 'วันพฤหัสบดี 14.30-15.30'),
(35, 'วันพฤหัสบดี 15.30-16.30'),
(36, 'วันพฤหัสบดี 16.30-17.30'),
(37, 'วันศุกร์ 08.30-09.30'),
(38, 'วันศุกร์ 09.30-10.30'),
(39, 'วันศุกร์ 10.30-11.30'),
(40, 'วันศุกร์ 11.30-12.30'),
(41, 'วันศุกร์ 12.30-13.30'),
(42, 'วันศุกร์ 13.30-14.30'),
(43, 'วันศุกร์ 14.30-15.30'),
(44, 'วันศุกร์ 15.30-16.30'),
(45, 'วันศุกร์ 16.30-17.30');

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE `instructor` (
  `id` int(5) NOT NULL,
  `personel_id` int(1) NOT NULL,
  `classroom_id` int(2) NOT NULL,
  `course_data_id` int(2) NOT NULL,
  `student_group_id` int(3) NOT NULL,
  `year_of_study_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`id`, `personel_id`, `classroom_id`, `course_data_id`, `student_group_id`, `year_of_study_id`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 2, 2, 3, 2, 1),
(3, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `personel`
--

CREATE TABLE `personel` (
  `id` int(5) NOT NULL,
  `position_id` int(1) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `lastchang` int(3) NOT NULL,
  `status_id` int(1) NOT NULL,
  `subject_matter_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personel`
--

INSERT INTO `personel` (`id`, `position_id`, `first_name`, `last_name`, `tel`, `lastchang`, `status_id`, `subject_matter_id`) VALUES
(1, 2, 'chainarong', 'sanmee', '0944501098', 1, 1, 1),
(2, 2, 'ลาดพร้าว', 'ซอย3', '0877099198', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `id` int(2) NOT NULL,
  `position_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `position_name`) VALUES
(1, 'ผู้อำนวยการ'),
(2, 'อาจารย์');

-- --------------------------------------------------------

--
-- Table structure for table `scheduel`
--

CREATE TABLE `scheduel` (
  `id` int(11) NOT NULL,
  `instuctor_id` int(11) NOT NULL,
  `date_time_id` int(2) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scheduel`
--

INSERT INTO `scheduel` (`id`, `instuctor_id`, `date_time_id`, `status`) VALUES
(244, 1, 3, 1),
(245, 1, 1, 1),
(246, 1, 9, 1),
(247, 1, 2, 1),
(248, 1, 45, 1),
(249, 2, 11, 1),
(250, 2, 12, 1),
(251, 1, 21, 1),
(252, 1, 22, 1),
(253, 1, 23, 1),
(254, 1, 24, 1),
(255, 1, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(1) NOT NULL,
  `status_detail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_detail`) VALUES
(1, 'อยู่'),
(2, 'ไม่อยู่');

-- --------------------------------------------------------

--
-- Table structure for table `status_room`
--

CREATE TABLE `status_room` (
  `id` int(3) NOT NULL,
  `day_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_room`
--

INSERT INTO `status_room` (`id`, `day_time`) VALUES
(1, '0'),
(2, 'ไม่ว่าง');

-- --------------------------------------------------------

--
-- Table structure for table `student_group`
--

CREATE TABLE `student_group` (
  `id` int(5) NOT NULL,
  `class` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_group`
--

INSERT INTO `student_group` (`id`, `class`) VALUES
(1, '1/1'),
(2, '1/2');

-- --------------------------------------------------------

--
-- Table structure for table `study_course`
--

CREATE TABLE `study_course` (
  `id` int(3) NOT NULL,
  `study_course_name` varchar(255) NOT NULL,
  `detail_result` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `study_course`
--

INSERT INTO `study_course` (`id`, `study_course_name`, `detail_result`) VALUES
(1, '2551', '2551');

-- --------------------------------------------------------

--
-- Table structure for table `subject_metter`
--

CREATE TABLE `subject_metter` (
  `id` int(2) NOT NULL,
  `subject_metter_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject_metter`
--

INSERT INTO `subject_metter` (`id`, `subject_metter_name`) VALUES
(1, 'วิทยาศาสตร์'),
(2, 'ภาษาอังกฤษ');

-- --------------------------------------------------------

--
-- Table structure for table `year_of_study`
--

CREATE TABLE `year_of_study` (
  `id` int(11) NOT NULL,
  `year` varchar(4) NOT NULL,
  `term` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `year_of_study`
--

INSERT INTO `year_of_study` (`id`, `year`, `term`) VALUES
(1, '2560', 1),
(2, '2560', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classroom`
--
ALTER TABLE `classroom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_room_id` (`status_room_id`);

--
-- Indexes for table `course_data`
--
ALTER TABLE `course_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subject_matter_id` (`subject_matter_id`),
  ADD KEY `study_course_id` (`study_course_id`),
  ADD KEY `subject_matter_id_2` (`subject_matter_id`),
  ADD KEY `subject_matter_id_3` (`subject_matter_id`);

--
-- Indexes for table `date_time`
--
ALTER TABLE `date_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personel_id` (`personel_id`),
  ADD KEY `classroom_id` (`classroom_id`),
  ADD KEY `course_id` (`course_data_id`,`student_group_id`,`year_of_study_id`),
  ADD KEY `student_group_id` (`student_group_id`),
  ADD KEY `year_of_study_id` (`year_of_study_id`);

--
-- Indexes for table `personel`
--
ALTER TABLE `personel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject_matter_id` (`subject_matter_id`),
  ADD KEY `position_id` (`position_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheduel`
--
ALTER TABLE `scheduel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personel_id` (`date_time_id`,`status`),
  ADD KEY `instuctor_id` (`instuctor_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_room`
--
ALTER TABLE `status_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_group`
--
ALTER TABLE `student_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_course`
--
ALTER TABLE `study_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_metter`
--
ALTER TABLE `subject_metter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `year_of_study`
--
ALTER TABLE `year_of_study`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classroom`
--
ALTER TABLE `classroom`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course_data`
--
ALTER TABLE `course_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `date_time`
--
ALTER TABLE `date_time`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personel`
--
ALTER TABLE `personel`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scheduel`
--
ALTER TABLE `scheduel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status_room`
--
ALTER TABLE `status_room`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_group`
--
ALTER TABLE `student_group`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `study_course`
--
ALTER TABLE `study_course`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subject_metter`
--
ALTER TABLE `subject_metter`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `year_of_study`
--
ALTER TABLE `year_of_study`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classroom`
--
ALTER TABLE `classroom`
  ADD CONSTRAINT `classroom_ibfk_1` FOREIGN KEY (`status_room_id`) REFERENCES `status_room` (`id`);

--
-- Constraints for table `course_data`
--
ALTER TABLE `course_data`
  ADD CONSTRAINT `course_data_ibfk_1` FOREIGN KEY (`study_course_id`) REFERENCES `study_course` (`id`),
  ADD CONSTRAINT `course_data_ibfk_2` FOREIGN KEY (`subject_matter_id`) REFERENCES `subject_metter` (`id`);

--
-- Constraints for table `instructor`
--
ALTER TABLE `instructor`
  ADD CONSTRAINT `instructor_ibfk_1` FOREIGN KEY (`personel_id`) REFERENCES `personel` (`id`),
  ADD CONSTRAINT `instructor_ibfk_2` FOREIGN KEY (`classroom_id`) REFERENCES `classroom` (`id`),
  ADD CONSTRAINT `instructor_ibfk_3` FOREIGN KEY (`course_data_id`) REFERENCES `course_data` (`id`),
  ADD CONSTRAINT `instructor_ibfk_4` FOREIGN KEY (`student_group_id`) REFERENCES `student_group` (`id`),
  ADD CONSTRAINT `instructor_ibfk_5` FOREIGN KEY (`year_of_study_id`) REFERENCES `year_of_study` (`id`);

--
-- Constraints for table `personel`
--
ALTER TABLE `personel`
  ADD CONSTRAINT `personel_ibfk_1` FOREIGN KEY (`subject_matter_id`) REFERENCES `subject_metter` (`id`),
  ADD CONSTRAINT `personel_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`),
  ADD CONSTRAINT `personel_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `scheduel`
--
ALTER TABLE `scheduel`
  ADD CONSTRAINT `scheduel_ibfk_1` FOREIGN KEY (`instuctor_id`) REFERENCES `instructor` (`id`),
  ADD CONSTRAINT `scheduel_ibfk_2` FOREIGN KEY (`date_time_id`) REFERENCES `date_time` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
