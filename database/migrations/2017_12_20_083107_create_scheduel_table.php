<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status');

            $table->integer('instructors_id')->unsigned();
            $table->foreign('instructors_id')->references('id')->on('instructors');
            $table->integer('times_id')->unsigned();
            $table->integer('days_id')->unsigned();
            $table->foreign('days_id')->references('id')->on('days');
            $table->foreign('times_id')->references('id')->on('times');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduels');
    }
}
