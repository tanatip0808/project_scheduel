<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructors', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('credit');
            $table->integer('times_study');
            $table->integer('check_times_study');
            $table->integer('scheduel_times');
            $table->string('coach');
            $table->string('color');

            $table->integer('courses_id')->unsigned();
            $table->foreign('courses_id')->references('id')->on('courses');
            $table->integer('personels_id')->unsigned();
            $table->foreign('personels_id')->references('id')->on('personels');
            $table->integer('classrooms_id')->unsigned();
            $table->foreign('classrooms_id')->references('id')->on('classrooms');
            $table->integer('student_groups_id')->unsigned();
            $table->foreign('student_groups_id')->references('id')->on('student_groups');
            $table->integer('study_courses_id')->unsigned();
            $table->foreign('study_courses_id')->references('id')->on('study_courses');
            $table->integer('year_studies_id')->unsigned();
            $table->foreign('year_studies_id')->references('id')->on('year_studies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructors');
    }
}
