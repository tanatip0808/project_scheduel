<?php

namespace App\Http\Controllers\Director;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
 
class PageController extends Controller
{
    public function getHome(){
  
      return view('director.home');
    
    }

    public function getViewPersonel(){

      $count_personels = DB::table('personels')->count();

      $view_personels = DB::table('personels')
                          ->leftJoin('statuses', 'statuses.id', '=', 'personels.statuses_id')
                          ->leftJoin('positions', 'positions.id', '=', 'personels.positions_id')
                          ->leftJoin('subject_matters', 'subject_matters.id', '=', 'personels.subject_matters_id')
                          ->select(
                                    'personels.first_name',
                                    'personels.last_name',
                                    'personels.tel',                                    
                                    'statuses.status_detail',
                                    'positions.position_name',
                                    'subject_matters.subject_matter_name'
                            )
                          ->orderBy('subject_matter_name', 'asc')
                          ->Paginate(15);
                    
      return view('director.view_Personels',compact('count_personels','view_personels'));

    }

    public function getViewScheduel(){

      $subject_matters = DB::table('subject_matters')
                           ->orderBy('subject_matter_name')
                           ->get();

      $year_studies = DB::table('year_studies')
                        ->orderBy('year')
                        ->orderBy('term')
                        ->get();

      return view('director.view_scheduel',compact('year_studies','subject_matters'));

    }

    public function getViewStudentGroups(){

      $year_studies = DB::table('year_studies')
                        ->orderBy('year')
                        ->orderBy('term')
                        ->get();

      return view('director.view_scheduel_student_group',compact('year_studies'));

    }

    public function getReturnStudentGroups($id,$ids){

      $count_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                           ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                           ->where('year_studies_id', '=', $ids)
                           ->count();


      $show_year_studies = DB::table('year_studies')
                             ->where('year_studies.id', '=', $ids)
                             ->select(
                                        'year_studies.id',
                                        'year_studies.year',
                                        'year_studies.term')
                             ->first();

      $year_studies = DB::table('year_studies')
                        ->orderBy('year')
                        ->orderBy('term')
                        ->get();

      $year_studiesss = $ids;

      $loop_scheduel = DB::table('scheduels')
                         ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                         ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                         ->where('year_studies_id', '=', $ids)
                         ->select(
                                   'student_groups.class',
                                   'scheduels.times_id',
                                   'scheduels.days_id',
                                   'instructors.student_groups_id'
                           )
                         ->groupBy('student_groups_id')
                         ->get();

      foreach($loop_scheduel as $loop_scheduels)
      {
        $check_status_scheduel = DB::table('scheduels')
                                   ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                   ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                   ->where('year_studies_id', '=', $ids)
                                   ->where('instructors.student_groups_id', '=', $loop_scheduels->student_groups_id)
                                   ->where('status', '=', 1)
                                   ->groupBy('student_groups.id')
                                   ->count();

        $check_scheduel_count = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                  ->where('year_studies_id', '=', $ids)
                                  ->where('instructors.student_groups_id', '=', $loop_scheduels->student_groups_id)
                                  ->groupBy('student_groups.id')
                                  ->count();

        $check_scheduel_personels = DB::table('scheduels')
                                      ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                      ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                      ->where('year_studies_id', '=', $ids)
                                      ->where('instructors.student_groups_id', '=', $loop_scheduels->student_groups_id)
                                      ->select(
                                                'student_groups.class',
                                                'instructors.student_groups_id'
                                        )
                                      ->groupBy('student_groups.id')
                                      ->get();

        $check_scheduels_student_groups = DB::table('student_groups')
                                            ->where('student_groups.id', '=', $loop_scheduels->student_groups_id)
                                            ->first();

        $check_status_scheduels[]    = $check_status_scheduel;
        $check_scheduels_count[]     = $check_scheduel_count;
        $check_scheduels_personels[] = $check_scheduel_personels;
        $check_student_groups[]      = substr($check_scheduels_student_groups->class,0,1);
      }

      $count_Unconfrim_scheduels = DB::table('scheduels')
                                     ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                     ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                     ->where('year_studies_id', '=', $ids)
                                     ->where('status', '=', 0)
                                     ->count();

      return view('director.view_scheduel_year_student_group',compact('count_scheduels','show_year_studies','year_studies','check_student_groups','check_status_scheduels','check_scheduels_count','check_scheduels_personels','check_student_groups','count_Unconfrim_scheduels','year_studiesss'));
    
    }

    public function getViewTableStudentGroups($id,$ids,$idss){

      $count_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                             ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                             ->where('student_groups.id', '=', $id)
                             ->where('year_studies_id', '=', $ids)
                             ->count();

      $show_student_groups = DB::table('student_groups')
                               ->where('student_groups.id', '=', $id)
                               ->select(
                                         'student_groups.class'
                                 )
                               ->first();

      $show_year_studies = DB::table('year_studies')
                             ->where('year_studies.id', '=', $ids)
                             ->select(
                                        'year_studies.id',
                                        'year_studies.year',
                                        'year_studies.term')
                             ->first();

      $student_groups = $id;

      $year_studies = $ids;

      $scheduels = DB::table('scheduels')
                     ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                     ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                     ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                     ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                       ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                                                                         
                       ->orderBy('days_id')
                       ->select(
                                 'scheduels.id as scheduels_id',
                                 'scheduels.times_id',
                                 'scheduels.days_id',
                                 'instructors.id as instructors_id',
                                 'student_groups.class',
                                 'courses.courses_name',
                                 'courses.courses_id',
                                 'classrooms.classroom_type',
                                 'personels.first_name',
                                 'personels.last_name',
                                 'scheduels.status',
                                 'instructors.coach'
                         )
                       ->where('student_groups.id', '=', $id)
                       ->where('year_studies_id', '=', $ids)
                       ->get();

      $scheduels  = $scheduels->sortBy('times_id')->toArray();

      for ($i=0; $i < count($scheduels); $i++) {
          $check      [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->status;      
          $data       [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->first_name;
          $datass     [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->last_name;        
          $datacourse [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->courses_id;
          $day        [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->days_id;
          $time       [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->times_id;
          $classroom  [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->classroom_type;
          $coach      [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->coach;
      }

      $check_confrim_scheduel = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                  ->select(
                                            'status'
                                    )                                     
                                  ->where('student_groups.id', '=', $id)
                                  ->where('year_studies_id', '=', $ids)
                                  ->first();
      $check_scheduel_unconfrim = DB::table('scheduels')
                                    ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                    ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                    ->where('student_groups.id', '=', $id)
                                    ->where('year_studies_id', '=', $ids)
                                    ->where('status', '=',  0)
                                    ->count();
                                
      $check_scheduel_confrim = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                                  ->where('student_groups.id', '=', $id)
                                  ->where('year_studies_id', '=', $ids)
                                  ->where('status', '=',  1)
                                  ->count();

      $check_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                           ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                           ->where('student_groups.id', '=', $id)
                           ->where('year_studies_id', '=', $ids)
                           ->count();

      $check_scheduels_student_groups = DB::table('student_groups')
                                          ->where('student_groups.id', '=', $id)
                                          ->first();

      $check_student_groups = $idss;

      return view('director.view_scheduel_student_groups',compact('day','time','scheduels','data','datacourse','classroom','datass','student_groups','show_student_groups','count_scheduels','show_year_studies','year_studies','confrim_student_groups','check_confrim_scheduel','check_scheduel_unconfrim','check_scheduel_confrim','check_scheduels','check_student_groups','check','coach','student_groups'));

    }

    public function getViewTable($id,$ids){
      
      $scheduels = DB::table('scheduels')
                     ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                     ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                     ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                     ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                     ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                                                      
                     ->orderBy('days_id')
                     ->select(
                               'scheduels.id as scheduels_id',
                               'scheduels.times_id',
                               'scheduels.days_id',
                               'instructors.id as instructors_id',
                               'student_groups.class',
                               'courses.courses_name',
                               'courses.courses_id',
                               'classrooms.classroom_type',
                               'personels.first_name',
                               'personels.last_name',
                               'scheduels.status',
                               'instructors.coach'
                       )
                     ->where('personels.id', '=', $id)
                     ->where('year_studies_id', '=', $ids)
                     ->get();

      $scheduels  = $scheduels->sortBy('times_id')->toArray();

      for ($i=0; $i < count($scheduels); $i++) {
          $check      [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->status;
          $data       [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->class;
          $datacourse [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->courses_id;
          $day        [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->days_id;
          $time       [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->times_id;
          $classroom  [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->classroom_type;
          $coach      [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->coach;
      }

      $show_personels = DB::table('personels')
                          ->where('personels.id', '=', $id)
                          ->first();

      $show_subject_matters = DB::table('personels')
                                ->where('personels.id', '=', $id)
                                ->select(
                                          'personels.subject_matters_id'
                                  )
                                ->first();

      $count_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                           ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                           ->where('personels.id', '=', $id)
                           ->count();

      $year_studies = $ids;

      $show_year_studies = DB::table('year_studies')
                             ->where('year_studies.id', '=', $ids)
                             ->select(
                                        'year_studies.id',
                                        'year_studies.year',
                                        'year_studies.term')
                             ->first();

      $check_confrim_scheduel = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                  ->select(
                                            'status'
                                    )                                
                                  ->where('personels.id', '=', $id)
                                  ->where('year_studies_id', '=', $ids)
                                  ->first();

      $check_scheduel_unconfrim = DB::table('scheduels')
                                    ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                    ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                    ->where('personels.id', '=', $id)
                                    ->where('year_studies_id', '=', $ids)
                                    ->where('status', '=', 0)
                                    ->count();

      $check_scheduel_confrim = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                  ->where('personels.id', '=', $id)
                                  ->where('year_studies_id', '=', $ids)
                                  ->where('status', '=', 1)
                                  ->count();
                          
      return view('director.view_scheduel_personels',compact('day','time','scheduels','data','datacourse','classroom','personels','show_personels','show_subject_matters','count_scheduels','year_studies','show_year_studies','check_confrim_scheduel','check_scheduel_unconfrim','check_scheduel_confrim','check','coach'));
      
    }

    public function getReturn($id,$ids){
      
      $count_subject_matters = DB::table('scheduels')
                                 ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                 ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                 ->where('subject_matters_id', '=', $id)
                                 ->where('year_studies_id', '=', $ids)                              
                                 ->count();

      $subject_matters = DB::table('subject_matters')
                           ->orderBy('subject_matter_name')
                           ->get();
  
      $show_subject_matters = DB::table('subject_matters')
                                ->where('subject_matters.id', '=', $id)
                                ->select(
                                          'subject_matters.subject_matter_name',
                                          'subject_matters.id'
                                  )
                                ->first();
      
      $show_year_studies = DB::table('year_studies')
                             ->where('year_studies.id', '=', $ids)
                             ->select(
                                        'year_studies.id',
                                        'year_studies.year',
                                        'year_studies.term')
                             ->first();
  
      $year_studies = $ids;

      $loop_scheduel = DB::table('scheduels')
                       ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                       ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                       ->where('subject_matters_id', '=', $id)
                       ->where('year_studies_id', '=', $ids)
                       ->select(
                                 'personels.first_name',
                                 'personels.last_name',
                                 'personels.id',
                                 'scheduels.times_id',
                                 'scheduels.days_id',
                                 'instructors.personels_id'
                         )
                       ->groupBy('personels_id')
                       ->get();
                                  
      foreach($loop_scheduel as $loop_scheduels)
      {
        $check_status_scheduel = DB::table('scheduels')
                                   ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                   ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                   ->where('subject_matters_id', '=', $id)
                                   ->where('year_studies_id', '=', $ids)
                                   ->where('personels.id', '=', $loop_scheduels->personels_id)
                                   ->where('status', '=', 1)
                                   ->groupBy('personels.id')
                                   ->count();
        
        $check_scheduel_count = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                  ->where('subject_matters_id', '=', $id)
                                  ->where('year_studies_id', '=', $ids)
                                  ->where('personels.id', '=', $loop_scheduels->personels_id)
                                  ->groupBy('personels.id')
                                  ->count();
  
        $check_scheduel_personels = DB::table('scheduels')
                                      ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                      ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                      ->where('subject_matters_id', '=', $id)
                                      ->where('year_studies_id', '=', $ids)
                                      ->where('personels.id', '=', $loop_scheduels->personels_id)
                                      ->select(
                                                'personels.first_name',
                                                'personels.last_name',
                                                'instructors.personels_id'
                                        )
                                      ->groupBy('personels_id')
                                      ->get();
  
        $check_status_scheduels[]    = $check_status_scheduel;
        $check_scheduels_count[]     = $check_scheduel_count;
        $check_scheduels_personels[] = $check_scheduel_personels;
      }
  
      $count_Unconfrim_scheduels = DB::table('scheduels')
                                     ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                     ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                     ->where('subject_matters_id', '=', $id)
                                     ->where('year_studies_id', '=', $ids)
                                     ->where('status', '=', 0)
                                     ->count();

      $count_confrim_scheduels = DB::table('scheduels')
                                   ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                   ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                   ->where('subject_matters_id', '=', $id)
                                   ->where('year_studies_id', '=', $ids)
                                   ->where('status', '=', 1)
                                   ->count();
  
      return view('director.view_scheduel_subject_matter',compact('subject_matters','count_subject_matters','show_subject_matters','show_year_studies','year_studies','count_unCheck_scheduels','count_Check_scheduels','count_Unconfrim_scheduels','check_status_scheduels','check_scheduels_count','check_scheduels_personels','count_confrim_scheduels'));
    
    }
}
