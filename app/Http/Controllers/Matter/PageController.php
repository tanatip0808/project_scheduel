<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use Excel;
use Auth;
 
class PageController extends Controller
{
    public function getHome(){

      return view('sub_matter.home');
    
    }

    public function getAddInstructor(){
      
      // $study_courses = DB::table('study_courses')
      //                    ->orderBy('study_course_name')
      //                    ->get();

      $courses = DB::table('courses')
                   ->orderBy('courses_id')
                   ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                   ->get();    

      $personels = DB::table('personels')
                     ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                     ->orderBY('first_name')
                     ->get();

      $classrooms = DB::table('classrooms')
                      ->orderBy('classroom_type')
                      ->get();

      $student_groups = DB::table('student_groups')
                          ->orderBY('class')
                          ->get();

      $year_studies = DB::table('year_studies')
                        ->orderBy('year')
                        ->orderBy('term')
                        ->get();

      return view('sub_matter.add_Instructor',compact('classrooms','personels','student_groups','year_studies','courses'));
    
    }

    public function getViewPersonel(){

      $count_personels = DB::table('personels')
                           ->where('subject_matters_id', '=', Auth::user()->subject_mat_id) 
                           ->count();

      $view_personels = DB::table('personels')
                          ->leftJoin('statuses', 'statuses.id', '=', 'personels.statuses_id')
                          ->leftJoin('positions', 'positions.id', '=', 'personels.positions_id')
                          ->leftJoin('subject_matters', 'subject_matters.id', '=', 'personels.subject_matters_id')
                          ->select(
                                    'personels.first_name',
                                    'personels.last_name',
                                    'personels.tel',                                    
                                    'statuses.status_detail',
                                    'positions.position_name',
                                    'subject_matters.subject_matter_name'
                            )
                          ->where('subject_matters_id', '=', Auth::user()->subject_mat_id) 
                          ->orderBy('first_name', 'asc')
                          ->Paginate(15);

      $personels_subject_matters = DB::table('subject_matters')
                                     ->where('id', '=', Auth::user()->subject_mat_id)
                                     ->first();

      $subject_matters_name = DB::table('subject_matters')
                                ->where('id', '=', Auth::user()->subject_mat_id)
                                ->select(
                                           'subject_matter_name'
                                  )
                                ->first();

      return view('sub_matter.view_Personels',compact('count_personels','view_personels','personels_subject_matters','subject_matters_name'));

    }

    public function getViewTable($id,$ids){
      
      $scheduels = DB::table('scheduels')
                   ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                   ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                   ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                   ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                   ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                   
                   ->orderBy('days_id')
                   ->select(
                             'scheduels.id as scheduels_id',
                             'scheduels.times_id',
                             'scheduels.days_id',
                             'instructors.id as instructors_id',
                             'student_groups.class',
                             'courses.courses_name',
                             'courses.courses_id',
                             'classrooms.classroom_type',
                             'personels.first_name',
                             'personels.last_name',
                             'coach'
                     )
                   ->where('personels.id', '=', $id)
                   ->where('year_studies_id', '=', $ids)
                   ->get();

      $count_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                           ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                           ->where('personels.id', '=', $id)
                           ->count();

      $scheduels  = $scheduels->sortBy('times_id')->toArray();

      for ($i=0; $i < count($scheduels); $i++) 
      {
        $data       [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->class;
        $datacourse [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->courses_id;
        $day        [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->days_id;
        $time       [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->times_id;
        $classroom  [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->classroom_type;
        $coach      [$scheduels[$i]->days_id.$scheduels[$i]->times_id]=$scheduels[$i]->coach;
      }
      $personel_pdf = $id;

      $personel_pdfs = $ids;

      $personel_excel = $id;

      $show_personels = DB::table('personels')
                          ->where('personels.id', '=', $id)
                          ->first();

      $show_year_studies = DB::table('year_studies')
                             ->where('year_studies.id', '=', $ids)
                             ->first();

      $check_confrim_scheduel = DB::table('scheduels')
                                  ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                  ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                  ->select(
                                            'status'
                                    )                                
                                  ->where('personels.id', '=', $id)
                                  ->where('year_studies_id', '=', $ids)
                                  ->first();

      $check_scheduel = DB::table('scheduels')
                          ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                          ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                          ->where('personels.id', '=', $id)
                          ->where('year_studies_id', '=', $ids)
                          ->count();

      return view('sub_matter.view_Scheduel_personels',compact('day','time','scheduels','data','datacourse','classroom','personel_pdf','personel_excel','count_scheduels','show_personels','personel_pdfs','show_year_studies','check_confrim_scheduel','check_scheduel','coach'));
      
    }

    public function getViewInstructorYearStudies(){

      $year_studies = DB::table('year_studies')
                        ->orderBy('year')
                        ->orderBy('term')
                        ->get();

      return view('sub_matter.view_Instructor_year_studies',compact('year_studies'));
      
    }

    public function getViewInstructor(Request $request){

      $count_instructors = DB::table('instructors')
                             ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id') 
                             ->where('year_studies_id', '=', $request->id)
                             ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)     
                             ->count();

      $personelss = DB::table('instructors')
                      ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                      ->select(
                                'personels.id',
                                'personels.first_name',
                                'personels.last_name'
                        )
                      ->where('year_studies_id', '=', $request->id)
                      ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)                       
                      ->groupBy('first_name')                                  
                      ->get();                             

      $personels = DB::table('personels')
                     ->where('subject_matters_id', '=', Auth::user()->subject_mat_id) 
                     ->orderBY('first_name')
                     ->get();

      $year_name = DB::table('year_studies')
                     ->where('id', '=', $request->id)
                     ->select(
                                'year',
                                'term'
                       )
                     ->orderBY('year')
                     ->orderBY('term')
                     ->first();

      // $student_groups = DB::table('student_groups')
      //                     ->orderBY('class')
      //                     ->get();

      $classrooms = DB::table('classrooms')
                      ->orderBy('classroom_type')
                      ->get();

      $view_instructors = DB::table('instructors')
                            ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                            ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                            ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                            ->leftJoin('year_studies' , 'year_studies.id', '=' , 'instructors.year_studies_id')
                            ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')
                            ->select(
                                      'instructors.id',
                                      'courses.courses_id',
                                      'courses.courses_name',
                                      'personels.first_name',
                                      'personels.last_name',
                                      'courses.times_study',
                                      'student_groups.class',
                                      'classrooms.classroom_type',
                                      'year_studies.term',
                                      'year_studies.year',
                                      'instructors.personels_id',
                                      'instructors.coach'
                              )
                            ->orderBy('courses_id', 'asc')
                            ->where('year_studies_id', '=', $request->id)
                            ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)       
                            ->Paginate(12);

      $personel_pdf = '0';

      $year_studies = $request->id;

      return view('sub_matter.view_Instructor',compact('personels','personelss','view_instructors','count_instructors','personel_pdf','classrooms','year_studies','year_name'));      
                            
    }

    public function loadPdfPersonels(){

      $count_personels = DB::table('personels')
                           ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                           ->count();

      $view_personels = DB::table('personels')
                          ->leftJoin('statuses', 'statuses.id', '=', 'personels.statuses_id')
                          ->leftJoin('positions', 'positions.id', '=', 'personels.positions_id')
                          ->leftJoin('subject_matters', 'subject_matters.id', '=', 'personels.subject_matters_id')
                          ->select(
                                    'personels.first_name',
                                    'personels.last_name',
                                    'personels.tel',                                    
                                    'statuses.status_detail',
                                    'positions.position_name',
                                    'subject_matters.subject_matter_name'
                            )
                          ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                          ->orderBy('first_name', 'asc')
                          ->get();

      $personels_subject_matters = DB::table('subject_matters')
                                     ->where('id', '=', Auth::user()->subject_mat_id)
                                     ->first();

      $subject_matters_name = DB::table('subject_matters')
                                ->where('id', '=', Auth::user()->subject_mat_id)
                                ->select( 
                                          'subject_matter_name'
                                  )
                                ->first();

      $pdf = PDF::loadView('sub_matter.loadPdf_Personels',compact('view_personels','count_personels','personels_subject_matters','subject_matters_name'));
        
      $pdf->setPaper('A4', 'portrait');

      return @$pdf->stream(); 

    }

    public function loadPdfInstructor($id,$ids){
      
      if($id == '0')
      {
        $view_instructors = DB::table('instructors')
                              ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                              ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                              ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                              ->leftJoin('year_studies' , 'year_studies.id', '=' , 'instructors.year_studies_id')
                              ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                              
                              ->select(
                                        'instructors.id',
                                        'courses.courses_id',
                                        'courses.courses_name',
                                        'personels.first_name',
                                        'personels.last_name',
                                        'courses.times_study',
                                        'student_groups.class',
                                        'classrooms.classroom_type',
                                        'year_studies.term',
                                        'year_studies.year',
                                        'instructors.coach'
                                )
                              ->orderBy('courses_id', 'asc')
                              ->where('year_studies_id', '=', $ids)
                              ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)                                   
                              ->get();

        $count_instructors = DB::table('instructors')
                               ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                               ->where('year_studies_id', '=', $ids)  
                               ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)                                  
                               ->count();
                               
        $instructor_subject_matters = DB::table('subject_matters')
                                        ->where('id', '=', Auth::user()->subject_mat_id)
                                        ->first();

        $subject_matters_name = DB::table('subject_matters')
                                  ->where('id', '=', Auth::user()->subject_mat_id)
                                  ->select( 
                                            'subject_matter_name'
                                    )
                                  ->first();

        $year_name = DB::table('year_studies')
                       ->where('id', '=', $ids)
                       ->select(
                                  'year',
                                  'term'
                         )
                       ->orderBY('year')
                       ->orderBY('term')
                       ->first();

        $pdf = PDF::loadView('sub_matter.loadPdf_Instructor',compact('view_instructors','count_instructors','instructor_subject_matters','subject_matters_name','year_name'));
        
        $pdf->setPaper('A4', 'portrait');

        return @$pdf->stream();  
      }
      
    }

    public function loadPdfInstructorPersonels($id,$ids){
      
      $view_instructors = DB::table('instructors')
                              ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                              ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                              ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                              ->leftJoin('year_studies' , 'year_studies.id', '=' , 'instructors.year_studies_id')
                              ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                            
                              ->select(
                                        'instructors.id',
                                        'courses.courses_id',
                                        'courses.courses_name',
                                        'personels.first_name',
                                        'personels.last_name',
                                        'courses.times_study',
                                        'student_groups.class',
                                        'classrooms.classroom_type',
                                        'year_studies.term',
                                        'year_studies.year',
                                        'instructors.coach'
                                )
                              ->where('personels_id', '=', $id)
                              ->where('year_studies_id', '=', $ids)  
                              ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)                                                               
                              ->orderBy('courses_id', 'asc')
                              ->get();

      $count_instructors = DB::table('instructors')
                             ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                             ->where('personels_id', '=', $id)
                             ->where('year_studies_id', '=', $ids)
                             ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)                                                                  
                             ->count();

      $instructor_subject_matters = DB::table('subject_matters')
                                      ->where('id', '=', Auth::user()->subject_mat_id)
                                      ->first();
      $subject_matters_name = DB::table('subject_matters')
                                ->where('id', '=', Auth::user()->subject_mat_id)
                                ->select( 
                                          'subject_matter_name'
                                  )
                                ->first();

      $personels_name = DB::table('personels')
                          ->where('id', '=', $id)
                          ->select( 
                                    'first_name',
                                    'last_name'
                            )
                          ->first();

      $year_name = DB::table('year_studies')
                     ->where('id', '=', $ids)
                     ->select(
                                'year',
                                'term'
                       )
                     ->orderBY('year')
                     ->orderBY('term')
                     ->first();

      $pdf = PDF::loadView('sub_matter.loadPdf_Instructor_personels',compact('view_instructors','count_instructors','instructor_subject_matters','subject_matters_name','personels_name','year_name'));
      
      $pdf->setPaper('A4', 'portrait');
      
      return @$pdf->stream(); 

    }

    public function loadPdfScheduel($id,$ids){
      
      $personels = DB::table('personels')
                   ->where('personels.id', '=', $id)
                   ->first();

      $scheduel = DB::table('scheduels')
                    ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                    ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                    ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                    ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                    ->leftJoin('year_studies', 'year_studies.id', '=', 'instructors.year_studies_id')   
                    ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                                                          
                    ->orderBy('days_id')
                    ->select(
                              'scheduels.id as scheduels_id',
                              'scheduels.times_id',
                              'scheduels.days_id',
                              'instructors.id as instructors_id',
                              'year_studies.term',
                              'year_studies.year',
                              'student_groups.class',
                              'courses.courses_id',
                              'classrooms.classroom_type',
                              'personels.first_name',
                              'personels.last_name',
                              'coach'
                      )
                    ->where('personels.id', '=', $id)
                    ->where('year_studies_id', '=', $ids)
                    ->get();

      $scheduel  = $scheduel->sortBy('times_id')->toArray();

      for ($i=0; $i < count($scheduel); $i++) 
      {
        $data       [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->class;
        $datacourse [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_id;
        $day        [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->days_id;
        $time       [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->times_id;
        $classroom  [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom_type;
        $coach      [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->coach;
      }

      $count_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                           ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                           ->where('personels.id', '=', $id)
                           ->where('year_studies_id', '=', $ids)
                           ->count();

      $subject_matters_name = DB::table('subject_matters')
                                ->where('id', '=', Auth::user()->subject_mat_id)
                                ->select( 
                                          'subject_matter_name'
                                  )
                                ->first();

      $year_name = DB::table('year_studies')
                     ->where('id', '=', $ids)
                     ->select(
                                'year',
                                'term'
                       )
                     ->orderBY('year')
                     ->orderBY('term')
                     ->first();

      $pdf = PDF::loadView('sub_matter.loadPdf_Scheduel',compact('day','time','scheduel','data','datacourse','classroom','personels','count_scheduels','subject_matters_name','coach','year_name'));
      
      $pdf->setPaper('A4', 'portrait');

      return $pdf->stream();
      
    }

    public function exportExcelScheduel($id){

      $personels = DB::table('personels')
                     ->where('personels.id', '=', $id)
                     ->first();

      $scheduel = DB::table('scheduels')
                    ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                    ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                    ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                    ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                    ->leftJoin('year_studies', 'year_studies.id', '=', 'instructors.year_studies_id')  
                    ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                                                                                                  
                    ->orderBy('days_id')
                    ->select(
                              'scheduels.id as scheduels_id',
                              'scheduels.times_id',
                              'scheduels.days_id',
                              'instructors.id as instructors_id',
                              'year_studies.term',
                              'year_studies.year',
                              'student_groups.class',
                              'courses.courses_id',
                              'classrooms.classroom_type',
                              'personels.first_name',
                              'personels.last_name'
                      )
                    ->where('personels.id', '=', $id)
                    ->get();

      $scheduel  = $scheduel->sortBy('times_id')->toArray();

      for ($i=0; $i < count($scheduel); $i++) 
      {
        $data       [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->class;
        $datacourse [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_id;
        $day        [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->days_id;
        $time       [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->times_id;
        $classroom  [$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom_type;
      }

      $count_scheduels = DB::table('scheduels')
                           ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                           ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                           ->where('personels.id', '=', $id)
                           ->count();

      Excel::create('ตารางสอน', function ($excel) use ($personels,$scheduel,$day,$time,$data,$datacourse,$classroom,$count_scheduels) {
             $excel->sheet('ตารางสอน', function ($sheet) use ($personels,$scheduel,$day,$time,$data,$datacourse,$classroom,$count_scheduels) {
             $sheet->loadView('sub_matter.exportExcel_scheduel')
                   ->setSize('A15', 20, 30)             
                   ->setSize('B15', 20, 50)
                   ->setSize('C15', 20, 50)
                   ->setSize('D15', 20, 50)
                   ->setSize('E15', 20, 50)
                   ->setSize('F15', 20, 50)
                   ->setSize('G15', 20, 50)
                   ->setSize('H15', 20, 50)
                   ->setSize('I15', 20, 50)
                   ->setSize('A16', 20, 30)             
                   ->setSize('B16', 20, 50)
                   ->setSize('C16', 20, 50)
                   ->setSize('D16', 20, 50)
                   ->setSize('E16', 20, 50)
                   ->setSize('F16', 20, 50)
                   ->setSize('G16', 20, 50)
                   ->setSize('H16', 20, 50)
                   ->setSize('I16', 20, 50)
                   ->setSize('A17', 20, 30)             
                   ->setSize('B17', 20, 50)
                   ->setSize('C17', 20, 50)
                   ->setSize('D17', 20, 50)
                   ->setSize('E17', 20, 50)
                   ->setSize('F17', 20, 50)
                   ->setSize('G17', 20, 50)
                   ->setSize('H17', 20, 50)
                   ->setSize('I17', 20, 50)
                   ->setSize('A18', 20, 30)                                
                   ->setSize('B18', 20, 50)
                   ->setSize('C18', 20, 50)
                   ->setSize('D18', 20, 50)
                   ->setSize('E18', 20, 50)
                   ->setSize('F18', 20, 50)
                   ->setSize('G18', 20, 50)
                   ->setSize('H18', 20, 50)
                   ->setSize('I18', 20, 50)
                   ->setSize('A19', 20, 30)                                
                   ->setSize('B19', 20, 50)
                   ->setSize('C19', 20, 50)
                   ->setSize('D19', 20, 50)
                   ->setSize('E19', 20, 50)
                   ->setSize('F19', 20, 50)
                   ->setSize('G19', 20, 50)
                   ->setSize('H19', 20, 50)
                   ->setSize('I19', 20, 50)
                   ->withPersonels($personels)
                   ->withScheduel($scheduel)
                   ->withDay($day)
                   ->withTime($time)
                   ->withData($data)
                   ->withDatacourse($datacourse)
                   ->withClassroom($classroom)
                   ->withCount_scheduels($count_scheduels); 
             });
      })->download('xls');

    }

    public function getViewScheduel(){

      $year_studies = DB::table('year_studies')
                        ->orderBy('year')
                        ->orderBy('term')
                        ->get();

      return view('sub_matter.view_Scheduel',compact('year_studies'));
    
    }

    public function getCoursesName(Request $request){
      
      $courses_name = DB::table('courses')
                        ->where('id', '=', $request->id)
                        ->first();
      
      return response()->json($courses_name);

    }

    public function getStudentGroups(Request $request){

      $student_group = DB::table('student_groups')
                         ->where('id', '=', $request->id)
                         ->first();

      $check_student_groups = substr($student_group->class,0,1);

      $courses_class_type = DB::table('courses')
                              ->where('study_group', '=', $check_student_groups)
                              ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                              ->select(
                                        'courses.courses_id',
                                        'courses.id'
                                )
                              ->orderBy('courses_id')
                              ->get();

      return response()->json($courses_class_type);

    }

}




