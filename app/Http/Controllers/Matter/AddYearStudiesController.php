<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class AddYearStudiesController extends Controller
{
  public function submit(Request $request){

    $count_year_studies = DB::table('year_studies')
                            ->where('year', '=', $request->year)
                            ->where('term', '=', $request->term)
                            ->count();

    $check_year_studies = DB::table('year_studies')
                            ->where('year', '=', $request->year)
                            ->where('term', '=', $request->term)
                            ->select(
                                      'year',
                                      'term'
                              )
                            ->first();

    if($count_year_studies <= 0)
    {
      $add_year_studies = DB::table('year_studies')
                            ->insert([
                                        'year'        =>  $request->year,
                                        'term'        =>  $request->term,
                                        'created_at'  =>  date('Y-m-d H:i:s'),
                                        'updated_at'  =>  date('Y-m-d H:i:s')
                            ]);

      $notification = array(
                'message' => 'เพิ่มปีการศึกษาเรียบร้อยเเล้ว !!!',
                'alert-type' => 'success',
           );
      
      return redirect('/sub_mat/add_instructor')->with($notification);
    }
    else
    {
      $notification = array(
                'message' => 'มีปีการศึกษา '.$check_year_studies->year.' เทอม '.$check_year_studies->term.' เเล้วกรุณากรอกใหม่ !!!',
                'alert-type' => 'error',
           );
      
      return redirect('/sub_mat/add_instructor')->with($notification);
    
    }
  
  }
}
