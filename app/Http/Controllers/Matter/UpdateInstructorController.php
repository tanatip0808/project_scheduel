<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use URL;
use Illuminate\Support\Facades\Redirect;

class UpdateInstructorController extends Controller
{
  public function getEditInstructor(Request $request){
    
    if($request->ajax())
    {
      $edit_instructors = DB::table('instructors')
                            ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                            ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                            ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                            ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                            
                            ->select(
                                      'instructors.id',
                                      'courses.id as ids',                                      
                                      'courses.courses_id',
                                      'courses.courses_name',
                                      'courses.times_study',
                                      'courses.study_group',
                                      'personels_id',
                                      'instructors.classrooms_id',
                                      'personels.first_name',
                                      'personels.last_name',
                                      'instructors.student_groups_id',
                                      'student_groups.class',
                                      'classrooms.classroom_type',
                                      'year_studies_id',
                                      'instructors.coach'
                              )
                            ->where('instructors.id','=',$request->id)
                            ->get();
                            
      return Response($edit_instructors);   
    }
    
  }

  public function getUpdateInstructor(Request $request,$id){

    $check_scheduels = DB::table('scheduels')
                         ->where('instructors_id', '=', $request->id)
                         ->count();
    
    $year_studies = $id;

    $check_course = DB::table('courses')
                      ->where('courses.id', '=', $request->ids)
                      ->select(
                                'courses_id',
                                'courses_name'
                        )
                      ->first();

    $check_year_id = DB::table('year_studies')
                       ->where('id', '=', $id)
                       ->select(
                                 'year',
                                 'term'
                         )
                       ->first();

    if($check_scheduels <= 0)    
    {                      
      // $update_courses = DB::table('courses')
      //                     ->where('courses.id','=', $request->ids)
      //                     ->update([
      //                                 'courses_name'        =>  $request->courses_name,
      //                                 'updated_at'          =>  date('Y-m-d H:i:s')
      //                     ]);

      $update_instructors = DB::table('instructors')
                              ->where('instructors.id','=', $request->id)
                              ->update([
                                          'personels_id'        =>  $request->personels_id,
                                          'coach'               =>  $request->coach,
                                          // 'student_groups_id'   =>  $request->student_groups_id,
                                          'classrooms_id'       =>  $request->classrooms_id,
                                          'updated_at'          =>  date('Y-m-d H:i:s')
                              ]);
      $notification = array(
                'message' => 'เเก้ไขรายวิชาเรียบร้อยเเล้ว !!!',
                'alert-type' => 'success',
          );

      return redirect('/sub_mat/view_instructor'.$year_studies)->with($notification);
    }
    else
    {
      $notification = array(
                'message' => 'วิชา '.$check_course->courses_id.' ปีการศึกษา '.$check_year_id->year.' / '.$check_year_id->term.' ได้จัดตารางสอนไว้เเล้วไม่สามารถเเก้ไขรายวิชาได้ !!!',
                'alert-type' => 'warning',
          );

      return redirect('/sub_mat/view_instructor'.$year_studies)->with($notification);
    }

  }

  public function getUpdateInstructors(Request $request,$id){

    $check_scheduels = DB::table('scheduels')
                         ->where('instructors_id', '=', $request->id)
                         ->count();

    $year_studies = $id;
    
    $check_course = DB::table('courses')
                      ->where('courses.id', '=', $request->ids)
                      ->select(
                                'courses_id',
                                'courses_name'
                        )
                      ->first();

    $check_year_id = DB::table('year_studies')
                       ->where('id', '=', $id)
                       ->select(
                                 'year',
                                 'term'
                         )
                       ->first();

    if($check_scheduels <= 0)    
    { 
      // $update_courses = DB::table('courses')
      //                     ->where('courses.id','=', $request->ids)
      //                     ->update([
      //                                 'courses_name'        =>  $request->courses_name,
      //                                 'updated_at'          =>  date('Y-m-d H:i:s')
      //                     ]);

      $update_instructors = DB::table('instructors')
                              ->where('instructors.id','=', $request->id)
                              ->update([
                                          'personels_id'        =>  $request->personels_id,
                                          'coach'               =>  $request->coach,
                                          // 'student_groups_id'   =>  $request->student_groups_id,
                                          'classrooms_id'       =>  $request->classrooms_id,
                                          'updated_at'          =>  date('Y-m-d H:i:s')
                              ]);
                                                                 
      $personel_pdf = $request->personels_id;
      
      $notification = array(
                'message' => 'เเก้ไขรายวิชาเรียบร้อยเเล้ว !!!',
                'alert-type' => 'success',
          );
          
      return redirect(URL::to('/sub_mat/update_instructor_personels/'.$personel_pdf,$year_studies))->with($notification);
    }
    else
    {    
      $personel_pdf = $request->personels_id;

      $notification = array(
                'message' => 'วิชา '.$check_course->courses_id.' ปีการศึกษา '.$check_year_id->year.' / '.$check_year_id->term.' ได้จัดตารางสอนไว้เเล้วไม่สามารถเเก้ไขรายวิชาได้ !!!',
                'alert-type' => 'warning',
          );
        
      return Redirect::back()->with($notification);
    }

  }
}