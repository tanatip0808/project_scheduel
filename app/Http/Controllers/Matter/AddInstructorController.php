<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class AddInstructorController extends Controller
{
  public function submit(Request $request){

    $count_courses_id  = DB::table('instructors')
                           ->where('courses_id', '=', $request->courses)
                           ->where('year_studies_id', '=', $request->year_studies_id)
                           ->count();

    $check_year_studies  = DB::table('instructors')
                           ->leftJoin('year_studies' , 'year_studies.id', '=' , 'instructors.year_studies_id')
                           ->where('year_studies_id', '=', $request->year_studies_id)
                           ->select(  
                                      'year_studies.year',
                                      'year_studies.term'
                             )
                           ->first();

    $check_courses  = DB::table('instructors')
                        ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')
                        ->where('instructors.courses_id', '=', $request->courses)
                        ->select(  
                                   'courses.courses_id',
                                   'courses.courses_name'
                          )
                        ->first();
    
    if($count_courses_id <= 0)
    {
      $add_instructors = DB::table('instructors')
                           ->insert([
                                       'year_studies_id'   =>  $request->year_studies_id,
                                       'student_groups_id' =>  $request->student_groups_id,
                                       'classrooms_id'     =>  $request->classrooms_id,
                                       'courses_id'        =>  $request->courses,
                                       'personels_id'      =>  $request->personels_id,
                                       'coach'             =>  $request->coach,
                                       'color'             => '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT),
                                       'created_at'        =>  date('Y-m-d H:i:s'),
                                       'updated_at'        =>  date('Y-m-d H:i:s')
                           ]);

      $notification = array(
                'message' => 'เพิ่มรายวิชาที่เปิดสอนเรียบร้อยเเล้ว !!!',
                'alert-type' => 'success',
          );

      return redirect('/sub_mat/add_instructor')->with($notification);
    }
    else
    {
      $notification = array(
                'message' => 'ปีการศึกษา '.$check_year_studies->year.' เทอม '.$check_year_studies->term.' มีวิชา '.$check_courses->courses_id.' '.$check_courses->courses_name.' เปิดสอนอยู่เเล้ว !!!',
                'alert-type' => 'error',
          );

      return redirect('/sub_mat/add_instructor')->with($notification);
    }

  }
}
