<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class AddStudyCourseController extends Controller
{
  public function submit(Request $request){

    $count_study_courses  = DB::table('study_courses')
                              ->where('study_course_name', '=', $request->study_course_name)
                              ->count();

    $check_study_courses  = DB::table('study_courses')
                              ->where('study_course_name', '=', $request->study_course_name)
                              ->select(
                                        'study_course_name'
                                )
                              ->first();

    if($count_study_courses <= 0)
    {
      $add_study_courses = DB::table('study_courses')
                             ->insert([
                                        'study_course_name'   =>  $request->study_course_name,
                                        'created_at'          =>  date('Y-m-d H:i:s'),
                                        'updated_at'          =>  date('Y-m-d H:i:s')
                             ]);

      $notification = array(
                'message' => 'เพิ่มหลักสูตรเรียบร้อยแล้ว !!!',
                'alert-type' => 'success',
          );

      return redirect('/sub_mat/add_instructor')->with($notification);
    }
    else
    {
      $notification = array(
                'message' => 'มีหลักสูตร '.$check_study_courses->study_course_name.' เเล้วกรุณากรอกใหม่ !!!',
                'alert-type' => 'error',
          );
      
      return redirect('/sub_mat/add_instructor')->with($notification); 
    }
  
  }
}
