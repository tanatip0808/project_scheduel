<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;

class ViewScheduelController extends Controller
{
  public function search(Request $request){
      
    $show_year_studies = DB::table('year_studies')
                           ->where('year_studies.id', '=', $request->id)
                           ->first();

    $year_studies = DB::table('year_studies')
                      ->orderBy('year')
                      ->orderBy('term')
                      ->get();

    $count_scheduels = DB::table('scheduels')
                         ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                         ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                         ->select(
                                    'instructors.year_studies_id',
                                    'personels.id',
                                    'personels.first_name',
                                    'personels.last_name'
                           )
                         ->where('year_studies_id', '=', $request->id)
                         ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                         ->groupBy('first_name')
                         ->get();

    $count_scheduels_year_studies = DB::table('scheduels')
                                      ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                                      ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                                      ->where('year_studies_id', '=', $request->id)
                                      ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                                      ->count();

    $subject_matters_name = DB::table('subject_matters')
                              ->where('id', '=', Auth::user()->subject_mat_id)
                              ->select(
                                        'subject_matter_name'
                                )
                              ->first();      

    return view('sub_matter.view_Scheduel_year_studies',compact('count_scheduels','count_scheduels_year_studies','show_year_studies','year_studies','subject_matters_name'));
   
  }
}