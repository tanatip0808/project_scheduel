<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class AddCoursesController extends Controller
{
  public function submit(Request $request){

    $count_courses_id  = DB::table('courses')
                           ->where('courses_id', '=', $request->courses_id)
                           ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                           ->count();

    $check_courses_id  = DB::table('courses')
                           ->where('courses_id', '=', $request->courses_id)
                           ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                           ->select(  
                                      'courses_id'
                             )
                           ->first();
    
    if($count_courses_id <= 0)
    {
      $add_courses = DB::table('courses')
                           ->insert([
                                       'courses_id'         =>  $request->courses_id,
                                       'courses_name'       =>  $request->courses_name,
                                       'subject_matters_id' =>  Auth::user()->subject_mat_id,
                                       'created_at'         =>  date('Y-m-d H:i:s'),
                                       'updated_at'         =>  date('Y-m-d H:i:s')
                           ]);

      $notification = array(
                'message' => 'เพิ่มรายวิชาเรียบร้อยเเล้ว !!!',
                'alert-type' => 'success',
          );

      return redirect('/sub_mat/add_instructor')->with($notification);
    }
    else
    {
      $notification = array(
                'message' => 'รหัสวิชา '.$check_courses_id->courses_id.' ซ้ำกรุณากรอกใหม่ !!!',
                'alert-type' => 'error',
          );

      return redirect('/sub_mat/add_instructor')->with($notification);
    }

  }
}
