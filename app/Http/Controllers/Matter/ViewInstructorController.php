<?php

namespace App\Http\Controllers\Matter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;

class ViewInstructorController extends Controller
{
  public function search(Request $request,$id){
    
    $count_instructors = DB::table('instructors')
                           ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id') 
                           ->where('year_studies_id', '=', $id)
                           ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)                                    
                           ->count();

    $count_instructors_personels = DB::table('instructors')
                                     ->where('personels_id', '=', $request->id)
                                     ->where('year_studies_id', '=', $id)
                                     ->count();

    $personelss = DB::table('instructors')
                    ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                    ->select(
                              'personels.id',
                              'personels.first_name',
                              'personels.last_name'
                      )
                    ->where('year_studies_id', '=', $id) 
                    ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                    ->groupBy('first_name')                                  
                    ->get(); 

    $personels = DB::table('personels')
                   ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                   ->orderBy('first_name') 
                   ->get();
                    
    $student_groups = DB::table('student_groups')
                        ->orderBy('class')
                        ->get();

    $classrooms = DB::table('classrooms')
                    ->orderBy('classroom_type')
                    ->get();

    $year_name = DB::table('year_studies')
                   ->where('id', '=', $id)
                   ->select(
                              'year',
                              'term'
                     )
                   ->orderBY('year')
                   ->orderBY('term')
                   ->first();

    $view_instructors = DB::table('instructors')
                          ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                          ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                          ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                          ->leftJoin('year_studies' , 'year_studies.id', '=' , 'instructors.year_studies_id')
                          ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                            
                          ->select(
                                    'instructors.id',
                                    'courses.courses_id',
                                    'courses.courses_name',
                                    'personels.first_name',
                                    'personels.last_name',
                                    'courses.times_study',
                                    'student_groups.class',
                                    'classrooms.classroom_type',
                                    'year_studies.term',
                                    'year_studies.year',
                                    'instructors.coach'
                            )
                          ->where('personels_id', '=', $request->id)
                          ->where('year_studies_id', '=', $id)  
                          ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)                                 
                          ->orderBy('courses_id', 'asc')
                          ->Paginate(12);

    $personel_pdf = $request->id;

    $show_personels = DB::table('personels')
                        ->where('personels.id', '=', $request->id)
                        ->where('subject_matters_id', '=', Auth::user()->subject_mat_id) 
                        ->first();

    $year_studies = $id;
                            
    return view('sub_matter.view_Instructor_personels',compact('personels','personelss','view_instructors','count_instructors','count_instructors_personels','personel_pdf','show_personels','student_groups','classrooms','year_studies','year_name'));

  }

  public function UpdateInstructorPersonels($id,$ids){
    
    $count_instructors = DB::table('instructors')
                             ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id') 
                             ->where('year_studies_id', '=', $ids)
                             ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)                                    
                             ->count();    
    $count_instructors_personels = DB::table('instructors')
                                     ->where('personels_id', '=', $id)
                                     ->where('year_studies_id', '=', $ids)     
                                     ->count();    
                                     
    $personelss = DB::table('instructors')
                    ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                    ->select(
                              'personels.id',
                              'personels.first_name',
                              'personels.last_name'
                      )
                    ->where('year_studies_id', '=', $ids) 
                    ->groupBy('first_name')                                  
                    ->get(); 
                    
    $personels = DB::table('personels')
                   ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                   ->orderBy('first_name')
                   ->get();
                    
    $student_groups = DB::table('student_groups')
                        ->orderBy('class')
                        ->get(); 

    $classrooms = DB::table('classrooms')
                    ->orderBy('classroom_type')
                    ->get();

    $year_name = DB::table('year_studies')
                   ->where('id', '=', $ids)
                   ->select(
                              'year',
                              'term'
                     )
                   ->orderBY('year')
                   ->orderBY('term')
                   ->first();
                    
    $view_instructors = DB::table('instructors')
                          ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
                          ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                          ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                          ->leftJoin('year_studies' , 'year_studies.id', '=' , 'instructors.year_studies_id')
                          ->leftJoin('courses' , 'courses.id', '=' , 'instructors.courses_id')                                                                                      
                          ->select(
                                    'instructors.id',
                                    'courses.courses_id',
                                    'courses.courses_name',
                                    'personels.first_name',
                                    'personels.last_name',
                                    'courses.times_study',
                                    'student_groups.class',
                                    'classrooms.classroom_type',
                                    'year_studies.term',
                                    'year_studies.year',
                                    'instructors.coach'
                            )
                          ->where('personels_id', '=', $id)
                          ->where('year_studies_id', '=', $ids) 
                          ->where('personels.subject_matters_id', '=', Auth::user()->subject_mat_id)     
                          ->orderBy('courses_id', 'asc')
                          ->Paginate(12);
                          
    $year_studies = $ids;                        
                          
    $personel_pdf = $id;
                          
    $show_personels = DB::table('personels')
                        ->where('personels.id', '=', $id)
                        ->where('subject_matters_id', '=', Auth::user()->subject_mat_id)
                        ->first();
                          
    return view('sub_matter.view_Instructor_personels',compact('personels','personelss','view_instructors','count_instructors','count_instructors_personels','personel_pdf','show_personels','student_groups','classrooms','year_studies','year_name')); 
    
  }
}