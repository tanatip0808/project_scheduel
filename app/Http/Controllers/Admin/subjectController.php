<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\subject_matter;

class subjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $objs = subject_matter::all();
      $data['objs']=$objs;
      $data['url']= url('admin/subject/create');
      return view('admin.subject',compact('objs'),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['method']="post";
        $data['url']= url('admin/subject/');
        return view('admin.add_subject',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $objs = new subject_matter();
        $objs->subject_matter_name= $request['subject'];
        $objs->save();

        return redirect(url('admin/subject'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $objs = subject_matter::find($id);
        $data['url']= url('admin/subject/'.$id);
        $data['obj']=$objs;
        $data['method']="PUT";
        return view('admin/add_subject',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $objs = subject_matter::find($id);
        $objs->subject_matter_name=$request['subject'];
        $objs->save();
        return redirect(url('admin/subject'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $objs = subject_matter::find($id);
        $objs->delete();
        return redirect(url('admin/subject'));
    }
}
