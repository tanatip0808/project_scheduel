<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\instructor;
use App\scheduel;
use App\student_group;
use DB;
class scheduelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $stdlabel = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->where('student_groups.id', '=','')
      ->get();

      $year = DB::table('year_studies')
      ->select('year_studies.id','year_studies.year','year_studies.term')
      ->get();

      $stdgroup = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->get();
      $std_id='';
      $year_use_id='';
      $instructor_count='';
      $year_display='';
      $std_display = DB::table('student_groups')
      ->select('student_groups.class')
      ->first();
      // $instructor = DB::table('instructors')
      //           ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
      //           ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
      //           ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
      //           ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
      //           ->select('instructors.courses_name','instructors.id','personels.first_name'
      //           ,'personels.last_name','personels.id as tchid','student_groups.id as stdid',
      //           'classrooms.classroom_type as classroom','classrooms.id as classid','student_groups.class as class','instructors.times_study','instructors.color')
      //           ->where('instructors.student_groups_id', '=','')
      //           ->get();
      return view('admin.indexscheduel',compact('std',
      'stdlabel',
      'year',
      'instructor',
      'stdgroup',
      'std_id',
      'year_use_id',
      'instructor_count',
      'std_display'
    ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $year = DB::table('year_studies')
      ->select('year_studies.id','year_studies.year','year_studies.term')
      ->get();

      $stdgroup = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->get();

      $std_display = DB::table('student_groups')
      ->select('student_groups.id as stdid','student_groups.class')
      ->where('student_groups.id', '=',$request['std_group'])
      ->first();

      $year_display = DB::table('year_studies')
      ->select('year_studies.id as yearid','year_studies.year','year_studies.term')
      ->where('year_studies.id', '=',$request['year'])
      ->first();

      $std_id=$request['std_group'];
      $year_use_id=$request['year'];

  $instructor = DB::table('instructors')
            ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
            ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
            ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
            ->Join('courses','courses.id', '=', 'instructors.courses_id')
            ->select('courses.courses_name','instructors.id','personels.first_name','instructors.year_studies_id as yearid'
            ,'personels.last_name','personels.id as tchid','student_groups.id as stdid',
            'classrooms.classroom_type as classroom','classrooms.id as classid','student_groups.class as class','instructors.times_study','instructors.check_times_study','instructors.color','instructors.coach')
            ->where('instructors.student_groups_id', '=', $request['std_group'])
            ->where('instructors.year_studies_id', '=',$request['year'])
            ->get();

            $instructor_count = DB::table('instructors')
                      ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                      ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                      ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                      ->Join('courses','courses.id', '=', 'instructors.courses_id')
                      ->select('courses.courses_name','instructors.id','personels.first_name','instructors.year_studies_id as yearid'
                      ,'personels.last_name','personels.id as tchid','student_groups.id as stdid',
                      'classrooms.classroom_type as classroom','classrooms.id as classid','student_groups.class as class','instructors.times_study','instructors.check_times_study','instructors.color')
                      ->where('instructors.student_groups_id', '=', $request['std_group'])
                      ->where('instructors.year_studies_id', '=',$request['year'])
                      ->count();


            $p=0;

  $scheduel = DB::table('scheduels')
            ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
            ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
            ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
            ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
            ->Join('courses','courses.id', '=', 'instructors.courses_id')
            ->orderBy('days_id')
            ->select(
                      'scheduels.id as idsch',
                      'scheduels.times_id',
                      'scheduels.days_id',
                      'personels.first_name',
                      'scheduels.id as schid',
                      'personels.last_name',
                      'courses.courses_name',
                      'instructors.id as insid',
                      'classrooms.classroom_type as classroom',
                      'scheduels.days_id as day',
                      'scheduels.times_id as time',
                      'instructors.personels_id as tchid',
                      'instructors.year_studies_id as yearid',
                      'instructors.classrooms_id as classtypeid',
                      'instructors.color',
                      'instructors.coach',
                      'instructors.student_groups_id'
                    )
            ->where('student_groups.id', '=',$request['std_group'])
            ->where('instructors.year_studies_id', '=',$request['year'])
            ->get();

  $scheduel  = $scheduel->sortBy('times_id')->toArray();
  for ($i=0; $i < count($scheduel) ; $i++) {
      $idsch[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->idsch;
      $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
      $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
      $insid[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->insid;
      $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
      $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
      $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
      $tchid[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->tchid;
      $yearid[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->yearid;
      $class_type_id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classtypeid;
      $color[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->color;
      $coach[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->coach;
      $std_g_id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->student_groups_id;

  }
          return view('admin.scheduel',compact(
            'p',
            'instructor',
            'tchid',
            'stdid',
            'day',
            'time',
            'scheduel',
            'coach',
            'data',
            'insid',
            'datacourse',
            'classroom',
            'idsch',
            'year',
            'stdgroup',
            'yearid',
            'class_type_id',
            'std_display',
            'std_id',
            'year_use_id',
            'color',
            'instructor_count',
            'year_display',
            'std_g_id'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $scheduel_count= DB::table('scheduels')//นับว่าในตารางเรียนมีรายวิชานวันนี้เวลานี้แล้วหรือไม่ี้แล้วหรือไม่
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->select('scheduels.times_id','scheduels.days_id','instructors.personels_id','instructors.student_groups_id')
                ->where('times_id', '=', $request['time'])
                ->where('days_id', '=', $request['day'] )
                ->where('instructors.student_groups_id', '=', $request['std'])
                ->where('year_studies_id','=',$request['year_studies'])
                ->count();
      $scheduel_add= DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->select('scheduels.times_id','scheduels.days_id')
                ->where('times_id', '=', $request['time'])
                ->where('days_id', '=', $request['day'] )
                ->where('instructors.student_groups_id','=',$request['std_g_id'])
                ->where('year_studies_id','=',$request['year_studies'])
                ->count();
      $scheduel_update= DB::table('scheduels')
              ->select('scheduels.id')
              ->where('id', '=', $request['idsch'])
              ->count();

      $teacher_count= DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->select('scheduels.times_id','scheduels.days_id','instructors.personels_id','instructors.student_groups_id')
                ->where('times_id', '=', $request['time'])
                ->where('days_id', '=', $request['day'] )
                ->where('personels_id', '=', $request['tcher'])
                ->where('year_studies_id','=',$request['year_studies'])
                ->count();

      $classroom_count= DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->select('scheduels.times_id','scheduels.days_id','instructors.personels_id','instructors.student_groups_id')
                ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                ->where('times_id', '=', $request['time'])
                ->where('days_id', '=', $request['day'] )
                ->where('classrooms_id', '=', $request['classroom'])
                ->where('year_studies_id','=',$request['year_studies'])
                ->where('classroom_type', '!=', 'ปกติ')
                ->count();


                if ( $teacher_count>=1) {
                    return response()->json([
                    'code' => 'nots',
                    'status' => 'อาจารย์ท่านนี้ไม่ว่าง'
                    ]);
                }
              if ($scheduel_add >=1 ) {
                    return response()->json([
                    'code' => 'error',
                    'status' => 'คาบเรียนไม่ว่าง'
                    ]);
                }
                if ($classroom_count>=1) {
                      return response()->json([
                      'code' => 'classroom_error',
                      'status' => 'ห้องไม่ว่าง'
                      ]);
                  }
      elseif($scheduel_count<=0 and $teacher_count<=0 and $request['std'] != 0){
        $objs = new scheduel();
        $objs->status =$request['status'];
        $objs->instructors_id =$request['id'];
        $objs->times_id =$request['time'];
        $objs->days_id = $request['day'];
        $objs->save();

        $objs_instructer= DB::table('instructors')
                    ->where('id', '=', $request['id'])
                    ->update(['check_times_study' => $request['times_study']+1]);

        return response()->json([
      'code' => 1,
      'status' => 'บันทึกเรียบร้อย'
      ]);
  }
     if ($scheduel_update==1  and $scheduel_add <= 0) {
        $affected = DB::table('scheduels')
          ->select('scheduels.times_id','scheduels.days_id','scheduels.id')
          ->where('id', '=', $request['idsch'])
          ->update(array('times_id' => $request['time'],'days_id' => $request['day']));
          return response()->json([
          'code' => 'updated',
          'status' => 'อัพเดทเรียบร้อย'
          ]);
      }
     else {
       return response()->json([
       'code' => 'error',
       'status' => 'คาบเรียนไม่ว่าง'
       ]);
     }
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

      $scheduel = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                ->Join('courses','courses.id', '=', 'instructors.courses_id')
                ->orderBy('days_id')
                ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','student_groups.class as student_group')
                ->where('instructors.personels_id', '=',$request['id'])
                ->get();

      $check_stdgroup = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                ->Join('courses','courses.id', '=', 'instructors.courses_id')
                ->orderBy('days_id')
                ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','student_groups.class as student_group')
                ->where('instructors.student_groups_id', '=',$request['std'])
                ->orwhere('instructors.student_groups_id', '=',$request['stdadd'])

                ->get();
                $check_stdgroup  = $check_stdgroup->sortBy('times_id')->toArray();

      $classroom_count = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                ->Join('courses','courses.id', '=', 'instructors.courses_id')
                ->orderBy('days_id')
                ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','student_groups.class as student_group')
                ->where('instructors.classrooms_id', '=',$request['classroom'])
                ->get();
                $classroom_count  = $classroom_count->sortBy('times_id')->toArray();

                  $std[]='';
                  $classroomclass[]='';
                  $stdclass[]='';
                  $stdgroup_lern[]='';
                  $datastd[]='';


                $scheduel  = $scheduel->sortBy('times_id')->toArray();
                for ($i=0; $i < count($classroom_count) ; $i++) {
                    $dataclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->first_name;
                    $datacourseclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->courses_name;
                    $idclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->id;
                    $dayclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->day;
                    $timeclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->time;
                    $classroomclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->classroom;
                    $stdclass[$classroom_count[$i]->days_id.$classroom_count[$i]->times_id]=$classroom_count[$i]->student_group;

                }

                for ($i=0; $i < count($scheduel) ; $i++) {
                    $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
                    $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
                    $id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->id;
                    $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
                    $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
                    $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
                    $std[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->student_group;

                }

                for ($i=0; $i < count($check_stdgroup) ; $i++) {
                    $datastd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->first_name;
                    $datacoursestd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->courses_name;
                    $idstd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->id;
                    $daystd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->day;
                    $timestd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->time;
                    $classroomstd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->classroom;
                    $stdstd[$check_stdgroup[$i]->days_id.$check_stdgroup[$i]->times_id]=$check_stdgroup[$i]->student_group;

                }
              return response()->json(array('data' => $std,'class' => $classroomclass,'classstd'=>$stdclass,'std_group'=>$datastd));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $scheduel_count = DB::table('scheduels')
                 ->where('scheduels.id', '=', $request['id'])
                 ->delete();
      $instructor =DB::table('instructors')
                ->where('instructors.id','=', $request['insid'])
                ->update(['check_times_study' => DB::raw('check_times_study-1')]);
                  return redirect()->back();
        //
    }
    public function resetscheduel(Request $request)
    {

      $scheduel = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->where('instructors.student_groups_id', '=',$request['stdid'])
                ->where('instructors.year_studies_id', '=',$request['yearid'])
                ->delete();
    $instructor_update = DB::table('instructors')
              ->where('instructors.student_groups_id', '=',$request['stdid'])
              ->where('instructors.year_studies_id', '=',$request['yearid'])
              ->update(['check_times_study' => 0]);

                  return redirect()->back();
    }
    public function resettchscheduel(Request $request)
    {

      $scheduel = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->where('instructors.personels_id', '=',$request['teacher_id'])
                ->where('instructors.year_studies_id', '=',$request['yearid'])
                ->delete();
    $instructor_update = DB::table('instructors')
              ->where('instructors.personels_id', '=',$request['teacher_id'])
              ->where('instructors.year_studies_id', '=',$request['yearid'])
              ->update(['check_times_study' => 0]);

                  return redirect()->back();
    }
}
