<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\course;
use DB;
class coursecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['url']= url('admin/courses/create');
        $subject = DB::table('subject_matters')
        ->select('subject_matters.id as smid','subject_matters.subject_matter_name as sname')
        ->get();
        $subject_display = DB::table('subject_matters')
       ->select('subject_matters.id as smid','subject_matters.subject_matter_name as sname')
        ->first();
        $objs = DB::table('courses')
        ->select('courses.id as cid','courses.courses_id','courses.courses_name')
        ->paginate(7);
        return view('admin.courses',compact('subject','objs','subject_display'),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['method']="post";
        $data['url']= url('admin/courses/');
        $subject = DB::table('subject_matters')
        ->select('subject_matters.id','subject_matters.subject_matter_name as sname')
        ->get();
        $objs = DB::table('courses')
        ->select('courses.id as cid','courses.courses_id','courses.courses_name')
        ->paginate(7);
        return view('admin.add_courses',compact('subject','objs'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $courses_count= DB::table('courses')
                  ->where('courses_id', '=', $request['courses_id'])
                  ->count();
        if ($courses_count<1) {
          DB::table('courses')->insert(
                          [
                                                'courses_id'         => $request['courses_id'],
                                                'courses_name'       => $request['courses_name'],
                                                'subject_matters_id' => $request['subject'],
                                                'created_at'         =>  date('Y-m-d H:i:s'),
                                                'updated_at'         =>  date('Y-m-d H:i:s')
                          ]);
            return redirect(url('admin/courses'))->with('success','เพิ่มสำเร็จ!');
        }
        else {
            return redirect(url('admin/courses'))->with('error','รายวิชาซ้ำ!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $data['method']="post";
        $data['url']= url('admin/courses/create');
        $subject = DB::table('subject_matters')
        ->select('subject_matters.id as smid','subject_matters.subject_matter_name as sname')
        ->get();
        $subject_display = DB::table('subject_matters')
       ->select('subject_matters.id as smid','subject_matters.subject_matter_name as sname')
       ->where('subject_matters.id', '=',$request['subject'])
        ->first();
        $objs = DB::table('courses')
        ->select('courses.id as cid','courses.courses_id','courses.courses_name')
        ->where('subject_matters_id', '=',$request['subject'])
        ->paginate(7);
        return view('admin.courses',compact('subject','objs','subject_display'),$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
      $objs = course::find($id);
      $data['url']= url('admin/courses/'.$id);
      $data['obj']=$objs;
      $subject = DB::table('subject_matters')
      ->select('subject_matters.id','subject_matters.subject_matter_name as sname')
      ->get();
      $data['method']="PUT";
        return view('admin.add_courses',compact('subject','objs'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $objs = course::find($id);
        $objs->courses_id = $request['courses_id'];
        $objs->courses_name = $request['courses_name'];
        $objs->subject_matters_id = $request['subject'];
        $objs->save();
        return redirect(url('admin/courses'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
          $objs = course::find($id)->delete();
            return redirect(url('admin/courses'));
    }
}
