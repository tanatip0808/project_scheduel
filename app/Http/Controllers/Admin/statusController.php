<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\status;

class statusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objs = status::all();
        $data['objs']=$objs;
        $data['url']= url('admin/status/create');
        return view('admin.status',compact('objs'),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['method']="post";
        $data['url']= url('admin/status/');
        return view('admin.add_status',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $objs = new status();
        $objs->status_detail = $request['status'];
        $objs->save();

        return redirect(url('admin/status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $objs = status::find($id);
        $data['url']= url('admin/status/'.$id);
        $data['obj']=$objs;
        $data['method']="PUT";
        return view('admin/add_status',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $objs = status::find($id);
        $objs->status_detail=$request['status'];
        $objs->save();
        return redirect(url('admin/status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $objs = status::find($id);
        $objs->delete();
        return redirect(url('admin/status'));
    }
}
