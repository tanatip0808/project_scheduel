<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\instructor;
class instructorcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $instructor = DB::table('instructors')
        ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
        ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
        ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
        ->leftJoin('study_courses', 'study_courses.id', '=', 'instructors.study_courses_id')
        ->leftJoin('year_studies', 'year_studies.id', '=', 'instructors.year_studies_id')
        ->leftJoin('courses','courses.id', '=', 'instructors.courses_id')
        ->select(
        'courses.courses_id',      
        'courses.courses_name',
        'instructors.credit',
        'instructors.times_study',
        'instructors.scheduel_times',
        'personels.first_name',
        'classrooms.classroom_type',
        'student_groups.class',
        'study_courses.study_course_name',
        'year_studies.year')
        ->get();
            $subject = DB::table('subject_matters')
            ->select('subject_matters.id','subject_matters.subject_matter_name')
            ->get();
            $course = DB::table('courses')
            ->select('courses.courses_name')
          ->distinct()
          ->get();
        return view('admin.instructor',compact('instructor','course','subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $instructor = DB::table('instructors')
      ->leftJoin('personels', 'personels.id', '=', 'instructors.personels_id')
      ->leftJoin('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
      ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
      ->leftJoin('study_courses', 'study_courses.id', '=', 'instructors.study_courses_id')
      ->leftJoin('year_studies', 'year_studies.id', '=', 'instructors.year_studies_id')
      ->leftJoin('courses','courses.id', '=', 'instructors.courses_id')
      ->select('instructors.courses_id','courses.courses_name','instructors.credit',
      'instructors.times_study','instructors.scheduel_times','personels.first_name','classrooms.classroom_type',
      'student_groups.class','study_courses.study_course_name','year_studies.year','personels.subject_matters_id')
      ->where('personels.subject_matters_id', '=',$request['course'])
      ->get();
      $subject = DB::table('subject_matters')
      ->select('subject_matters.id','subject_matters.subject_matter_name')
      ->get();
      return view('admin.instructor',compact('instructor','course','subject'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
