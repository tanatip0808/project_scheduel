<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class teachcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $teacherpdf='';
      $yearforpdf='';
      $personellabel = DB::table('personels')
      ->select('personels.id','personels.first_name','personels.last_name')
      ->where('personels.id', '=','')
      ->get();
      $personel = DB::table('personels')
      ->select('personels.id','personels.first_name','personels.last_name')
      ->get();
      $year = DB::table('year_studies')
      ->select('year_studies.id','year_studies.year','year_studies.term')
      ->get();
      $std_display = DB::table('student_groups')
      ->select('student_groups.class')
      ->first();
      return view('admin.indexshowtch',compact('personel','personellabel','teacherpdf','year','yearforpdf','std_display'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $personel = DB::table('personels')
      ->select('personels.id','personels.first_name','personels.last_name')
      ->get();

      $teacherpdf = DB::table('personels')
      ->select('personels.id')
      ->where('personels.id', '=',$request['teacher'])
      ->first();

      $personellabel = DB::table('personels')
      ->select('personels.id','personels.first_name','personels.last_name')
      ->where('personels.id', '=',$request['teacher'])

      ->get();

      $year = DB::table('year_studies')
      ->select('year_studies.id','year_studies.year','year_studies.term')
      ->get();

      $yearforpdf = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                ->select('instructors.year_studies_id')
                 ->where('instructors.personels_id', '=',$request['teacher'])
                 ->where('instructors.year_studies_id', '=',$request['year'])
                ->first();
      $amount_course = DB::table('scheduels')
      ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
      ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
      ->where('instructors.personels_id', '=',$request['teacher'])
      ->where('instructors.year_studies_id', '=',$request['year'])
      ->count();

      $scheduel = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                ->leftJoin('courses', 'courses.id', '=', 'instructors.courses_id')
                ->orderBy('days_id')
                ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','student_groups.class as stdgroup')
                 ->where('instructors.personels_id', '=',$request['teacher'])
                 ->where('instructors.year_studies_id', '=',$request['year'])
                ->get();
      $scheduel  = $scheduel->sortBy('times_id')->toArray();
      for ($i=0; $i < count($scheduel) ; $i++) {
          $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
          $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
          $id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->id;
          $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
          $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
          $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
          $stdgroup[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->stdgroup;
      }
        return view('admin.scheduelteacher',compact('instructor','day','time','scheduel','arr','data','id','datacourse','classroom','personel','personellabel','teacherpdf','year','yearforpdf','stdgroup','amount_course'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
