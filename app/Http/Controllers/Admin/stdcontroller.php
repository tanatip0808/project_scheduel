<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class stdcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $stdlabel = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->where('student_groups.id', '=','')
      ->get();

      $year = DB::table('year_studies')
      ->select('year_studies.id','year_studies.year','year_studies.term')
      ->get();

      $stdgroup = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->get();

      $std_id='';
      $instructor_count='';
      $stdforpdf='';
      $year_use_id='';
      $std_display = DB::table('student_groups')
      ->select('student_groups.class')
      ->first();
      return view('admin.indexshowstd',compact('stdlabel','year','stdgroup','std_id','instructor_count','stdforpdf','year_use_id','std_display'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $stdlabel = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->where('student_groups.id', '=',$request['std_group'])
      ->get();

      $year_use_id  = DB::table('instructors')
      ->select('instructors.year_studies_id')
      ->where('instructors.year_studies_id', '=',$request['year'])
      ->first();

      $stdgroup = DB::table('student_groups')
      ->select('student_groups.id','student_groups.class')
      ->get();
      $amount_course = DB::table('scheduels')
      ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
      ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
      ->where('instructors.student_groups_id', '=',$request['std_group'])
      ->where('instructors.year_studies_id', '=',$request['year'])
      ->count();

      $scheduel = DB::table('scheduels')
                ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                ->leftJoin('courses', 'courses.id', '=', 'instructors.courses_id')
                ->orderBy('days_id')
                ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','instructors.color')
                 ->where('instructors.student_groups_id', '=',$request['std_group'])
                ->where('instructors.year_studies_id', '=',$request['year'])
                ->get();
                $std_display = DB::table('student_groups')
                ->select('student_groups.id as stdid','student_groups.class')
                ->where('student_groups.id', '=',$request['std_group'])
                ->first();

                $year_display = DB::table('year_studies')
                ->select('year_studies.id as yearid','year_studies.year','year_studies.term')
                ->where('year_studies.id', '=',$request['year'])
                ->first();


                $year_use_id =$request['year'];
                $stdforpdf = $request['std_group'];
                $year = DB::table('year_studies')
                ->select('year_studies.id','year_studies.year','year_studies.term')
                ->get();

      $scheduel  = $scheduel->sortBy('times_id')->toArray();
      for ($i=0; $i < count($scheduel) ; $i++) {
          $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
          $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
          $id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->id;
          $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
          $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
          $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
          $color[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->color;

      }
        return view('admin.stdscheduel',compact('instructor','day','time','scheduel','arr','data','id','datacourse','classroom','stdgroup','stdlabel','stdforpdf','year','year_use_id','std_display','year_display','color','amount_course'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
