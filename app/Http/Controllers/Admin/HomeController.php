<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use DB;
class HomeController extends Controller
{
  public function downloadPDF()

    {

    	$pdf = PDF::loadView('admin.pdfView');

		  return @$pdf->stream();

    }
    public function downloadPDFstd(Request $request)
      {
        $stdlabel = DB::table('student_groups')
        ->select('student_groups.id','student_groups.class')
        ->where('student_groups.id', '=', $request['stdforpdf'])
        ->first();

        $stdforpdf = $request['stdforpdf'];
        $year_use_id =  $request['year_use_id'];
        $std = DB::table('student_groups')
        ->select('student_groups.id','student_groups.class')
        ->get();

        $termyear = DB::table('year_studies')
                  ->select('year_studies.term','year_studies.year')
                  ->where('year_studies.id', '=',$year_use_id)
                  ->first();

        $amount_course = DB::table('scheduels')
        ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
        ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
        ->where('instructors.student_groups_id', '=',$request['stdforpdf'])
        ->where('instructors.year_studies_id', '=',$request['year_use_id'])
        ->count();

        $scheduel = DB::table('scheduels')
                  ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                  ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                  ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                  ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                  ->leftJoin('courses', 'courses.id', '=', 'instructors.courses_id')
                  ->orderBy('days_id')
                  ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                  'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                  'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','courses.courses_id','instructors.coach')
                   ->where('instructors.student_groups_id', '=', $request['stdforpdf'])
                    ->where('instructors.year_studies_id', '=', $request['year_use_id'])
                  ->get();
                  $std_display = DB::table('student_groups')
                   ->where('student_groups.id', '=', $request['stdforpdf'])
                  ->select('student_groups.class')
                  ->first();
        $scheduel  = $scheduel->sortBy('times_id')->toArray();
        for ($i=0; $i < count($scheduel) ; $i++) {
            $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
            $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
            $id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->id;
            $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
            $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
            $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
            $course_id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_id;
            $coach[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->coach;
        }
        $pdf = PDF::loadView('admin.pdfstudentscheduel',compact('instructor','day','time','scheduel','arr','data','id','datacourse','classroom','std','stdlabel','stdforpdf','termyear','course_id','amount_course','std_display','coach'));
        $pdf->setPaper('A4', 'portrait');
        return @$pdf->stream();

      }
      public function downloadPDFteacher(Request $request)
        {
          $personellabel = DB::table('personels')
          ->select('personels.id','personels.first_name','personels.last_name')
          ->where('personels.id', '=',$request['teacherid'])
          ->first();

          $termyear = DB::table('scheduels')
                    ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                    ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                    ->leftJoin('year_studies', 'year_studies.id', '=', 'instructors.year_studies_id')
                    ->select('year_studies.term','year_studies.year')
                    ->first();

          $scheduel = DB::table('scheduels')
                    ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                    ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                    ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                    ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                    ->leftJoin('courses', 'courses.id', '=', 'instructors.courses_id')
                    ->orderBy('days_id')
                    ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                    'personels.last_name','courses.courses_name','courses.courses_id','instructors.id','classrooms.classroom_type as classroom','student_groups.class as stdgroup',
                    'scheduels.days_id as day','scheduels.times_id as time','instructors.personels_id','student_groups.class as stdgroup','instructors.coach')
                     ->where('instructors.personels_id', '=', $request['teacherid'])
                    ->where('instructors.year_studies_id', '=',$request['year'])
                    ->get();
        $amount_course = DB::table('scheduels')
        ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
        ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
        ->where('instructors.personels_id', '=', $request['teacherid'])
        ->where('instructors.year_studies_id', '=',$request['year'])
        ->count();


          $scheduel  = $scheduel->sortBy('times_id')->toArray();
          for ($i=0; $i < count($scheduel) ; $i++) {
              $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
              $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
              $id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->id;
              $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
              $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
              $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
              $course_id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_id;
              $stdgroup[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->stdgroup;
              $coach[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->coach;
          }

          $pdf = PDF::loadView('admin.pdfteacher',compact('instructor','day','time','scheduel','arr','data','id','datacourse','classroom','std','personellabel','$personelforpdf','termyear','course_id','amount_course','stdgroup','coach'));
          return @$pdf->stream();

        }
        public function downloadPDFclassroom(Request $request)
          {
            $classroompdf = DB::table('classrooms')
            ->select('classrooms.id','classrooms.classroom_type')
            ->where('classrooms.id', '=',$request['classroom'])
            ->first();

            $termyear = DB::table('scheduels')
                      ->leftJoin('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                      ->leftJoin('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                      ->leftJoin('year_studies', 'year_studies.id', '=', 'instructors.year_studies_id')
                      ->select('year_studies.term','year_studies.year')
                      ->first();

            $scheduel = DB::table('scheduels')
                      ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                      ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                      ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                      ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                      ->leftJoin('courses', 'courses.id', '=', 'instructors.courses_id')
                      ->orderBy('days_id')
                      ->select('scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                      'personels.last_name','courses.courses_name','instructors.id','classrooms.classroom_type as classroom',
                      'scheduels.days_id as day','scheduels.times_id as time','student_groups.class as stdgroup','instructors.personels_id','courses.courses_id','student_groups.class as stdgroup','instructors.coach')
                       ->where('instructors.classrooms_id', '=', $request['classroom'])
                      ->where('instructors.year_studies_id', '=',$request['year'])
                      ->get();
          $amount_course = DB::table('scheduels')
          ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
          ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
          ->where('instructors.classrooms_id', '=', $request['classroom'])
          ->where('instructors.year_studies_id', '=',$request['year'])
          ->count();


            $scheduel  = $scheduel->sortBy('times_id')->toArray();
            for ($i=0; $i < count($scheduel) ; $i++) {
                $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
                $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
                $id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->id;
                $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
                $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
                $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
                $courses_id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_id;
                $coach[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->coach;
                $stdgroup[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->stdgroup;
            }

            $pdf = PDF::loadView('admin.pdfclassroom',compact('instructor','day','time','scheduel','arr','data','id','datacourse','classroom','std','personellabel','personelforpdf','termyear','courses_id','amount_course','classroompdf','stdgroup','coach'));
            return @$pdf->stream();

          }
    //
}
