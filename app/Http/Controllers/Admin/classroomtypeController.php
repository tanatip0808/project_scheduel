<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\classroom;
use DB;
class classroomtypeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $objs = classroom::all();
      $data['objs']=$objs;
      $data['url']= url('admin/classroom/create');
      return view('admin.classrooms',compact('objs'),$data);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $data['method']="post";
      $data['url']= url('admin/classrooms/');
      return view('admin.add_classroom',$data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // $this->validata($request,[
    //     'classroom_type'=>'required'
    //   ]);
    $count_room = DB::table('classrooms')
    ->where('classroom_type', '=', $request['classroom_type'])
    ->count();
    if($count_room<=0){
      $objs = new classroom();
      $objs->classroom_type = $request['classroom_type'];
      $objs->save();
      return redirect(url('admin/classrooms'))->with('success','เพิ่มห้องสำเร็จ!');
    }
    else {
        return redirect(url('admin/classroom'))->with('error','มีห้องเรียนนี้แล้ว!');
    }
      // return redirect(url('admin/classroom'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      $objs = classroom::find($id);
      $data['url']= url('admin/classrooms/'.$id);
      $data['obj']=$objs;
      $data['method']="PUT";
      return view('admin/add_classroom',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {

      $objs = classroom::find($id);
      $objs->classroom_type=$request['classroom_type'];
      $objs->save();
      return redirect(url('admin/classrooms'))->with('success','แก้ไขเรียนร้อย!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $objs = classroom::find($id);
      $objs->delete();
      return redirect(url('admin/classrooms'))->with('delete','ลบเรียบร้อยแล้ว!');

  }
}
