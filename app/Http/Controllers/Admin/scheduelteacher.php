<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class scheduelteacher extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $teacher = DB::table('personels')
        ->Join('statuses', 'statuses.id', '=', 'personels.statuses_id')
        ->select('personels.id','personels.first_name','personels.last_name')
        ->where('statuses.id', '=','1')
        ->get();


              $year = DB::table('year_studies')
              ->select('year_studies.id','year_studies.year','year_studies.term')
              ->get();

              return view('admin.indexschedueltch',compact('teacher','year'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $year = DB::table('year_studies')
        ->select('year_studies.id','year_studies.year','year_studies.term')
        ->get();

        $teacher = DB::table('personels')
        ->Join('statuses', 'statuses.id', '=', 'personels.statuses_id')
        ->select('personels.id','personels.first_name','personels.last_name')
        ->where('statuses.id', '=','1')
        ->orwhere('statuses.status_detail','=','อยู่')
        ->get();

        $teacher_display = DB::table('personels')
        ->select('personels.id as teacherid','personels.first_name','personels.last_name')
        ->where('personels.id', '=',$request['teacher_id'])
        ->first();

        $year_display = DB::table('year_studies')
        ->select('year_studies.id as yearid','year_studies.year','year_studies.term')
        ->where('year_studies.id', '=',$request['year'])
        ->first();

        $teacher_id=$request['teacher_id'];
        $year_use_id=$request['year'];

        $instructor = DB::table('instructors')
                  ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                  ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                  ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                  ->Join('courses','courses.id', '=', 'instructors.courses_id')
                  ->select('courses.courses_name','instructors.id','personels.first_name','instructors.year_studies_id as yearid'
                  ,'personels.last_name','personels.id as tchid','student_groups.id as stdid',
                  'classrooms.classroom_type as classroom','classrooms.id as classid','student_groups.class as class','instructors.times_study','instructors.check_times_study','instructors.color','instructors.coach')
                  ->where('instructors.personels_id', '=', $request['teacher_id'])
                  ->where('instructors.year_studies_id', '=',$request['year'])
                  ->get();

        $instructor_count = DB::table('instructors')
                  ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                  ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                  ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                  ->Join('courses','courses.id', '=', 'instructors.courses_id')
                  ->select('courses.courses_name','instructors.id','personels.first_name','instructors.year_studies_id as yearid'
                  ,'personels.last_name','personels.id as tchid','student_groups.id as stdid',
                  'classrooms.classroom_type as classroom','classrooms.id as classid','student_groups.class as class','instructors.times_study','instructors.check_times_study','instructors.color','instructors.coach')
                  ->where('instructors.personels_id', '=', $request['teacher_id'])
                  ->where('instructors.year_studies_id', '=',$request['year'])
                  ->count();

                  $p=0;

        $scheduel = DB::table('scheduels')
                  ->Join('instructors', 'instructors.id', '=', 'scheduels.instructors_id')
                  ->Join('personels', 'personels.id', '=', 'instructors.personels_id')
                  ->Join('classrooms', 'classrooms.id', '=', 'instructors.classrooms_id')
                  ->Join('student_groups', 'student_groups.id', '=', 'instructors.student_groups_id')
                  ->Join('courses','courses.id', '=', 'instructors.courses_id')
                  ->orderBy('days_id')
                  ->select('scheduels.id as idsch','scheduels.times_id','scheduels.days_id','personels.first_name','scheduels.id as schid',
                  'personels.last_name','courses.courses_name','instructors.id as insid','classrooms.classroom_type as classroom','scheduels.days_id as day',
                  'scheduels.times_id as time','instructors.personels_id as tchid','instructors.year_studies_id as yearid','instructors.classrooms_id as classtypeid','instructors.color','student_groups.class as stdgroup','instructors.coach')
                  ->where('personels.id', '=',$request['teacher_id'])
                  ->where('instructors.year_studies_id', '=',$request['year'])
                  ->get();

        $scheduel  = $scheduel->sortBy('times_id')->toArray();
        for ($i=0; $i < count($scheduel) ; $i++) {
            $idsch[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->idsch;
            $data[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->first_name;
            $datacourse[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->courses_name;
            $insid[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->insid;
            $day[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->day;
            $time[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->time;
            $classroom[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classroom;
            $tchid[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->tchid;
            $yearid[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->yearid;
            $class_type_id[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->classtypeid;
            $color[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->color;
            $stdgroup[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->stdgroup;
            $coach[$scheduel[$i]->days_id.$scheduel[$i]->times_id]=$scheduel[$i]->coach;
                  }
        return view('admin.createscheduelteacher',compact('year','teacher','teacher_display','year_display','coach','year_use_id',
        'instructor','instructor_count','p','scheduel','idsch','data','datacourse','insid','day','time','teacher_id',
        'classroom','tchid','yearid','class_type_id','class_type_id','color','stdgroup'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
