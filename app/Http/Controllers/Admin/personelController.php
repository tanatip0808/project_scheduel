<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\personel;
use DB;
class personelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // $objs = personel::all();
      // $data['objs']=$objs;
      $users = DB::table('personels')
            ->leftJoin('statuses', 'statuses.id', '=', 'personels.statuses_id')
            ->leftJoin('positions', 'positions.id', '=', 'personels.positions_id')
            ->leftJoin('subject_matters', 'subject_matters.id', '=', 'personels.subject_matters_id')
            ->select('personels.id','personels.first_name','personels.last_name','personels.tel','positions.position_name',
            'statuses.status_detail','subject_matters.subject_matter_name','subject_matters.id as sid')
            ->where('position_name', '=',$request['name'])
            ->get();

      $subject = DB::table('subject_matters')
            ->select('subject_matters.id as subid','subject_matters.subject_matter_name as sname')
            ->get();
      $data['subject']=$subject;
      $data['url']= url('admin/personel/create');
      return view('admin.personel',compact('users'),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $position  = DB::table('positions')
            ->get();
      $status  = DB::table('statuses')
            ->get();
      $subject = DB::table('subject_matters')
            ->get();
      $data['positions']=$position;
      $data['subject']=$subject;
      $data['statuses']=$status;
      $data['method']="post";
      $data['url']= url('admin/personel/');
      return view('admin.add_personel',compact('subject','status','positions'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $objs = new personel();
      $objs->first_name = $request['fname'];
      $objs->last_name = $request['lname'];
      $objs->tel = $request['phone'];
      $objs->statuses_id = $request['status'];
      $objs->positions_id = $request['position'];
      $objs->subject_matters_id = $request['subject'];
      $objs->save();

      return redirect(url('admin/personel/?name=ครู'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $users = DB::table('personels')
            ->leftJoin('statuses', 'statuses.id', '=', 'personels.statuses_id')
            ->leftJoin('positions', 'positions.id', '=', 'personels.positions_id')
            ->leftJoin('subject_matters', 'subject_matters.id', '=', 'personels.subject_matters_id')
            ->select('personels.id','personels.first_name','personels.last_name','personels.tel','positions.position_name',
            'statuses.status_detail','subject_matters.subject_matter_name','subject_matters.id as sid')
            ->where('subject_matters_id', '=',$request['subject'])
            ->get();

      $subject = DB::table('subject_matters')
            ->select('subject_matters.id as subid','subject_matters.subject_matter_name as sname')
            ->get();
      $data['subject']=$subject;
        $data['url']= url('admin/personel/');
        return view('admin.personel',compact('users'),$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $objs = personel::find($id);
      $position  = DB::table('positions')
            ->get();
      $status  = DB::table('statuses')
            ->get();
      $subject = DB::table('subject_matters')
            ->get();
      $data['positions']=$position;
      $data['subject']=$subject;
      $data['status']=$status;
      $data['url']= url('admin/personel/'.$id);
      $data['obj']=$objs;
      $data['method']="PUT";
      return view('admin/add_personel',compact('subject','status','positions','objs'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $objs = personel::find($id);
      $objs->first_name = $request['fname'];
      $objs->last_name = $request['lname'];
      $objs->tel = $request['phone'];
      $objs->statuses_id = $request['status'];
      $objs->positions_id = $request['position'];
      $objs->subject_matters_id = $request['subject'];
      $objs->save();
      return redirect(url('admin/personel'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $objs = personel::find($id)->delete();
    }
}
