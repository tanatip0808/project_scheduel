<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\position;
class positionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $objs = position::all();
      $data['objs']=$objs;
      $data['url']= url('admin/position/create');
      return view('admin.position',compact('objs'),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['method']="post";
      $data['url']= url('admin/position/');
      return view('admin.add_position',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $objs = new position();
        $objs->position_name = $request['position_name'];
        $objs->save();

        return redirect(url('admin/position'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $objs = position::find($id);
        $data['url']= url('admin/position/'.$id);
        $data['obj']=$objs;
        $data['method']="PUT";
        return view('admin/add_position',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $objs = position::find($id);
        $objs->position_name=$request['position_name'];
        $objs->save();
        return redirect(url('admin/position'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $objs = position::find($id);
        $objs->delete();
        return redirect(url('admin/position'));
    }
}
