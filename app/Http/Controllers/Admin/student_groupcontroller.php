<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\student_group;
use DB;
class student_groupcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $objs = student_group::all();
      $data['objs']=$objs;
      $data['url']= url('admin/student_group/create');
      return view('admin.student',compact('objs'),$data);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['method']="post";
      $data['url']= url('admin/student_group/');
      return view('admin.add_student',$data);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $count_room = DB::table('student_groups')
      ->where('class', '=', $request['class'])
      ->count();
      if($count_room<=0){
        $objs = new student_group();
        $objs->class = $request['class'];
        $objs->save();
        return redirect(url('admin/student_group'))->with('success','เพิ่มห้องสำเร็จ!');
      }
      else {
          return redirect(url('admin/student_group'))->with('error','มีห้องเรียนนี้แล้ว!');
      }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $objs = student_group::find($id);
      $data['url']= url('admin/student_group/'.$id);
      $data['obj']=$objs;
      $data['method']="PUT";
      return view('admin/add_student',$data);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $objs = student_group::find($id);
      $objs->class=$request['class'];
      $objs->save();
      return redirect(url('admin/student_group'))->with('success','แก้ไขเรียนร้อย!');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $objs = student_group::find($id);
        $objs->delete();
        return redirect(url('admin/student_group'))->with('delete','ลบเรียบร้อยแล้ว!');
    }
}
