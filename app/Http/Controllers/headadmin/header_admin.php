<?php

namespace App\Http\Controllers\headadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class header_admin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( )
    {
        //
        $admin = DB::table('users')
              ->Join('subject_matters', 'subject_matters.id', '=', 'users.subject_id')
              ->select('users.id','users.user_name','subject_matters.subject_matter_name as subname')
              ->get();
        $data['url']= url('headadmin/head_admin/create');
        return view('head_admin.head_admin',compact('admin','subject'),$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subject = DB::table('subject_matters')
              ->select('subject_matters.id as subid','subject_matters.subject_matter_name as sname')
              ->get();
        $data['method']="post";
        $data['url']= url('headadmin/head_admin/');
        return view('head_admin.register_admin',compact('subject'),$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $subject = DB::table('subject_matters')
              ->select('subject_matters.id as subid','subject_matters.subject_matter_name as sname')
              ->get();
              $data['url']= url('headadmin/head_admin');
              $data['method']="post";

          $count_user = DB::table('users')
          ->select('users.user_name')
          ->where('users.user_name', '=',$request['user_name'])
          ->count();

          $admin = DB::table('users')
                ->select('users.id','users.user_name')
                ->where('user_name', '=',$request['user_name'])
                ->get();

              if ($count_user<2) {
                $id = DB::table('users')->insertGetId(
                [
                               'user_name' => $request['user_name'],
                               'password' => $request['password'],
                               'subject_id' => $request['subject'],
                ]);
                  return redirect(url('headadmin/head_admin'))->with('success','เพิ่มข้อมูลสำเร็จ!');
              }
              else {
                return redirect(url('headadmin/head_admin'))->with('error','user_name ซ้ำ!');
              }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
