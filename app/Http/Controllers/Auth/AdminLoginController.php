<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class AdminLoginController extends Controller
{
  public function __construct()
  {
     $this->middleware('guest:admin',['except' => ['logout']]);
  }
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }
    public function login(Request $request)
    {
           //validate the form data
           $this->validate($request,[
               'email' => 'required|email',
               'password' => 'required|min:6'
           ]);
           //attempt to login the admins in
           if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])){
               //if successful redirect to admin dashboard
               return redirect()->intended(route('admin.dashboard'));
           }
           //if unsuccessfull redirect back to the login for with form data
           return redirect()->back()->withInput($request->only('email','remember'));
    }
    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('/');
    }
}
